package com.prefabware.js.test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.prefabware.commons.CollectionUtil;
import com.prefabware.commons.Condition;
import com.prefabware.web.PathBuilder;

public class DohTestUrlBuilder {
	String baseUrl = "";

	public DohTestUrlBuilder withBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
		return this;
	}

	public DohTestUrlBuilder withTestClass(String testClass) {
		this.testClass = testClass;
		return this;
	}

	public DohTestUrlBuilder withRunnerResource(String runnerResource) {
		this.runnerResource = runnerResource;
		return this;
	}

	/**
	 * @author stefan
	 *
	 */
	static class Module {
		
		public Module(String name, String urlPath) {
			super();
			this.name = name;
			this.urlPath = new PathBuilder().appendPathWithoutTrailingSlash(urlPath).toString();
		}
		/**leading
 		 * the name of the resources of this module, should NOT end with a slash '/'
		 */
		String name;
		/**
		 * the urlPath of the resources of this module, should NOT end with a slash '/'
		 */
		String urlPath;
	}
	List<Module>modules=new ArrayList<>();
	public DohTestUrlBuilder withDohUtilBaseUrl(String dohUtilBaseUrl) {
		this.dohUtilBaseUrl = dohUtilBaseUrl;
		return this;
	}

	public DohTestUrlBuilder withModule(String name, String urlPath) {
		Module m=new Module(name,urlPath);		
		modules.add(m);
		return this;
	}

	String testClass;
	String runnerResource = "doh/runner.html";
	/**
	 * the url of /util
	 */
	String dohUtilBaseUrl;
	private PathBuilder b;

	public String build() {
		Condition.notNull(runnerResource, "..runnerResource");
		Condition.notNull(testClass, "..testClass");
		b = new PathBuilder(baseUrl);
		appendPath(dohUtilBaseUrl);
		appendPath(runnerResource);
		append("?testModule=");
		append(testClass);
		append("&registerModulePath=");
		append(CollectionUtil.seperatedBy(modules.stream().map(m->m.name+","+m.urlPath).collect(Collectors.toList()), ";"));		
		return b.toString();
	}

	private void append(String string) {
		b.append(string);
	}

	private void appendPath(String path) {		
		b.appendPath(path);
	};
}