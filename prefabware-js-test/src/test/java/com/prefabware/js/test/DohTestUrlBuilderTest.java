package com.prefabware.js.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DohTestUrlBuilderTest {

	@Before
	public void setUp() throws Exception {}

	@Test
	public void testSingleModule() {
		DohTestUrlBuilder b = new DohTestUrlBuilder();
		b.withBaseUrl("http://localhost:8080")
		.withDohUtilBaseUrl("webjars/dojo-dojo/1.9.5/util/")
		.withTestClass("prefabware.util.tests.TemplateTest")
		.withModule("prefabware", "/webjars/prefabware/");		
		assertEquals("http://localhost:8080/webjars/dojo-dojo/1.9.5/util/doh/runner.html?testModule=prefabware.util.tests.TemplateTest&registerModulePath=prefabware,/webjars/prefabware", b.build());
	}
	@Test
	public void testTwoModules() {
		DohTestUrlBuilder b = new DohTestUrlBuilder();
		b.withBaseUrl("http://localhost:8080")
		.withDohUtilBaseUrl("webjars/dojo-dojo/1.9.5/util/")
		.withTestClass("prefabware.util.tests.TemplateTest")
		.withModule("prefabware", "/webjars/prefabware/")
		.withModule("adamarina", "/webjars/adamarina/");
		assertEquals("http://localhost:8080/webjars/dojo-dojo/1.9.5/util/doh/runner.html?testModule=prefabware.util.tests.TemplateTest&registerModulePath=prefabware,/webjars/prefabware;adamarina,/webjars/adamarina", b.build());
	}
}
