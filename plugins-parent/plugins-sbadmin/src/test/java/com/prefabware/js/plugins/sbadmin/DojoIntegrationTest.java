package com.prefabware.js.plugins.sbadmin;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.prefabware.js.test.DojoTestSupport;
@Ignore
public class DojoIntegrationTest {
    private DojoTestSupport testSupport;
    @Before
    public void setUp() throws Exception {
    	testSupport=new DojoTestSupport("http://localhost:8080/plugins-sbadmin/js");
    	testSupport.add("prefabware/plugins/prefabware/sbadmin/tests/menu/MenuTest.html");    	
    	testSupport.add("prefabware/plugins/prefabware/sbadmin/tests/PluginTest.html");    	
    }
    
    @Test
    public void testAll() throws Exception {
    	testSupport.testAll();    	
    } 
    @After
	public void after() throws Exception {		
		//testSupport.tearDown();
	}
}