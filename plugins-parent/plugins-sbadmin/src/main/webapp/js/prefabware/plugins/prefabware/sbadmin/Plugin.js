//this plugin provides integration with the sbadmin template
define('prefabware/plugins/prefabware/sbadmin/Plugin', [ 'dojo',  
		'dojo/_base/array', 'dojo/_base/lang','dojo/aspect',
		'dojo/_base/declare',
		'prefabware/plugin/Plugin' ], function(dojo,  array, lang, aspect) {
	dojo.declare("prefabware.plugins.prefabware.sbadmin.Plugin",
			[ prefabware.plugin.Plugin ], {
			storePlugin    : {inject:'prefabware.store',type:'plugin'},  	
			uiPlugin       : {inject:'prefabware.ui',type:'plugin'},
			
			startup : function() {
				
			},

			});
});
