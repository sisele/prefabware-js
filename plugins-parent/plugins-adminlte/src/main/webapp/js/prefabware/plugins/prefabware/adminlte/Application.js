//to initialize template specific widgets
define('prefabware/plugins/prefabware/adminlte/Application', [
        'dojo',
        "dojo/_base/lang", // lang.hitch lang.trim
        "dojo/_base/array", //forEach
        "dojo/i18n", // i18n.getLocalization
        'dojo/aspect',
        'prefabware/plugin/PluginRegistry',
        "prefabware/lang",
        'dojo/_base/Deferred',        
		'dojo/_base/declare',
		], function(dojo, lang,array,i18n,aspect) {
	dojo.declare("prefabware.plugins.prefabware.adminlte.Application", null, {
		pluginRegistry:null,		
		constructor : function(options) {
			this.pluginRegistry=options.pluginRegistry;
		},
		startup : function() {
			var that=this;
			 this.pluginRegistry.startup().then(function(value){
				    // Do something when the process completes
					var uiPlugin=that.pluginRegistry.findPlugin('prefabware.ui');
					
					uiPlugin.createMenus();
				  }, function(err){
				    prefabware.lang.throwError(err);
				  }, function(update){
				    // Do something when the process provides progress information
				  });
		}
});
});
