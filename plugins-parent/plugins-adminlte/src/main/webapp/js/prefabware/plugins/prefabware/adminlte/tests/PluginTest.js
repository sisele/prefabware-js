dojo.provide("prefabware.app.crating.tests.PluginTest");
dojo.require("prefabware.plugins.prefabware.whitemin.Whitemin");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.promise.all");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.app.crating.tests.PluginTest", [ {
	name : "app-crating",

	setUp : function() {
		 var root=dojo.byId("logBody").parentNode;
		 var menuNode=dojo.create("div",null,root);
		 dojo.attr(menuNode, 'id', 'appdriver-menu'); // set
		 var centerNode=dojo.create("div",null,root);
		 dojo.attr(centerNode, 'id', 'appdriver-center'); // set
	},

	runTest : function() {
		 var pluginRegistry=new prefabware.plugin.PluginRegistry({namespace:'prefabware.app.crating.tests',
				fileName:'pluginTest-registry.json'});
		var workplace = new prefabware.plugins.prefabware.whitemin.Whitemin({pluginRegistry:pluginRegistry});
		workplace.startup();

		return;
	},
	tearDown : function() {
	}
} ]);