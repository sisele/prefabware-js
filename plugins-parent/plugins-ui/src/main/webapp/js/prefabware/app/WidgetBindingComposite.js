//a combobox to select a value for a :1 relation
//the value, that corresponds to the antity.attribute is widget.item not widget.value !
define('prefabware/app/WidgetBindingComposite', [ 'dojo',  'prefabware/app/WidgetBinding','dojo/_base/declare' ], function(dojo,WidgetBinding) {
	dojo.declare("prefabware.app.WidgetBindingComposite", [prefabware.app.WidgetBinding], {
		attribute:null,
		widget:null,

		constructor : function(attribute,widget) {
		},
		getValue : function() {
			//get the value of the widget
			return this.widget.item;
		},
		setValue : function(value) {			
			//value is the value of entity.attribute
			//value is a Value or Composite or an Entity itself
			//this is the current item selected in the combobox
			this.widget.item=value;
			//show the user the label of the value
			if (value) {
				this.widget.set("value", value.__label());
			}else{
				this.widget.set("value", "");
			}
		},		
	});
});
