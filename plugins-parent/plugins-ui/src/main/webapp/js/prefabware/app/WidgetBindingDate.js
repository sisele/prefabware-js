//a combobox to select a value for a :1 relation
//the value, that corresponds to the antity.attribute is widget.item not widget.value !
define('prefabware/app/WidgetBindingDate', [ 'dojo',  'prefabware/app/WidgetBinding','dojo/_base/declare' ], function(dojo,WidgetBinding) {
	dojo.declare("prefabware.app.WidgetBindingDate", [prefabware.app.WidgetBinding], {

		setValue : function(value) {			
				this.widget.set("value", value);
		},		
	});
});
