//this is the the plural of a single binding for the widgets of an editor
//TODO anders als die widget bindings kennt das EditorBinding sein widget nicht !!
//es ist also eher ein inneres Binding und sollte EditorBindings heissen !
define('prefabware/app/EditorBinding', [ 'dojo',
                                  'dojo/query',
                                  'dojo/dom-attr',
                                  'dojo/dom-construct',
                                  'dojo/_base/array',
                                  'dojo/_base/lang',
                                  'dojox/collections/Dictionary',
                                  'dojo/aspect',
		], function(dojo, query, domAttr,domConstruct,array,lang,dictionary,aspect) {
	dojo.declare('prefabware.app.EditorBinding', null, {
		actionContext:{},
		parent:null,
		actions : null,
		entity : null,
		default_:null,//the object with defaults for new entites
		bindings : null,// the widget bindings
		//i18n labels
		labels : null,
		constructor : function(type,bindings) {
			this.type = type;
			this.bindings=new dojox.collections.Dictionary();
			array.forEach(bindings,function(binding){
				this.bindings.add(binding.attribute.name,binding);
				aspect.after(binding.widget, "onChange",lang.hitch(this,"onChange"),true);
			},this);
		},	
		startup : function() {
			this.inherited(arguments);
			//TODO never called
//			var that=this;
//			array.forEach(this.bindings,function(binding){
//				//listen to the changes of widgets
//				//binding.widget.on("change", that.onChange());
//			},this);
		},
		onChange : function() {
			//will be called when one of the widgets is changed
			//clients can hook in here
			return;
		},
		attachedBindings:function(){
			//returns an array containing only the attached bindings
			return array.filter(this.bindings.getValueList(),function(binding){
				return binding.isAttached();
			});
		},
		validate : function() {
			//validates the bindings, they check if the value of their widgets can be converted to attributes of the entity
			//returns the validation result that failed {success:false,message:message,binding:binding}
			//or {success:true}
			var result=array.map(this.attachedBindings(),function(binding){
				//reset old markers
				var message={messageType:'none'};
				binding.setValidationMessage(message);
				if (binding.readOnly) {
					//do not validate read only ??					
					return {success:true};
				}else{
					var validationResult=binding.validate();
					if (validationResult.success) {
						return validationResult;
					}else{
						var clone = lang.clone(validationResult);
						//clone.binding=binding;
						return clone;
					}
				}
			});
			
			//the result is success, if all elements are success
			result.success=array.every(result,function(element){
				return element.success;
			},this);
			
			return result;
		},
		setValidationMessage : function(marker) {
			//'error', 'warning', 'success', 'none'
			array.forEach(this.attachedBindings(),function(binding){
				binding.setValidationMessage(marker);
			},this);
		},
		setToDefault : function() {
			//sets the ui elements to their default values
			array.forEach(this.attachedBindings(),function(binding){
				//do not commit read only
				if (!binding.readOnly) {
					binding.setToDefault();
				}
			});
		},
		setValue : function(entity) {						
			// set the values of the ui, also from nested properties
			// this.formWidget.set('value', entity);
			array.forEach(this.attachedBindings(),function(binding){
				
				var attrName=binding.attribute.name;
				if (entity==null||entity==undefined) {
					prefabware.lang.debug("set default " + attrName);
					binding.setToDefault();
				}else{
					entity.__resolveProxies(attrName).then(function(value){
						var value=entity.__valueOf(attrName);
						binding.setValue(value);
					});
				}
			});
		},
		commit: function(entity) {
			//writes the values from the ui into the given entity
			array.forEach(this.attachedBindings(),function(binding){
				//do not commit read only
				if (!binding.readOnly) {
					binding.commit(entity);
				}
			});
			return entity;
		},
	});
});