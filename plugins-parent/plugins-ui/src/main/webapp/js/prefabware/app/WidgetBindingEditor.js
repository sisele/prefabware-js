//binds an editor widget to a composite
//the value, that corresponds to the antity.attribute is editor.entity
define('prefabware/app/WidgetBindingEditor', [ 'dojo',  'prefabware/app/WidgetBinding','dojo/_base/declare' ], function(dojo,WidgetBinding) {
	dojo.declare("prefabware.app.WidgetBindingEditor", [prefabware.app.WidgetBinding], {
		attribute:null,
		widget:null,//the editor

		constructor : function(attribute,widget) {
		},
		validate : function() {
			return this.widget.controller.validate();
		},
		getValue : function() {
			//get the value of the widget
			var controller=this.widget.controller;
			controller.commit();
			return this.widget.controller.getEntity();
		},
		setValue : function(value) {
			//TODO shouldnt we use a clone here ?so the widget may not polute the parent entity
			//value is the value of entity.attribute
			//value is a Value or Composite or an Entity itself
			//provide a default if null
		this.widget.controller.setEntity(value);
		},		
		setToDefault : function() {
			//sets the value to the default
			if (this.default_!=undefined&&this.default_!=null) {
				//TODO type conversion, deserialize in editorExtension, fallback to type.default there !!!
				this.setValue(this.default_);
			}else{
				//the widgets of this editor will only create their defaults, when the value is set to null
				this.setValue(null);
			}
		},
	});
});
