//to calculate the width of a column
define('prefabware/app/ColumnWidthStrategy', [ 'dojo', 'dojo/_base/array', 'dojo/_base/declare'], function(dojo, dijit,array) {
	dojo.declare("prefabware.app.ColumnWidthStrategy", null, {
		constructor : function() {			
		},
		
		calcWidth : function(column) {
			//calculates the width of a column and returns it.
			//column.label must be set
			//does not change the column
			//if a width for the column was set, use that without any changes
			if (column.width!=undefined) {
				return column.width;
			}else{
				//if not, calculate a default
				var colWidth = this.pixel(column.__attribute.length);
				var labelWidth = this.pixel(column.label.length);
				//consider the label also, return the bigger of column.width and label.length
				return Math.max(colWidth,labelWidth);
			}
		},
		pixel : function(nbrOfChars) {
			//returns the width in pixel for the given number of chars
			return nbrOfChars*10;
		}
	});
});
