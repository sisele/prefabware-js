//the ui controller
//can openEditor,openView, openBox, showError,showWarning etc.
define('prefabware/plugins/prefabware/ui/tests/menu/ExtensionController', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/aspect',
       'dojo/dom-construct',
       'dojo/dom-style',
       "dojo/window",
       'dojo/promise/all',
       'dojox/collections/Dictionary',
       'prefabware/lang',
       'prefabware/plugins/prefabware/ui/ExtensionController',
       'dojo/_base/declare' ], function(
		dojo, array,aspect,domConstruct,domStyle,win,fx,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.ui.tests.menu.ExtensionController", [prefabware.plugins.prefabware.ui.ExtensionController], {
		storePlugin:{inject:'prefabware.store',type:'plugin'},   
		uiPlugin:{inject:'prefabware.ui',type:'plugin'},   
		actionPlugin:{inject:'prefabware.action',type:'plugin'},   
		
		addRow : function(row) {
			
			},
		addMenuNode : function(options) {},
		addMenuItem : function(options) {},
		addMenuSubItem : function(options) {},
		addSidebarNode : function(options) {},
		addSidebarItem : function(options) {},
		addSidebarSubItem : function(options) {},
		
	});
});
