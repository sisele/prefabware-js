define('prefabware/plugins/prefabware/ui/layout/Layout', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, array) {
	dojo.declare("prefabware.plugins.prefabware.ui.layout.Layout", null, {
		items:null,//an item is something that will be shown using this layout
		constructor : function() {
			this.items=new dojox.collections.Dictionary();
		},
		startup : function() {			
		},	
		fromJSON : function(json) {			
			array.map(json.items,function(jsonItem){
				var item=this.createItem(jsonItem);
				this.items.add(item.name,item);
			},this);
		},	
		createItem : function(jsonItem) {
			//subclasses may override this
			//the returnes item must have a property name
			return jsonItem;
		},	
	});
});
