//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/ui/ExtensionAttributeTooltip', [ 
       'dojo', 
       'prefabware/plugins/prefabware/ui/ExtensionWidget',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionAttributeTooltip", [prefabware.plugins.prefabware.ui.ExtensionWidget], {
		constructor : function() {
			},
		createWidget : function(params, srcNode) {
		var clazz=this.getWidgetClass();
		var widget=new clazz(params,srcNode);
		return widget;
			},		
		createTooltip : function(attribute,srcNode) {
			var parms=this.createTooltipParms(attribute);
			return this.createWidget(parms,srcNode);
			},
		createTooltipParms : function(attribute) {
				return {title   : 'Attribute    : '+attribute.name,
						content : ' type        : '+attribute.type.name+'<br>'
				      +' length      : '+attribute.type.length+'<br>'
				      +' min         : '+attribute.min+'<br>'
				      +' max         : '+attribute.max+'<br>'
				      +' order       : '+attribute.order+'<br>'
				      +' label       : '+attribute.label+'<br>'
				      +' containment : '+attribute.containment+'<br>'
				      +' readOnly    : '+attribute.readOnly+'<br>'
				      +' isLabel     : '+attribute.isLabel+'<br>'
				      +' isKey       : '+attribute.isKey+'<br>'
				      +' isSortDesc. : '+attribute.isSortDescending+'<br>'
					};
		},
		
	});
});
