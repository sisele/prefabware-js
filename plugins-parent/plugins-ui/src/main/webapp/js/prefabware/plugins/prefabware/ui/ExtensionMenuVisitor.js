//to create a menu visting prefabware.ui.menus extensions
define('prefabware/plugins/prefabware/ui/ExtensionMenuVisitor', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/promise/all',
       'dojo/_base/lang',
       'prefabware/lang',
       'prefabware/plugin/Extension',
       'prefabware/plugins/prefabware/ui/MenuVisitor',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, array,promiseAll,lang) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionMenuVisitor", [prefabware.plugin.Extension], {
		uiPlugin:   {inject:'prefabware.ui',type:'plugin'},
		constructor : function(options) {
			var matcher=options.json.matcher;
			if (matcher==null) {
				matcher={};
			}
			var target=matcher.value;
			if (target==null) {
				target='menu';
			}
			this.target=target;
		},		
		createMenu : function() {
			//async
			var that=this;
			var options={
				uiPlugin:this.uiPlugin,					
			};
			var p1=this.uiPlugin.fetchController().then(function(controller){
				options.controller=controller;
				//changes the targets name from 'menu' to 'Menu'
				//so the methodnames will be something like 'createMenuNode'
				//var mid=prefabware.lang.firstLetterUpperCase(that.target);
//				var addNode="add"+mid+"Node";
//				prefabware.lang.assert(typeof controller[addNode]=='function',"The controller has no method "+addNode);
//				var addItem="add"+mid+"Item";
//				prefabware.lang.assert(typeof controller[addItem]=='function',"The controller has no method "+addItem);
//				var addSubItem="add"+mid+"SubItem";
//				prefabware.lang.assert(typeof controller[addSubItem]=='function',"The controller has no method "+addSubItem);
				
//				options.itemFactory={
//						//the MenuVisitor will use this methods to add the menuitems
//						addNode:lang.hitch(controller, addNode),
//						addItem:lang.hitch(controller, addItem),
//						addSubItem:lang.hitch(controller, addSubItem)
//				};
			});
			var p2=this.uiPlugin.matchExtension('prefabware.ui.menuVisitable',{value:that.target}).then(function(menuVisitable){
				options.menuVisitable=menuVisitable;
			});
			var p3=this.uiPlugin.matchExtensions('prefabware.ui.menus',{value:that.target}).then(function(menuExtensions){
				//there are many menus
				options.menuExtensions=menuExtensions;
			});
			return promiseAll([p1,p2,p3]).then(function(){
				var visitor=new prefabware.plugins.prefabware.ui.MenuVisitor(options);
				return visitor.visit();			
			});
		},
	});
});
