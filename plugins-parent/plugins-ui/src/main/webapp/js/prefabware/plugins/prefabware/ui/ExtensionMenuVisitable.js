//to be visited by the ExtensionVisitor
//to create the MenuNodes,-Items and -SubItems
//TODO at the moment theres only one extensiion allowed
//better : one for every target
define('prefabware/plugins/prefabware/ui/ExtensionMenuVisitable', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,globalLabels) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionMenuVisitable", [prefabware.plugin.Extension], {
		constructor : function() {
		},
		startup : function() {
			this.inherited(arguments);
		},	
		
		createNode : function(options){
			//async
			//creates a menu node for the given element
			//options={parent:parent,name:name,extension:extension}
			//returns a promise with the node
		},
		createSubItem : function(options){
			//async
			//creates an item in a submenu for the element inside the parent
			//options={parent:parent,name:name,extension:extension}
			//returns a promise with the subItem
		},
		createItem : function(options){
			//async
			//creates a menu item
			//returns a promise subItem with the Item
		},
	});
});
