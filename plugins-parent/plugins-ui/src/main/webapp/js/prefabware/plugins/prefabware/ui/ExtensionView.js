//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/ui/ExtensionView', [ 
       'dojo',                    
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/promise/all',
       'prefabware/plugins/prefabware/model/TypeAcceptorMixin',
       'prefabware/plugins/prefabware/model/TypeRegistryAccessMixin',
       'prefabware/app/ColumnWidthStrategy',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionView", [prefabware.plugin.Extension, prefabware.plugins.prefabware.model.TypeRegistryAccessMixin, prefabware.plugins.prefabware.model.TypeAcceptorMixin], {
		actionPlugin:{inject:'prefabware.action',type:'plugin'}, 
		uiPlugin:{inject:'prefabware.ui',type:'plugin'}, 
		columnWidthStrategy:null,//the strategy to calculate the column width
		constructor : function() {
		},
		startup : function() {
			if (this.json.columnWidthStrategy!=undefined) {
				var clazz=this.getClassforName(columnWidthStrategy);
				this.columnWidthStrategy=new clazz();
			}else{
				this.columnWidthStrategy=new prefabware.app.ColumnWidthStrategy();
			}
		},	
		
		getViewActions : function(type) {
			//async
			return this.__getActions(this.json.viewActions,type);
		},
		getRowActions : function(type) {
			//async
			return this.__getActions(this.json.rowActions,type);
		},
		getDefaultRowAction : function(type) {
			//async
			return this.__getAction(this.json.defaultRowAction,type);
		},
		__getAction : function(actionId,type) {
			//async
			return this.actionPlugin.fetchAction(actionId,{type:type,i18nKey:actionId}).then(function(action){
				return action;
			});;
		},
		__getActions : function(actionIds,type) {
			//async
			var actions=new Array();
			var that=this;
			array.forEach(actionIds,function(actionId){
				//inside the table we want the actionId to determine thae label and the icon, not the type
				var action=that.__getAction(actionId,{type:type,i18nKey:actionId});
				actions.push(action);
			});
			return promiseAll(actions);
		},
		getColumns : function(type) {
			//async
			var that=this;
			var columns=this.json.columns;
			if (!columns) {
				columns=[];
			}
			if (columns.length==0) {
				//if no columns configured, create them here
				var candidates=array.filter(type.getAttributes(),function(attribute){
					//create columns only for visible attributes
					//and not for multi arrays
					//and are not optional,
					return that.uiPlugin.isVisible(attribute)&&attribute.max==1&&attribute.isOptional==false;
					});
				//create one column for each field as a default
				columns =array.map(candidates,function(attribute){
					return {name:attribute.name};
				});
			}
			columns=this.__appendColumns(columns,type);
			return this.__postProcessColumns(columns,type).then(function(columns){
				return columns;
			});
		},
		__appendColumns : function(columns,type) {
			// clone the columns array, to not polute the original	
			var result=new dojox.collections.Dictionary();
			array.forEach(columns,function(column){
				result.add(column.name,column);
			});
			var that=this;
			var append=function(name){
				if (that.uiPlugin.isVisible(name)) {
					if (!result.containsKey(name)) {
						result.add(name,{
							name : name,
							label : name,//default
							width : '50px'
						});
					}
				}
			};
			
			// apends columns for type,id and actions
			append('pfw_id');
			append('version');
			append('pfw_type');

//TODO this is dijit specific move there !! 
//append an empty column at the end to fill the panel
//			result.add(' ',{
//				name : ' ',
//				label : ' ',
//				width : 'auto',
//				get:function(){return '';}
//			});
			return result.getValueList();
		},
		__postProcessColumns : function(columns,type) {
			//async
			var that=this;
			var promises=[columns.length];
			for ( var intx = 0; intx < columns.length; intx++) {
				var column = columns[intx];
				var attr = type.getAttribute(column.name);
				if (!attr) {
					console.warn( 'cannot find attribute for column "' + column.name
							+ '" in type' + type.label);
					continue;
				} else {
					column.__attribute = attr;
				}
				if (column.label == undefined) {
					promises[intx]= that.extendedPlugin.labelOf(attr.label).then(function(label){
						 column.label=label;	
					});
				}
				
				if (column.field == undefined) {
					// the field is needed to have a class like field-firstName
					// in the row
					column.field = column.name;
				}

				// set a formatter for each column, that can handle nested
				// fields
				if (column.get == null) {
					column.get = function(	entity) {
						// 'this' is the column
						// the entity may be a promise from the store
						//in that case add a callback
						if (entity.then) {
							//cant do nothing deferred here, 
							return 'could not read response from server';
						}else{
						return entity.__labelOf(this.name);
						}
					};
				}
				if (column.width==undefined) {
					column.width=this.columnWidthStrategy.calcWidth(column);
				}
				if (column.__attribute) {
					//the last empty cell has no attribute
				var renderCell=function(object, data, td, options) {
					//this=the column, do not use the var column, thats allways the last column of the grid here 
					var style=this.__attribute.type.style;
					//TODO setting styles here does not work for columns that
					//get their style through css like pfw_id
//					if (style) {
//						domStyle.set(td,style);
//					};
					//width is set in GridWidget
					td.appendChild(document.createTextNode(data));
				  return; };
				  
				  column.renderCell=renderCell;
				  //aspect.after(column,"renderCell",renderCell,true);
				}


			}
			return promiseAll(prefabware.lang.selectNotNull(promises)).then(function(){
				return columns;	
			});
		},
	});
});
