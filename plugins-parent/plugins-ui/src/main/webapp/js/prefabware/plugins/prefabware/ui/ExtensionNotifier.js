//to send notifications to the user
//a notifier is a message that pops up and closes after a while
define('prefabware/plugins/prefabware/ui/ExtensionNotifier', [ 
       'dojo', 
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       "dojo/on",
       'dojo/_base/declare' ], function(
		dojo, Extension,array,on) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionNotifier", [prefabware.plugin.Extension], {
		hookSelector:'.top-left',
		constructor : function() {
		},
		startup : function() {
			
			return;
		},		
		notify : function(options) {
			//subclasses have to implement this
			
		},		
	});
});