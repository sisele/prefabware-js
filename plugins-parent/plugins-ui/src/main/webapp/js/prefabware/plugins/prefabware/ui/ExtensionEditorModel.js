//plugins may contribute models for types
define('prefabware/plugins/prefabware/ui/ExtensionEditorModel', [ 'dojo',
		'prefabware/plugin/Extension', 'dojo/_base/array', 'dojo/aspect',
		'dojox/collections/Dictionary',
		'dojo/_base/declare' ], function(dojo, Extension, array, aspect) {
	dojo.declare("prefabware.plugins.prefabware.ui.ExtensionEditorModel", [	prefabware.plugin.Extension], {
		id : null,
		modelPlugin : {inject : 'prefabware.model', type : 'plugin'},
		actionPlugin : {inject : 'prefabware.action', type : 'plugin'},
		default_ : null,
		attributes : null,//the names of the attributes to show
		actions:null,
		modal:true,
		counter : {
			__next : 1,
			next : function() {
				var value = this.__next;
				this.__next++;
				return value;
			}
		},
		startup : function() {
			this.actions=this.json.actions;
			if (this.actions==null) {
				this.actions=[];
			}
			//if the default actions are not declared in json
			//add them at the beginning
			if (this.actions.indexOf("close")==-1) {
				this.actions.unshift("close");
			}
			if (this.actions.indexOf("save")==-1) {
				this.actions.unshift("save");
			}
		},
		constructor : function() {
			var that=this;
			this.attributes=new dojox.collections.Dictionary();
			if (that.json.modal!=null) {
				this.modal=that.json.modal;
			}
			if (that.json.attributes!=null) {
				array.map(this.json.attributes,function(attribute){
					that.attributes.add(attribute);
				});
			}
		},
		// startup : function() {},
		getType : function() {
			var type = this.getTypeRegistry().findType(this.json.type);
			return type;
		},
		__getAttributes : function(type, outer) {
			var that = this;
			return array.filter(type.getQualifiedAttributes(), function(
					qAttribute) {
				if (that.attributes.count>0) {
					//if attributes are configured, use only those
					return that.attributes.contains(qAttribute.attribute.name);
				} else {
					//else use all non system attributes
					//TODO configure that 
					return !qAttribute.attribute.isSystem;
				}
			});
		},
		createEditorModel : function(type, outer) {
			// async
			// non recursive
			var that=this;
			var qattributes = this.__getAttributes(type);
			return this.actionPlugin.fetchActions(this.actions, type)
					.then(function(actions) {
						return {
							type : type,
							qattributes : qattributes,
							actions : actions,
							outer : outer,
							modal:that.modal
						};
					});
		},
		__assertParams : function(params) {
			if (params == null || params == undefined) {
				params = {};
			}
			return params;
		},
		getActions : function(references) {
			//TODO what is that  ? references ?
			var actions = new Array();
			var that = this;
			array.forEach(references, function(reference) {
				var action = that.resolveReference(reference);
				actions.push(action);
			});
			return actions;
		},
	});
});
