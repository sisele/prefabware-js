//base for widgets
//TODO rename this to _AttributeWidget because this is a widget that will allwas be bound to an attribute
define('prefabware/plugins/prefabware/ui/widgets/_Widget', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dijit/_Widget',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct) {
	dojo.declare("prefabware.plugins.prefabware.ui.widgets._Widget", [dijit._Widget], {
		attachPoint_value:null,//where to put the value of the widget
		qattribute:null,
		attribute:null,
		outer:null,
		width:null,
		widthPercent:null,
		constructor : function(params,parent) {
			//programatically created widgets :
			//normally parent == undefined and the caller
			//will place the widget.domNode into the documet
			this.params=params;
			this.outer=params.outer;
			if (params.qattribute) {
				//TODO refactor to _Widget, _WidgetUnbound
				this.qattribute=params.qattribute;
				this.attribute=params.qattribute.attribute;
			}
			//if no id was supplied, create one which is more meaningfull
			//than the one that might be created by dijit
			
			this.width=params.width;
			this.widthPercent=params.widthPercent;
		},		
		startup : function() {
			this.inherited(arguments);	
			//mark the root of this widget with the attributes name
			//if its a widget for an attribute !
			if (this.qattribute!=null) {
				dojo.attr(this.domNode,"data-pfw-qattribute",this.qattribute.name);
			}else if (this.attribute!=null) {
				dojo.attr(this.domNode,"data-pfw-attribute",this.attribute.name);
			}
			return;
		},
		_setDisabledAttr : function(disabled) {
			if (this.attachPoint_value!=null) {
				this.attachPoint_input.disabled=disabled;
			}
			return;
		},
		_setValueAttr : function(value) {
			if (this.attachPoint_value!=null) {
				this.attachPoint_input.value=value;
			}
			return;
		},
		_getValueAttr : function() {
			if (this.attachPoint_value == null) {
				return null;
			} else {
				return this.attachPoint_value.value;
			}
		},
		__assertParams : function(params) {
			if (params==null||params==undefined) {
				params={};
			}
			return params;
		},
	});
});
