//a Container
define('prefabware/plugins/prefabware/ui/widgets/Container', [ 
       'dojo',  
       'dojo/_base/array',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo,array,template) {
	dojo.declare("prefabware.plugins.prefabware.ui.widgets.Container", [dijit._Widget, dijit._Container, dijit._Templated], {
		constructor : function(options) {			
		},			
	});
});
