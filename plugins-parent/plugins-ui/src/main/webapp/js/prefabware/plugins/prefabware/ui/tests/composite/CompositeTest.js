// Test runs suceessfull, but tests only that creating an editormodel is possible
dojo.provide("prefabware.plugins.prefabware.ui.tests.composite.CompositeTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.promise.all");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.ui.tests.composite.CompositeTest", [ {
	name : "menu test",
	timeout : 1000,
	registry : null,
	uiPlugin:null,
	modelPlugin:null,
	storePlugin:null,
	setUpDeferred : null,
	setUp : function() {
		var that = this;
	
		this.setUpDeferred = new doh.Deferred();
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.ui.tests.composite',
			fileName : 'compositeTest-registry.json'
		});
		this.registry.startup().then(function(registry) {
			that.uiPlugin = registry.findPlugin('prefabware.ui');
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);
			var loader=that.typeRegistry.loader;
			//loader.add("com.prefabware.business.commons.Currency","prefabware.plugins.prefabware.ui.tests.composite", "currency-type.json");
			that.setUpDeferred.resolve();
		});
	},

	runTest : function() {
		var that = this;
		var testDeferred = new doh.Deferred();
		this.setUpDeferred.then(function() {
		var amountType=that.typeRegistry.findType('com.prefabware.business.commons.Amount');
		var currencyType=that.typeRegistry.findType('com.prefabware.business.commons.Currency');
		var withAmountType=that.typeRegistry.findType('com.prefabware.business.commons.WithAmount');
		var withWithAmountType=that.typeRegistry.findType('com.prefabware.business.commons.WithWithAmount');
		that.storePlugin.createStore({type:currencyType}).then(function(store){
			store.query({code:'EUR'}).then(function(euro){
				var amount=amountType.create();
				amount.currency=euro;
				amount.value=1.5;
				
				var withAmount=withAmountType.create();
				withAmount.amount=amount;
				var withWithAmount=withWithAmountType.create();
				withWithAmount.withAmount=withAmount;
				
				that.uiPlugin.createEditorModelAndController(withWithAmountType).then(function(editorMC){
					doh.assertFalse(editorMC==null);
					var model=editorMC.model;
					doh.assertFalse(model==null);
					var controller=editorMC.controller;
					doh.assertFalse(controller==null);
					doh.assertFalse(controller.binding.bindings==null);
					doh.assertEqual(1,controller.binding.bindings.count);

					var amountBinding=controller.binding.bindings.item('amount');
					doh.assertFalse(amountBinding==null);
					var amountQatt=model.qattributes[0];
					doh.assertEqual(amountBinding.qattribute,amountQatt);
					
					var amountWidget=amountBinding.widget;
					doh.assertFalse(amountWidget==null);
					doh.assertEqual(amountWidget.qattribute,amountQatt);
					doh.assertEqual(amountWidget.outer.model,model);
					
					doh.assertTrue(model.outer==null);
					doh.assertTrue(dojo.isArray(model.actions));
					doh.assertEqual(amountQatt.name,"com.prefabware.business.commons.WithWithAmount.amount");
					doh.assertEqual(amountQatt.attribute.name,"amount");
						testDeferred.callback(true);
				});
			});
		}).then(function(amount){
			
		});
		});

		return testDeferred;
	},
	
} ]);