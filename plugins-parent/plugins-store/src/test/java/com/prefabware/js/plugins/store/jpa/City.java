package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

import com.prefabware.jpa.JpaEntity;
@Entity
public class City extends JpaEntity<Long>{
	private static final long serialVersionUID = 1L;

public City(String name) {
		this.name = name;
	}

@ManyToOne(fetch=FetchType.EAGER)
public Country country;

public City() {
}

public String name;
public CitySize size;

}
