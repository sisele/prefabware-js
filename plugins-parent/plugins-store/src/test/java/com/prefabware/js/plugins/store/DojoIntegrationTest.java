package com.prefabware.js.plugins.store;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prefabware.js.test.DohTestUrlBuilder;
import com.prefabware.js.test.DojoTestSupport;
import com.prefabware.web.ResourceMappings.ResourceMapping;

/**
 * to run a test use an url like this :
 * http://localhost:8080/webjars/util/doh/runner.html?testModule=prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWhitManyToOneTest&registerModulePath=prefabware,/webjars/prefabware/
 * 
 * @author stefan
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootTestApplication.class)
// port:0 = use a random, free port
//but here use fixed port, client must connect to server
@WebIntegrationTest("server.port:8080")
public class DojoIntegrationTest {
	private DojoTestSupport testSupport;

	@Value("${local.server.port}") int port;

	@Autowired @Qualifier("rmWebJarDojoUtil") ResourceMapping rmWebJarDojoUtil;
	@Autowired @Qualifier("rmWebJarPrefabware") ResourceMapping rmWebJarPrefabware;

	private DohTestUrlBuilder urlBuilder;

	@Before
	public void setUp() throws Exception {
		String baseUrl = "http://localhost:" + Integer.toString(port);
		testSupport = new DojoTestSupport(baseUrl);
		urlBuilder = createUrlBuilder();
		int wait = 200;
		add("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWhitManyToOneTest",wait);
		add("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWhitOneToManyTest",wait);
		add("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithEmbeddedReferenceTest");
		add("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithStringTest");
		add("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerPagingTest");

		add("prefabware.plugins.prefabware.store.tests.cacheStore.CacheStoreTest");    	
    	add("prefabware.plugins.prefabware.store.spring.tests.SpringRestSerializerTest",3000);    	
    	add("prefabware.plugins.prefabware.store.spring.tests.SpringRestStoreTest");    	
    	add("prefabware.plugins.prefabware.store.tests.reference.ReferenceTest");    	
    	add("prefabware.plugins.prefabware.store.tests.memory.MemoryStoreTest");    	
    	add("prefabware.plugins.prefabware.store.tests.StoreTest");    
	}

	public void add(String testJsClass) {
		testSupport.add(urlBuilder.withTestClass(testJsClass).build());		
	}
	public void add(String testJsClass,int wait) {
		testSupport.add(urlBuilder.withTestClass(testJsClass).build(),wait);		
	}
	public DohTestUrlBuilder createUrlBuilder() {
		DohTestUrlBuilder b = new DohTestUrlBuilder();
		b
				.withDohUtilBaseUrl(rmWebJarDojoUtil.urlPath())
				.withModule("prefabware", rmWebJarPrefabware.urlPath());
		return b;
	}

	@Test
	public void testAll() throws Exception {
		testSupport.testAll();
		return;
	}
}