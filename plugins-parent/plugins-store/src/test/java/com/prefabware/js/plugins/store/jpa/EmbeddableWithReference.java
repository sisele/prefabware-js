package com.prefabware.js.plugins.store.jpa;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
@Embeddable
public class EmbeddableWithReference implements com.prefabware.meta.domain.client.ValueObject,Serializable{
	private static final long serialVersionUID = 1L;
	public String code;
	@ManyToOne
	public WithString one;

	public EmbeddableWithReference() {
		super();
	}


}
