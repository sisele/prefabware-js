package com.prefabware.js.plugins.store.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.prefabware.commons.StringUtil;
import com.prefabware.jpa.TestDataPersisterI;
import com.prefabware.meta.domain.client.EntityWithId;

public class TestData {
	@Autowired
	TestDataPersisterI persister;
	public <E extends EntityWithId<Long>> E add(E e) {
		return persister.add(e);
	}
	public <E> E add(Collection<? extends EntityWithId<Long>> entities) {
		return persister.add(entities);
	}

	public City BERLIN;
	public Country GERMANY;
	public Country SPAIN;
	 City KOELN;
	 com.prefabware.js.plugins.store.jpa.Currency EUR;

	public TestData() { 
		super();
	}
	@PostConstruct
	public void init() throws Exception {
		createWithX();
		Set<java.util.Currency> currencies = new HashSet<java.util.Currency>();
		Locale[] locales = Locale.getAvailableLocales();
		Map<String,Country> countries=new HashMap<String,Country>();
		for (Locale locale : locales) {
			try {
				String iso = locale.getISO3Country();
				//there may be many locals for country, just add once 
				if (!countries.containsKey(iso)) {
				String code = locale.getCountry();
				String name = locale.getDisplayCountry();
				currencies.add(java.util.Currency.getInstance(locale));
				if (!StringUtil.isEmpty(name)) {
					Country country = createCountry(name, iso);
					add(country);
					countries.put(iso,country);
				}
				
				} 
			} catch (Exception e) {
				// currency getInstance may throw exceptions,
			}
		}
		
		
		for (java.util.Currency currency : currencies) {
			 Currency currency2 = new Currency(currency.getSymbol(), currency.getCurrencyCode(), currency.getCurrencyCode(),
					currency.getDefaultFractionDigits());
			 if (currency.getCurrencyCode().equals("EUR")) {
				 this.EUR=currency2;
			 }
			 add(currency2);
	}
		
		GERMANY=countries.get("DEU");
		SPAIN=countries.get("ESP");
		BERLIN = createCity("Berlin",GERMANY,CitySize.XL);
		add(BERLIN);
		add( createCity("Frankfurt",GERMANY,CitySize.L));
		
		KOELN= createCity("Köln",GERMANY,CitySize.L);
		add(KOELN);
		
		add( createCity("Hamburg",GERMANY,CitySize.L));
		add( createCity("Kiel",GERMANY,CitySize.L));
		add( createCity("Dresden",GERMANY,CitySize.L));
		add( createCity("Remscheid",GERMANY,CitySize.S));
		add( createCity("Leipzig",GERMANY,CitySize.L));
		add( createCity("Hannover",GERMANY,CitySize.M));
		add( createCity("Düsseldorf",GERMANY,CitySize.L));

		add( createCity("Madrid",SPAIN,CitySize.XL));
		add( createCity("Don Benito",SPAIN,CitySize.S));
		add( createCity("Badajoz",SPAIN,CitySize.M));
		add( createCity("Barcelona",SPAIN,CitySize.XL));
		add( createCity("Alicante",SPAIN,CitySize.M));
		add( createCity("Murcia",SPAIN,CitySize.M));
		add( createCity("Valencia",SPAIN,CitySize.L));
		add( createCity("Sevilla",SPAIN,CitySize.L));

		Product product=new Product();
		product.name="Milk";
		product.number="0010";
		add(product);
		
		createInvoice(product, "Rechnung 1");
		createInvoice(product, "Rechnung 2");
	}
	public void createWithX() {
		List<WithString> allWs=new ArrayList<>();
		//start with 1 so id 1=> code1
		for (int i = 1; i < 50; i++) {
			WithString ws = createWithString("code"+i);
			add(ws);
			allWs.add(ws);
			WithManyToOne wmto = createWithManyToOne("withManyToOne"+i, ws);
			add(wmto);
		}
		
			WithOneToMany wmtoAll = createWithOneToMany("withOneToMany1", allWs);
			add(wmtoAll);
			WithOneToMany wmtoNone = createWithOneToMany("withOneToMany2", Collections.emptyList());
			add(wmtoNone);
			
			EmbeddableWithReference ewr1 = createEmbeddableWithReference("EmbeddableWithReference1", allWs.get(0));
			WithEmbeddedReference wer = createWithEmbeddedReference("withEmbeddedReference1", ewr1);
			add(wer);
	}
	 void createInvoice(Product product, String renr) {
		Invoice invoice = new Invoice();
		invoice.number = renr;

		invoice.add(addInvoiceItem(10, product));
		invoice.add(addInvoiceItem(20,product));
		add(invoice);
	}
	protected InvoiceLine addInvoiceItem(Integer number, Product product) {
		InvoiceLine line = new InvoiceLine();
		line.number=number;
		line.product=product;
		line.price=new Amount();
		line.price.currency=EUR;
		line.price.value=new BigDecimal(4711);
		return line;
	}
	 City createCity(String name, Country country, CitySize size) {
		City city = new City();
		city.country = country;
		city.name = name;
		city.size=size;
		return city;
	}
	WithString createWithString(String code) {
		WithString entity = new WithString();
		entity.code = code;
		return entity;
	}
	WithManyToOne createWithManyToOne(String code, WithString one) {
		WithManyToOne entity = new WithManyToOne();
		entity.code = code;
		entity.one=one;
		return entity;
	}
	WithOneToMany createWithOneToMany(String code, List<WithString> many) {
		WithOneToMany entity = new WithOneToMany();
		entity.code = code;
		entity.many=many;
		return entity;
	}
	EmbeddableWithReference createEmbeddableWithReference(String code, WithString one) {
		EmbeddableWithReference entity = new EmbeddableWithReference();
		entity.code = code;
		entity.one=one;
		return entity;
	}
	WithEmbeddedReference createWithEmbeddedReference(String code1, EmbeddableWithReference embeddedWithReference) {
		WithEmbeddedReference entity = new WithEmbeddedReference();
		entity.code1 = code1;
		entity.embeddedWithReference=embeddedWithReference;
		return entity;
	}
	 Country createCountry(String name, String iso) {
		Country country = new Country();
		country.name = name;
		country.code = iso;
		return country;
	}
}
