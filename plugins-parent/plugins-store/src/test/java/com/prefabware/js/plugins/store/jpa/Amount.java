package com.prefabware.js.plugins.store.jpa;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
@Embeddable
public class Amount implements com.prefabware.meta.domain.client.ValueObject,Serializable{
	private static final long serialVersionUID = 1L;
	@ManyToOne
	public Currency currency;
	public BigDecimal value;

	public Amount() {
		super();
	}

	public Amount(Currency currency, BigDecimal value) {
		super();
		this.currency = currency;
		this.value = value;
	}
	public Amount(String value, Currency currency) {
		super();
		this.currency = currency;
		this.value = new BigDecimal(value);
	}


	@Override
	public String toString() {
		return "Amount " + value + " " + currency;
	}

}
