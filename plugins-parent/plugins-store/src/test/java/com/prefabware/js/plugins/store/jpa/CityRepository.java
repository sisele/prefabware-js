package com.prefabware.js.plugins.store.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(path="city")
public interface CityRepository extends PagingAndSortingRepository<City, Long> {
	public List<City> findByName(@Param("name") String name);
}
