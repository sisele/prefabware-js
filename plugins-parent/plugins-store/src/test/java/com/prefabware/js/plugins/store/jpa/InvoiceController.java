package com.prefabware.js.plugins.store.jpa;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * to get the totals of an invoice
 * returns always the same totals
 * 
 * @author Stefan Isele
 * 
 */
@Controller
@RequestMapping(value = "/invoice",produces=org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
public class InvoiceController {
	public static class Totals{
		public Long id;
		public BigDecimal totalGross;

	public Totals(Invoice invoice) {
		this.id=invoice.getId();
		this.totalGross=new BigDecimal("123.45");
	}

}
	@Autowired
	InvoiceRepository repository;

	public InvoiceController() {
		super();
	}

	/**
	 * http://localhost:8080/plugins-store/api/invoice/totals?id=1	
	 * @param name
	 * @return 
	 */
	@RequestMapping(value = "totals", method = RequestMethod.GET)
	public @ResponseBody
	Totals totals(@RequestParam("id") Long id) {
		Invoice invoice = repository.findOne(id);
		return new Totals(invoice);
	}
	/**
	 * http://localhost:8080/plugins-store/api/invoice/one?id=1	
	 * @param name
	 * @return 
	 */
	@RequestMapping(value = "one", method = RequestMethod.GET)
	public @ResponseBody
	Invoice findOne(@RequestParam("id") Long id) {
		Invoice inv = repository.findOne(id);
		if (inv==null) {
			throw new ResourceNotFoundException("Invoice not found, id="+id);
		}
		return inv;
	}

}
