package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Entity;

import com.prefabware.jpa.JpaEntity;
import com.prefabware.meta.domain.annotation.KeyAttribute;

@Entity
public class Currency extends JpaEntity<Long> {
	private static final long serialVersionUID = 1L;
	
	
	/**
	 *TODO fields must be public or have getters to be recognized by jackson 
	 */
	@KeyAttribute
	public String code;
	public Integer decimals;

	public String description;
	public String symbol;
	public Currency() {
		super();
	}

	public Currency(String symbol, String code, String description, int decimals) {
		super();
		this.symbol = symbol;
		this.code = code;
		this.decimals = decimals;
		this.description = description;
		
	}	

	@Override
	public String toString() {
		return "Currency " + symbol + " " + code + " " + decimals;
	}
	
}
