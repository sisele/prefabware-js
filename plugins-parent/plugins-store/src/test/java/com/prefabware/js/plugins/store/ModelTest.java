package com.prefabware.js.plugins.store;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prefabware.js.plugins.store.ModelTest.Config;
import com.prefabware.js.plugins.store.jpa.Invoice;
import com.prefabware.js.plugins.store.jpa.InvoiceLine;
import com.prefabware.model.domain.Attribute;
import com.prefabware.model.domain.EntityType;
import com.prefabware.model.domain.RelationAttribute;
import com.prefabware.model.domain.jpa.TypeFactoryJpaSupport;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { Config.class })
public class ModelTest {
	@Import(StoreTestConfig.class)
	@EnableAutoConfiguration
	@EnableEntityLinks
	static class Config {}


	private TypeFactoryJpaSupport support;

	@Before
	public void setUp() throws Exception {
		support = new TypeFactoryJpaSupport();
	}

	@Test
	public void test() {
		EntityType e = support.factory.getType(Invoice.class);
		assertModelValid(e);
		Attribute<?> attribute = e.getAttribute("lines");
		assertNotNull(attribute);
		assertTrue(attribute instanceof RelationAttribute);
		assertNotNull(attribute.getReference());
		assertTrue(attribute.getReference().isContainment());

		EntityType el = support.factory.getType(InvoiceLine.class);
		assertModelValid(el);
	}

	protected void assertModelValid(EntityType type) {
		assertNotNull(type.getId());
		assertNotNull(type.getPrimaryKey());
		// description is optional
		// assertFalse(type.getDescriptionAttributes().isEmpty());
	}

}
