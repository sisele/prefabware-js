package com.prefabware.js.plugins.store.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(path="invoice")
public interface InvoiceRepository extends PagingAndSortingRepository<Invoice, Long> {
	public Page<Invoice> findByNumberLikeIgnoreCase(@Param("number")String number,Pageable pageable);
}