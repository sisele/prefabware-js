package com.prefabware.js.plugins.store.jpa;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
@RestResource(path="withEmbeddedReference")
public interface WithEmbeddedReferenceRepository extends PagingAndSortingRepository<WithEmbeddedReference, Long> {
	public List<WithEmbeddedReference> findByCode1(@Param("code1")String code1);
}
