package com.prefabware.js.plugins.store;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.jpa.config.EntityPackage;
import com.prefabware.jpa.include.TestDataPersisterConfig;
import com.prefabware.js.plugins.store.jackson.StoreTestModule;
import com.prefabware.js.plugins.store.jpa.StoreTestPackage;
import com.prefabware.js.plugins.store.jpa.TestData;
import com.prefabware.model.rest.server.ModelServerConfiguration;
import com.prefabware.model.rest.server.web.ModelServerWebConfiguration;

@Configuration
@EnableAutoConfiguration
@Import({ ModelServerConfiguration.class,ModelServerWebConfiguration.class,TestDataPersisterConfig.class })
public class StoreTestConfig  {
	
	@Bean
	public StoreTestModule storeTestModule() {
		return new StoreTestModule();
	}

	@Bean
	public TestData testData() {
		return new TestData();
	}

	@Bean
	public EntityPackage storeTestPackage() {
		return new StoreTestPackage();
	}
	
}
