package com.prefabware.js.plugins.store.jpa;

import javax.persistence.Entity;

import com.prefabware.jpa.JpaEntity;
import com.prefabware.meta.domain.annotation.KeyAttribute;
@Entity
public class Country extends JpaEntity<Long> {
		private static final long serialVersionUID = 1L;
		public String name;
		@KeyAttribute
		public String code;
		public Country() {
			super();
		}
		
	}