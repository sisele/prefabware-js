package com.prefabware.js.plugins.store;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.config.EnableEntityLinks;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prefabware.commons.CollectionUtil;
import com.prefabware.commons.logging.LoggingSupport;
import com.prefabware.js.plugins.store.JpaTest.Config;
import com.prefabware.js.plugins.store.jpa.City;
import com.prefabware.js.plugins.store.jpa.CityRepository;
import com.prefabware.js.plugins.store.jpa.InvoiceLine;
import com.prefabware.js.plugins.store.jpa.InvoiceLineRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( classes = { Config.class })
public class JpaTest {
	@Import(StoreTestConfig.class)
	@EnableAutoConfiguration
	@EnableEntityLinks
	static class Config {}

	private LoggingSupport log;
	@Autowired private CityRepository repository;
	@Autowired private InvoiceLineRepository invoiceLineRepository;

	@Before
	public void setUp() throws Exception {

		Package pkg = this.getClass().getPackage();

		log = new LoggingSupport(pkg.getName());

		assertNotNull(repository);
	}

	@Test
	public void testInvoiceLine() throws Exception {
		Pageable pageable = new PageRequest(0, 100);
		Page<InvoiceLine> lines = invoiceLineRepository.findAll(pageable);
		for (InvoiceLine invoiceLine : lines) {
			assertNotNull(invoiceLine.price.currency);
		}

	}

	@Test
	public void testPersist() throws Exception {
		Pageable pageable = new PageRequest(0, 100);
		// use fresh, unpersisted data
		// TestData td = new TestData();
		// theres no cascade for role, so persist before
		Page<City> role = repository.findAll(pageable);
		City city = CollectionUtil.first(role);
		assertNotNull(city);
		assertNotNull(city.country);
	}

	@After
	public void tearDown() {}

	private void log(String string, Object... args) {
		System.out.println(String.format(string, args));
	}

}
