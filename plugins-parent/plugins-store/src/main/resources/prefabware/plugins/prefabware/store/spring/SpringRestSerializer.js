//serializes for Spring REST
//serializes asynchroun, because ReferenceProxy may have to be resolved

define('prefabware/plugins/prefabware/store/spring/SpringRestSerializer', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/promise/all',
       'prefabware/lang',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, array,all) {
	dojo.declare("prefabware.plugins.prefabware.store.spring.SpringRestSerializer", null, {
		map:null,//
		//the serializer needs the store to save new entities that are inside an array
		//before the array can be serialized using the links of the elements
		storePlugin:{inject:'prefabware.store',type:'plugin'},
		serializers:null,
		constructor : function() {
			this.map==new dojox.collections.Dictionary();
			this.serializers=[];
			//arrays first, will just serialize every selement
			this.serializers.push({selector:this.isArray,serializer:this.serializeArray,name:"serializeArray"});
			//entities inside an array have to be serialized as an array of links
			this.serializers.push({selector:this.isEntityAsLink,serializer:this.serializeEntityAsLink,name:"serializeEntityAsLink"});
			//a composite is serialized by serializing its attributes
			this.serializers.push({selector:this.isComposite,serializer:this.serializeComposite,name:"serializeComposite"});
			this.serializers.push({selector:this.isProxy,serializer:this.serializeProxy,name:"serializeProxy"});
			this.serializers.push({selector:this.isEntityRef,serializer:this.serializeEntityRef,name:"serializeEntityRef"});
		},
		serialize : function(object,attribute,options) {
			var that=this;
			//cannot allways return null for null
			//the serializeXXX method has to decide what to do
			//references e.g. must be returned as link null
			//if (object==null) {
			//return prefabware.lang.deferredValue();
			//}
			if (options==null) {
				options={};
			}
			//this is the default, if no serializer is found
			var result=prefabware.lang.deferredValue(undefined);
			array.some(this.serializers,function(def){
				if (def.selector.call(that,object,attribute,options)) {
					prefabware.lang.log("serializing : "+attribute +" using "+def.name);
					result=def.serializer.call(that,object,attribute);
					if (result==null||typeof result.then !='function') {
						//if the serializer did not return a promise, make it a promise
						result= prefabware.lang.deferredValue(result);
					}
					return true;//to escape the loop
				}
			});			
				return result;
		},
		isProxy : function(proxy) {
			if (proxy==null) {
				return false;
			}
			return proxy.__isProxy;
		},			
		serializeProxy : function(proxy,attribute) {
			return proxy.__serializable().then(function(link){
				if (link==null) {
					return {include:false,name:attribute.name,value:null};
				}else{
					return {include:true,name:attribute.name,value:link.href};
				}
			});
		},			
		isArray : function(object,attribute) {
			if (object==null) {
				return false;
			}
			return dojo.isArray(object);
		},			
		
		serializeArray : function(object,attribute) {
			var that=this;
			pValue=array.map(object,function(element){
				return that.serialize(element,attribute).then(function(result) {
					if (result.value!=null&&result.value.href!=null) {
						//references in an array must be serialized as strings and not {rel:..,href..}
						return result.value.href;
					}else{
						//if its not a ref serialize the value 
						return result.value;
					}
				});
			});
			return all(pValue).then(function(values){
				//above all resolves the promises to an array, that contains all result.value
				//from array.map before
				return {include:true,name:attribute.name,value:values};
			});
		},
		
		isEntityRef : function(object,attribute) {
			//its a reference to another entity ?
			return attribute!=null&&attribute.type.isEntityType&&attribute.containment==false;
		},
		serializeEntityRef : function(object,attribute) {
			//entity references are serialized to the self link
			var href;
			var include;
			//the object can be null
			if (object!=null) {
				//if this should be a reference, but its an unsaved entity, its an error
				if (object.__isNew()) {
					prefabware.lang.throwError({sender:"SpringRestSerializer",id:1,text:"cannot serialize a reference to an unsaved entity ",data: object});
				}
				href=object.__prefabware.selfLink.href;
				include=true;
			}else{
				href=null;
				include=false;
			}
			return {include:include,name:attribute.name,value:href};
		},		
		isEntityAsLink : function(composite,attribute,options) {
			if (attribute!=null&&attribute.type.isEntityType&&attribute.max>1&&attribute.containment==true) {
				//serialize entities that are contained in an array
				return true;
			}else{
				return false;
			}
		},
		serializeEntityAsLink : function(composite,attribute) {
			//serialize the entity as a string with the link
			// like "http://localhost:8080/plugins-store/invoiceLine/1"
			
			//if the entity is a proxy and its not dirty, just serialize the proxy link
			var that=this;
			if (composite.__isDirty==false&&this.isProxy(composite)) {
				var result=this.serializeProxy(proxy, attribute);
				return result;
			}
			//else save the entity and than serialize the link of the returned entity
			return that.storePlugin.createStore({type:composite.__type}).then(function(store){
				return store.put(composite).then(function(entity){
					var result= that.serializeEntityRef(entity,attribute);
					return result;
				});
			});
		},
		isComposite : function(composite,attribute) {
			if (composite==null) {
				return false;
			}
			if (composite.__isEntity&&attribute==null) {
				//only if its the root entity
				return true;
			}
			if (composite.__isEntity&&attribute!=null&&attribute.containment==true) {
				//values
				return true;
			}
				//enumerations are composites, too but are serialized to its elements name
				return composite.__isComposite&&!composite.__isEntity&&!composite.__isEnumeration;
			},			
		serializeComposite : function(composite,cattribute) {
			var that=this;
			//a composite is serialized by serializing its attribute values
			var serialized=new Object();
			var promises=[];
			var resultName=null;
			if (cattribute!=null) {
				resultName=cattribute.name;
			}
			array.forEach(composite.__type.getAttributes(),function(attribute){
				var attrName=attribute.name;
				var attrValue=composite[attrName];
				var serName=attribute.name;
//				if (attrName=='version') {
//					//do not serialize version
//					return;
//				}else 
				if (attribute.isReadOnly||attrName=='pfw_type'||attrName=='self') {
					//do not serialize pfw_type, or self reference
					//do not serialize read only attributes
					return;
				}else if (attrName=='pfw_id') {
//					if (attrValue<0||attrValue>9000000000000000) {
//						//very big ids are objects stored in a memorystore
//						//TODO find a better way !
//						//do not serialize the id of new entities
//						return;
//					}
					//serialize pfw_id as id
					serName='id';
				}
				//wrap the call to the serializer inside a self executing function to make the current attrValue,attribute,serName 
				//available for then() 
				var p=(function(attrValue,attribute,serName){return that.serialize(attrValue,attribute).then(function(result){
					if (result==null) {
						//no serializer found here so
						//use the default serializer of the attribute
						var value=attribute.serializableOf(attrValue);
						result={include:true,name:attribute.name,value:value};
					}
					if (result.include==true) {
						//all links must be put in an array with name 'links'
						if (result.inLinks=='links') {
							if (serialized.links==null) {
								serialized.links=[];
							}
							console.log("serialized "+serName+" as link "+result.value);
							serialized.links.push(result.value);
						}else{
						//all other with the attributes name
						console.log("serialized "+serName+" as "+result.value);
						serialized[serName]=result.value;
						}
					}
				});///
				})(attrValue,attribute,serName);//(function
				promises.push(p);
			});
			return all(promises).then(function(){
				return {include:true,name:resultName,value:serialized};;
			});
		},		
	});
});
