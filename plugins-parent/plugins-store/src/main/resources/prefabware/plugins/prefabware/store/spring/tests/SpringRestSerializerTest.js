dojo.provide("prefabware.plugins.prefabware.store.spring.tests.SpringRestSerializerTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");
dojo.require("prefabware.plugins.prefabware.store.spring.hal.SpringRestHalSerializer");
dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.tests.SpringRestSerializerTest", [ 
{
	name : "invoice",
	timeout : 3000,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			that.countryType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Country');
			that.cityType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.City');
			that.invoiceType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Invoice');
			that.invoiceLineType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.InvoiceLine');
			return that.storePlugin.createStore({type:that.countryType}).then(function(store){
				that.countryStore=store;
				return that;
			}).then(function(){
				return that.storePlugin.createStore({type:that.cityType}).then(function(store){
					that.cityStore=store;
					return that;
				}).then(function(){
					return that.storePlugin.createStore({type:that.invoiceType}).then(function(store){
						that.invoiceStore=store;
						return that;
				}).then(function(){
					return that.countryStore.query({code:'DEU'},{urlPath:'country/search/findByCode'}).then(function(result){
						that.countryDE=result[0];
						return that;
					}).then(function(){
						return that.countryStore.query({code:'ESP'},{urlPath:'country/search/findByCode'}).then(function(result){
							that.countryES=result[0];
							return that;
						});
					});
				});
				});;
			});;
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.invoiceStore;
				return store.get(2).then(function(invoice){
					doh.assertTrue(invoice!=null);
					doh.assertTrue(invoice.__type===setup.invoiceType);
					return invoice.__resolveProxies().then(function(){
						
					return store.__toJson(invoice).then(function(serialized){
// the ids of currencies change randomly						
//						doh.assertEqual(
//								{"number":"Rechnung 2","id":"2","version":0,"lines":[{"number":10,"id":"3","version":0,"product":{"rel":"product","href":"http://localhost:8080/plugins-store/product/1"},"price":{"value":4711,"currency":{"rel":"currency","href":"http://localhost:8080/plugins-store/currency/58"}}},{"number":20,"id":"4","version":0,"product":{"rel":"product","href":"http://localhost:8080/plugins-store/product/1"},"price":{"value":4711,"currency":{"rel":"currency","href":"http://localhost:8080/plugins-store/currency/58"}}}]}
//								,dojo.fromJson(serialized));
						deferred.callback(true);
					});
						
					});
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
},
{
	name : "invoice-line",
	timeout : 3000,
	
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.invoiceStore;
			return store.get(2).then(function(invoice){
				doh.assertTrue(invoice!=null);
				doh.assertTrue(invoice.__type===setup.invoiceType);
				return invoice.__resolveProxies().then(function(){
					
					return store.__toJson(invoice.lines[0]).then(function(serialized){
						// the ids of currencies change randomly
//						doh.assertEqual(
//								{"number":10,"id":"3","version":0,"product":{"rel":"product","href":"http://localhost:8080/plugins-store/product/1"},"price":{"value":4711,"currency":{"rel":"currency","href":"http://localhost:8080/plugins-store/currency/58"}}}
//								,dojo.fromJson(serialized));
						deferred.callback(true);
					});
					
				});
			});
		});
		return deferred;
	},
	tearDown : function() {
	}
},
{
	name : "city",
	timeout : 3000,
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var cityType=setup.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.City');
			doh.assertTrue(cityType!=null);
			var citySizeType=setup.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.CitySize');
			doh.assertTrue(citySizeType!=null);
			setup.storePlugin.createStore({type:cityType}).then(function(store){
				doh.assertTrue(store!=null);
				var query={name:'Frankfurt'};
				return store.query(query,{urlPath:'city/search/findByName'}).then(function(result){
					doh.assertTrue(result!=null);
					var city=result[0];
					doh.assertTrue(city!=null);
					
					var serializer=new prefabware.plugins.prefabware.store.spring.hal.SpringRestHalSerializer();
					serializer.serialize(city).then(function(serialized){
						
						
						doh.assertEqual("Frankfurt",serialized.value.name,"");
						doh.assertEqual("L",serialized.value.size,"");
						doh.assertTrue(serialized.value.id>0,"");
						doh.assertTrue(serialized.value.version>=0,"");
						doh.assertEqual(0,serialized.value.country.indexOf("http://localhost:8080/country/"),"");
						deferred.callback(true);
					});;
				});
			});
		});
		return deferred;
	},
	
	tearDown : function() {
	}
},


]);