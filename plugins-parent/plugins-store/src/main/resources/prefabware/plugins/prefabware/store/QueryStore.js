define(["dojo", 'dojo/_base/lang','dojo/_base/array','prefabware/lang'
], function(dojo, lang, array ){

// module:
//		prefabware/plugins/prefabware/store/QueryStore

var QueryStore = function(/*Store*/ store,type,queryCache){
	// wraps and enhances a store so that
	// - queries using placeholder '*' as value are understood
	// - query values get a function test() to work with the simple query engine
	// - a query cache, if supplied, is evicted on put, update,delete
	// - a query cache, is a prefabware.commons.Cache for the method query
	prefabware.lang.assert(store!=null);
	prefabware.lang.assert(type!=null);
	
	var originalQuery = store.query;
	var originalPut = store.put;
	var originalAdd = store.add;
	var originalRemove = store.remove;
	var originalClear = store.clear;
	store.isQueryStore=true;//for test and debug
	store.query= function(p_query, p_options){
		//make a shallow copy
		var query=dojo.mixin({}, p_query);
		var options=null;
		if (p_options==null) {
			options={};
		}else{
			options=dojo.mixin({}, p_options);
		}
		for (prop in p_query){
		//the SimpleQueryEngine does not understand values like '*' or 'A*' 
		//but automatically calls a method test on every query attribute if it exists	
		//so we convert the placeholder into an according test method here
		var propValue=query[prop];//the value of the property in the query
		if (propValue!=null) {
			//TODO and dates ?
			propValue=propValue.toString();
		}
		if (propValue=='*') {
			query[prop]={
					toString:function(){return propValue;},
					value:propValue,//the value of the property in the query
					test:function(value){return true;},	//create a function test, that allways returns true
			};
		}  else if (prefabware.lang.endsWith(propValue,'*')){
			query[prop]={
					toString:function(){return propValue;},
					value:propValue,//the value of the property in the query
					test:function(value){
						//returns true when it matches generic
						//the property value without the appended star
						var woStar=propValue.substring(0, propValue.length-1);
						return prefabware.lang.startsWith(value,woStar);
					}
			};
	  }
	};
	
	if (options.sort==null) {
		//allways use a sort
		//sort must be an array like
		//[{attribute: "name",	descending: false}]
		options.sort=[{attribute:type.keyAttribute.name,descending: false}];
	}
	
	
	arguments[0]=query;	
	var result =originalQuery.call(this, query,options);		
	//QueryResult makes forEach, filter etc. available on the promise
	return result ;
	};
	
	store.put = function(object, options){
		if (queryCache) {
			//TODO why do we evict the whole cache on a single put ?
			queryCache.evict();
		}
		return originalPut.apply(this, arguments);		
		
	};			
	store.add = function(object, options){
		if (queryCache) {
			queryCache.evict();
		}
		return originalAdd.apply(this, arguments);		
		
	};			
	store.remove = function(id, options){
		if (queryCache) {
			queryCache.evict();
		}
		return originalRemove.apply(this, arguments);		
		
	};			
	store.clear = function() {			
		if (queryCache) {
			queryCache.evict();
		}
		return originalClear.apply(this, arguments);		
		
	};			
	return store;
};
lang.setObject("prefabware.plugins.prefabware.store.QueryStore", QueryStore);
return QueryStore;
});
