//a proxy for a reference
//contains a reference to the store it must be loaded from
//TODO that means the store must never be destroyed !
define('prefabware/plugins/prefabware/store/proxy/ReferenceProxy',
		[  'dojo'
		  ,'dojo/_base/lang'
		  ,'dojo/_base/array'
		  ,'prefabware/commons/Proxy'
		  ,'prefabware/lang',
		  'dojo/_base/declare' ],
		  function(dojo, lang,array) {
	dojo.declare("prefabware.plugins.prefabware.store.proxy.ReferenceProxy", [prefabware.commons.Proxy], {		
		__store:null,
		//__isEntity:true,
		constructor : function(type,reference) {
			this.__reference=reference;			
			array.forEach(type.getAttributes(),function(attr){
				if (!attr.isSystem) {
					//the system properties are directly accessible and have values set,
	    			//no need to resolve before acessing them
				this.defineProperty(attr.name);
				}
			},this);
			return; 
		},
		__clone : function() {
			//do not clone proxies
			return this;
		},
		__serializable: function(){
			var that=this;
			//asynchroun, may require resolving the proxy !
		if (this.__isResolvedToNull) {
			return prefabware.lang.deferredValue(null);
		}else if(this.__isResolved) {
			return prefabware.lang.deferredValue(this.__resolved.link);
		}else{
			return this.resolve().then(function(){
				//the proxy may resolve to null, if the resource was not found,
				//this may not be an error
				if (that.__resolved!=null) {
					return that.__resolved.link;
				}else{
					return null;
				}
			});
		}
	},
	doResolve: function(reference){
			var that=this;
			prefabware.lang.assert(reference!=null);
			prefabware.lang.assert(reference.href!=null);
			prefabware.lang.assert(reference.store!=null);
			var store=reference.store;
			//the id may be null !!
			return store.get(reference.id,{target:reference.href}).then(function(result) {
				if (result==null) {
					//if the result is undefined, the requeste resource could not be found
					//this may happen and does not need to be an error
					return prefabware.lang.deferredValue();
				}
				that.__resolved={};
				that.__resolved.link=result.__prefabware.selfLink;
				prefabware.lang.assert(result.pfw_id!=null,'pfw_id must not be null');
				that.pfw_id=result.pfw_id;
				return result;
			});
	}
});
});
