//a dynamic proxy to allow e.g. lazy resolving of references
//can resolve everything if an according __resolver method is provided
//create an instance
//add all properties you want to set and get using addProperty(propertName)
//set a reference. this is object should contain everything you need to resolve the proxy
//set doResolve to a method that can resolve the proxy. the function will get the reference as argument
//and must return the resolved entity
//normally you will also set
define('prefabware/plugins/prefabware/store/proxy/Proxy', [
       'dojo'
       ,'dojo/_base/declare'
       ,'prefabware/lang'
       ,'dojo/when'], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.store.proxy.Proxy", null, {
		//will be used to resolve the reference in doResolve()
		__reference:null,
		//if __isResolved==true, this contains the resolved entity of this proxy
		__target:null,
		//allwayy true, just to make it easy to check
		__isProxy:true,
		//==true means this proxy is allready resolved
		__isResolved:false,
		constructor : function() {
		},
		resolve : function() {
			//async !!
			//returns the resolved this
			if (this.__isResolved) {
				return prefabware.lang.deferredValue(this);
			};
			if (this.__isResolving) {
				return this.__resolvePromise;
			};
			var that=this;
			that.__resolvePromise = this.doResolve(this.__reference).then(function(target){
				if (target==null) {
					throw('reference '+that.__reference+' was resolved to null');
				}
				//mark as resolved
				that.__target=target;
				that.__isResolving=false;
				that.__isResolved=true;
				return this;
			});			
			return that.__resolvePromise;
		},
		doResolve : function(reference) {
			//subclasses do the resolving here
			//must return the target or null if not possible
			//may resolve to a promise
			throw('cannot resolve reference '+__reference+', function doResolve was not set');
		},
		doGet : function(propertyName,value) {
			//propertyName is the name of the property requested
			// value is target[propertyName]
			//default implementation just returns the found value
			//subclasses may change the value before returning it
			return value;
		},
		get : function (propertyName){
			prefabware.lang.log('get '+propertyName);
    		if (this.__target==null) {
    			this.resolve();
			}    		
    		var value=this.__target[propertyName];
    		return this.doGet(propertyName,value);
    	},
    	set : function (propertyName,value){
    		prefabware.lang.log('set '+propertyName);
    		if (this.__target==null) {
    			this.resolve();
			}     		
    		this.__target[propertyName]=value;
    	},
    	createPropertyAccessors : function(propertyName,target){
    		//creates setter/getter for the property
    		if (!target) {
				target=this;
			}
    		//adds a property that will delegate to the set/get accessor method
			var getter=dojo.partial( this.get, propertyName);
			var setter=dojo.partial( this.set, propertyName);

    		Object.defineProperty(this, propertyName, {get:getter,
                set:setter,
                enumerable : true,
                configurable : true});
    	}
	});
});
