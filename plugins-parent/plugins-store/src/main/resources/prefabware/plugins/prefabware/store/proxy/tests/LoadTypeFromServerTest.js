// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.to.tests.LoadTypeFromServerTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.to.TypeImporter");
dojo.require("prefabware.plugins.prefabware.store.proxy.TypeLoaderServer");
dojo.require("prefabware.model.EntityType");
dojo.require("dojo._base.lang");
dojo.require("dojo.cache");
doh.register("prefabware.model.to.tests.LoadTypeFromServerTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	pluginRegistry:null,
	typeRegistry:null,
	loader:null,
	importer:null,
	setUp : function() {
		this.pluginRegistry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.store.proxy.tests',fileName:'LoadTypeFromServerTest-registry.json'});
		this.pluginRegistry.startup();
		this.typeRegistry=this.pluginRegistry.findPlugin('prefabware.model').getTypeRegistry();
		this.servicePlugin=this.pluginRegistry.findPlugin('prefabware.server');
		var target=this.servicePlugin.getContextUrl();
		var url= target +'/'+ 'modelService';
		this.typeRegistry.loader = new prefabware.plugins.prefabware.store.proxy.TypeLoaderServer(url);
		this.importer = new prefabware.model.to.TypeImporter(this.typeRegistry,this.loader);
	},
	runTest : function() {
		var amountType=this.typeRegistry.findType("com.prefabware.business.commons.Amount");
		doh.assertFalse(amountType===null);
		doh.assertFalse(amountType===undefined);
		doh.assertFalse(amountType.getAttribute('currency')===undefined);
		//doh.assertFalse(amountType.getAttribute('name')===undefined);
		return;
	},
	tearDown : function() {
	}
} ]);