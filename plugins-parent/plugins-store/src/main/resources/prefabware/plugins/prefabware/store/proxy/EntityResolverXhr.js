//to resolve a reference
//uses the $ref to call the server and load the referenced entity
//the returned entity may than itself contain unresolved references
define('prefabware/plugins/prefabware/store/proxy/EntityResolverXhr', [ 'dojo', 'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.store.proxy.EntityResolverXhr", null, {
		proxyFactory:null,
		constructor : function(proxyFactory) {
			this.proxyFactory=proxyFactory;
		},
			resolve : function(reference) {
				//expected : reference.$ref = the url
				//expected : reference.type = the type of the entity
				
				var resolvedWrapper = {
						resolved : null
					};
					var outer = this;
				//does really resolve calling the server
				dojo.xhrGet({
					// The target URL on the webserver, per convention this is the
					// reference itself
					url : reference.$ref,
					// call synchron, we have to wait for the result
					sync : true,
					// send JSON
					contentType : 'application/json',
					// receive JSON
					handleAs : 'json',
					// Timeout in milliseconds:
					timeout : 5000,
					// Event handler on successful call:
					load : dojo.partial(function(outer, resolvedWrapper, response, ioArgs) {
						//wrap a proxy around the response
						//the entity arriving here is allready resolved, but it may contain references, entities and values that must
						//be proxied. th proxy created here around the entity will take care of that
						//new prefabware.plugins.prefabware.store.proxy.EntityProxy(reference.type,response);
						outer.proxyFactory.createProxy(reference.type,response);
						resolvedWrapper.resolved = response;
						resolvedWrapper.resolved.__isResolved=true;
					}, outer, resolvedWrapper),

					// Event handler on errors:
					error : function(response, ioArgs) {
						debug.dir(response);
						// return the response for succeeding callbacks
						return response;
					}
				});
				//the call is synchron ! we dont have to wait for response
				var resolved=resolvedWrapper.resolved;
					if (resolved == null) {
						prefabware.lang.log('EntityResolver resolved to null');
					} else {
						prefabware.lang.log('EntityResolver resolved to name'
								+ resolved.toString() + ' label='
								+ resolved.__label());
					}
				return resolvedWrapper.resolved;
			}
	});
});

