dojo.provide("prefabware.plugins.prefabware.store.spring.tests.QueryTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo._base.array");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.tests.QueryTest", [ 
{
	name : "query",
	timeout : 1000,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			that.invoiceType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Invoice');
			return that.storePlugin.createStore({type:that.invoiceType}).then(function(store){
				that.invoiceStore=store;
				return that;
		});
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
	},	
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.invoiceStore;
				return store.get(1).then(function(invoice){
					//enrich the invoice with the totals from an extra call
					doh.assertTrue(invoice!=null);
					doh.assertTrue(invoice.__type===setup.invoiceType);
					var appendPath="totals";
					var query={id:invoice.pfw_id};
					return store.query(query,{entity:invoice,appendPath:appendPath}).then(function(withTotals){
						doh.assertEqual(withTotals.totalGross,123.45);
						deferred.callback(true);
					});
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
},
]);