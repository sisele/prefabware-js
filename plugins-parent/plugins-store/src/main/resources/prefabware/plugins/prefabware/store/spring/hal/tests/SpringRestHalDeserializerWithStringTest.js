dojo.provide("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithStringTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("prefabware.plugins.prefabware.store.spring.SpringRestSerializer");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithStringTest", [ 
{
	name : "setUp",
	timeout : 3000,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.hal.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			
			that.withStringType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.WithString');
			
			return that.storePlugin.createStore({type:that.withStringType}).then(function(store){
				that.withStringStore=store;
				return that;
			}).then(function(that){
				return that;
			});
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},		
	runTest : function() {
		//this test just does the setup
	},
	tearDown : function() {
	}
},
//---------------------------------------------------------------------------------------------------------------
{
	name : "get()",
	timeout : 3000,	
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.withStringStore;
				return store.get(1).then(function(entity){
					doh.assertTrue(entity!=null);
					doh.assertTrue(entity.code!=null);			
					//check starts with to allow repeated tests, also after update
					doh.assertTrue(entity.code.indexOf("code1"==0));
					deferred.callback(true);
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
},
//---------------------------------------------------------------------------------------------------------------
{
	name : "store.put()",
	timeout : 3000,	
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.withStringStore;
				return store.get(1).then(function(entity){
					var version=entity.version;
					var code=entity.code+new Date();
					entity.code=code;
                    store.put(entity).then(function(updated){
                    	//should be read after put, so version is the updated
                    	doh.assertEqual(version+1,updated.version);
                    	doh.assertEqual(code,updated.code);
                    	store.get(1).then(function(reread){
                    		//now read again, just to be sure was persisted
                    		doh.assertEqual(code,reread.code);
                    		doh.assertEqual(version+1,reread.version);                    		
                    	deferred.callback(true);
                    	});
                    });										
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
}
]);