//a proxy for a array of references
//
define('prefabware/plugins/prefabware/store/proxy/ReferenceArrayProxy',
		[ 'dojo'
		  , 'dojo/_base/lang'
		  ,'dojo/_base/array'
		  ,'prefabware/commons/Proxy',
		  'dojo/_base/declare' ],
		  function(dojo, lang,array,Proxy) {
	dojo.declare("prefabware.plugins.prefabware.store.proxy.ReferenceArrayProxy", [prefabware.commons.Proxy], {		
		__store:null,
		__type:null,
		//__isEntity:true,
		constructor : function(type,reference) {
			this.__type=type;
			this.__reference=reference;			
			//add a property length
			//this.defineProperty("length");
			return; 
		},
		__clone : function() {
			//do not clone proxies
			return this;
		},
		__serializable: function(){
		if (this.__isResolved) {
			return this.__resolved.link;
		}
		//do not include unresolved references in POST requests
		return undefined;	
	},
	doResolve: function(reference){
			var that=this;
			prefabware.lang.assert(reference!=null);
			prefabware.lang.assert(reference.href!=null);
			prefabware.lang.assert(reference.store!=null);
			var store=reference.store;
			//expects the store to return an array of entities
			return store.get(null,{target:reference.href}).then(function(result) {
				//the ReferenceArrayProxy must be mixed into an array
				//so 'that' is an array, just add the elements of the result
				array.forEach(result,function(element) {
					that.push(element);
				});
				return result;
			});
	}
});
});
