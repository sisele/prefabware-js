dojo.provide("prefabware.plugins.prefabware.store.spring.tests.cache.CacheTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo._base.array");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.tests.cache.CacheTest", [ 
{
	name : "finder",
	timeout : 1000,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.tests.cache',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.countryType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Country');
			that.cityType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.City');
			that.invoiceType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.Invoice');
			that.invoiceLineType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.InvoiceLine');
			return that.storePlugin.createStore({type:that.countryType}).then(function(store){
				that.countryStore=store;
				return that;
			}).then(function(){
				return that.storePlugin.createStore({type:that.cityType}).then(function(store){
					that.cityStore=store;
					return that;
				}).then(function(){
					return that.storePlugin.createStore({type:that.invoiceType}).then(function(store){
						that.invoiceStore=store;
						return that;
				}).then(function(){
					return that.countryStore.query({name:'Deutschland'}).then(function(result){
						that.countryDE=result[0];
						return that;
					}).then(function(){
						return that.countryStore.query({name:'Spanien'}).then(function(result){
							that.countryES=result[0];
							return that;
						});
					});
				});
				});;
			});;
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		//a cached store should 
		//* be reused, the plugin should not create a new store for every request
		//* resue the cache also on query, as long as the query stays the same
		var deferred=new doh.Deferred();
		prefabware.test.runSetup().then(function(setup){
			var countryType=setup.countryType;
			doh.assertTrue(countryType!=null);
			var store=setup.countryStore;
			doh.assertTrue(store!=null);
			store.__debugId=1;//mark the instance
			var pResult=store.get(1);
			var country=null;
			return pResult.then(function(result){
				doh.assertTrue(result!=null);
				countryGet=result;
				doh.assertTrue(countryGet!=null);
				
				doh.assertTrue(countryGet.pfw_id>0);
				doh.assertTrue(countryGet.__isEntity);
				doh.assertTrue(countryGet.__type===countryType);
				
				var query={name:'*'};
				return store.query(query).then(function(result){
					doh.assertTrue(result!=null);
					country=result[0];
					doh.assertTrue(country!=null);
					store.query(query).then(function(result2){
						var country2=result[0];
						doh.assertEqual(country,country2);
						doh.assertTrue(country===country2);
						return country;
					});
				});
				
			}).then(function(country1){
				//create a second store 
				setup.storePlugin.createStore({type:setup.countryType}).then(function(store2){
					//should be the SAME as before because the extension is cached
					doh.assertTrue(store!=store2,"the stores should not be the same, createStore should return a new instance");
					deferred.callback(true);
					return store2;
				}).then(function(store2){
					var query2={name:'*'};
					return store2.query(query2).then(function(result){
						doh.assertTrue(result!=null);
							var country2=result[0];
							doh.assertEqual(country,country2);
							doh.assertTrue(country===country2);
							return country;
					});
				});;	
				
			});;
		});
		return deferred;
	},
	tearDown : function() {
	}
},

]);