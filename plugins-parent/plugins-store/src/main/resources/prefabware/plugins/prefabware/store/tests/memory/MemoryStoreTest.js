dojo.provide("prefabware.plugins.prefabware.store.tests.memory.MemoryStoreTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.tests.memory.MemoryStoreTest", [ 
{
	name : "query MemoryStore",
	timeout:1000,
	registry : null,
	
	extensionPoint : null,
	extension : null,
	doSetup : function() {
		var that=this;
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.tests.memory',
			fileName : 'memorystoreTest-registry.json'
		});
		
		this.registry.startup().then(function(registry){
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
		
			that.cityType = that.modelPlugin.findType("com.prefabware.js.plugins.store.jpa.City");;
			that.countryType = that.modelPlugin.findType("com.prefabware.js.plugins.store.jpa.Country");;
			doh.assertNotEqual(null,that.cityType);
			doh.assertNotEqual(null,that.countryType);
			
//		var cityType = new prefabware.model.EntityType('test.City');
//		that.cityType=cityType;
//		cityType.addAttribute({
//			name : 'name',
//			type : that.typeRegistry.STRING,
//			length : 30,
//		});
//		cityType.addAttribute({
//			name : 'pfw_id',
//			type : that.typeRegistry.STRING,
//			length : 30,
//		});
//		that.typeRegistry.registerType(cityType);
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var setup=prefabware.test.setup;
		var cityType=setup.cityType;
		var countryType=setup.countryType;
			
			setup.storePlugin.createStore({type:cityType}).then(function(store){
				doh.assertTrue(store.isMemoryStore,'store should be a MemoryStore');
				doh.assertFalse(store.notify==null,'Obervable expected');
				var pResult=store.query({name:"Cologne"});
				doh.assertFalse(pResult.observe==null,'Obervable expected');
				pResult.then(function(results){
					doh.assertNotEqual(null,results);
					doh.assertEqual(1,results.length);
					
					doh.assertNotEqual(results.forEach,'QueryResult expected');
					
					var city=results[0];					
					doh.assertTrue(city.__isEntity);
					doh.assertEqual('Cologne',city.name);
					doh.assertEqual(4711,city.pfw_id);
					var country=city.country;
					doh.assertNotEqual(null,country);
					doh.assertTrue(country.__isEntity);
					doh.assertEqual(countryType,country.__type);
					city.__resolveProxies().then(function(){
						doh.assertEqual('Deutschland',country.name);
						
					});;
				});
					testDeferred.callback(true);
			});
			
		return testDeferred;
			},
	tearDown : function() {
	}
},
{
	name : "Plugin.createMemoryStore",
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var setup=prefabware.test.setup;
		var cityType=setup.cityType;
		
		setup.storePlugin.createMemoryStore(cityType).then(function(store){
			var madrid=cityType.create();
			madrid.name='Madrid';
			doh.assertTrue(madrid.pfw_id<0);
			madrid=store.put(madrid).then(function(){
				doh.assertEqual(cityType,madrid.__type);
				doh.assertTrue(madrid.pfw_id>0);
				testDeferred.callback(true);
			});;
		});
		
		return testDeferred;
	},
	tearDown : function() {
	},
}, 
{
	name : "query generic MemoryStore",
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var setup=prefabware.test.setup;
		var cityType=setup.cityType;
			
		setup.storePlugin.matchExtension('prefabware.store.store',{type:cityType}).then(function(extension){
			doh.assertTrue(extension!=null);
			doh.assertEqual('prefabware.plugins.prefabware.store.ExtensionMemoryStore',extension.declaredClass);
			
			setup.storePlugin.createStore({type:cityType}).then(function(store){
				
				store.query({name:"Berlin*"}).then(function(results){
					doh.assertNotEqual(null,results);
					doh.assertEqual(2,results.length);
					testDeferred.callback(true);
					doh.assertNotEqual(results.forEach,'QueryResult expected');
				});
			});
			
		});
			
		return testDeferred;
	},
	tearDown : function() {
	},
}, 
{
	name : "put MemoryStore",
	setUp : function() {
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var setup=prefabware.test.setup;
		var cityType=setup.cityType;
		
		setup.storePlugin.createStore({type:cityType}).then(function(store){
			var madrid=cityType.create();
			madrid.name='Madrid';
			doh.assertTrue(madrid.pfw_id<0);
			madrid=store.put(madrid).then(function(){
				doh.assertEqual(cityType,madrid.__type);
				doh.assertTrue(madrid.pfw_id>0);
				testDeferred.callback(true);
			});;
		});
		
		return testDeferred;
	},
	tearDown : function() {
	},
} 
]);