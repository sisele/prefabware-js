dojo.provide("prefabware.plugins.prefabware.store.tests.StoreTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.tests.StoreTest", [ {
	name : "byid",
	timeout : 1000,
	registry : null,

	extensionPoint : null,
	extension : null,
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.tests',
			fileName : 'storeTest-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);

			var cityType = new prefabware.model.EntityType('test.City');
			that.cityType=cityType;
			cityType.addAttribute({
				name : 'name',
				type : that.typeRegistry.STRING,
				length : 30,
			});
			cityType.addAttribute({
				name : 'pfw_id',
				type : that.typeRegistry.STRING,
				length : 30,
			});
			that.typeRegistry.registerType(cityType);
			that.typeRegistry.startup();

			that.storePlugin = that.registry.findPlugin('prefabware.store');
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred = new doh.Deferred();
		prefabware.test.runSetup().then(function(){
		var setup=prefabware.test.setup;
		var cityType=setup.cityType;
		return setup.storePlugin.createStore({type:cityType}).then(
				function(store) {
					var pResult = store.get(1);
					doh.assertNotEqual(null, pResult);
					pResult.then(function(result) {
						var city = result;
						doh.assertEqual(1, city.pfw_id);
						doh.assertEqual('Berlin', city.name);
						testDeferred.callback(true);
					});
					;
				});

		});
		return testDeferred;
	},
	tearDown : function() {
	}
},
{name : "get not found",
	 timeout : 1000,
		runTest : function() {
			var testDeferred = new doh.Deferred();
			prefabware.test.runSetup().then(function(){
			var setup=prefabware.test.setup;
			var cityType=setup.cityType;
			return setup.storePlugin.createStore({type:cityType}).then(
					function(store) {
						var pResult = store.get(9999999);
						
						doh.assertNotEqual(null, pResult);
						pResult.then(function(result) {
							doh.assertEqual(null, result);
							testDeferred.callback(true);
						});
						;
					});

			});
			return testDeferred;
		},
		tearDown : function() {
		}
},
{name : "query",
 timeout : 1000,
	runTest : function() {
		var testDeferred = new doh.Deferred();
		prefabware.test.runSetup().then(function(){
		var setup=prefabware.test.setup;
		var cityType=setup.cityType;
		return setup.storePlugin.createStore({type:cityType}).then(
				function(store) {
					var pResult = store.query(
							{name : "Frankfurt"},{urlPath:'city/search/findByName'});
					doh.assertNotEqual(pResult.forEach,'promise of a QueryResult expected');
					doh.assertNotEqual(null, pResult);
					pResult.then(function(result) {
						doh.assertNotEqual(result.forEach,'QueryResult expected');
						var city = result[0];
						doh.assertEqual(2, city.pfw_id);
						doh.assertEqual('Frankfurt', city.name);
						testDeferred.callback(true);
					});
					;
				});

		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);