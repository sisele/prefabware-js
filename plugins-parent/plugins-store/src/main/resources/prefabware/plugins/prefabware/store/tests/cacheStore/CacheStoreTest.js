dojo.provide("prefabware.plugins.prefabware.store.tests.cacheStore.CacheStoreTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.tests.cacheStore.CacheStoreTest", [ 
{
	name : "cacheStoreTest, to test that createStore is cached",
	timeout:1000,
	registry : null,
	
	extensionPoint : null,
	extension : null,
	doSetup : function() {
		var that=this;
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.tests.cacheStore',
			fileName : 'plugin-registry.json'
		});
		
		this.registry.startup().then(function(registry){
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
		
			that.cityType = that.modelPlugin.findType("com.prefabware.js.plugins.store.jpa.City");;
			that.countryType = that.modelPlugin.findType("com.prefabware.js.plugins.store.jpa.Country");;
			doh.assertNotEqual(null,that.cityType);
			doh.assertNotEqual(null,that.countryType);
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},	
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var setup=prefabware.test.setup;
		var cityType=setup.cityType;
		var first=0;
		var second=0;
			setup.storePlugin.createStore({type:cityType}).then(function(store){
				first=store;
			}).then(function(){
			return setup.storePlugin.createStore({type:cityType}).then(function(store){
				second=store;
				doh.assertTrue(first===second,"should be the same instance as the first");
				testDeferred.callback(true);
				})	;
			});
			
		return testDeferred;
			},
	tearDown : function() {
	}
}
]);