//a Plugin
define('prefabware/plugins/prefabware/store/Plugin', [ 'dojo', 
		'dojo/_base/declare',
		'prefabware/plugin/Plugin' ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.store.Plugin",
			[ prefabware.plugin.Plugin ], {
				id : null,
				globalCache:null,//cache for all stores
				storeId:0,
				servicePlugin:{inject:'prefabware.service',type:'plugin'},   
				constructor : function() {
				},
				startup : function() {
					this.globalCache={};
					this.globalCache.map = new dojox.collections.Dictionary();
				},
				createStore : function(options) {
					prefabware.lang.assert(options!=null,'type must not be null');
					var type=options.type;
					prefabware.lang.assert(type!=null,'type must not be null');
					//async
					var that=this;
					return this.matchExtension('prefabware.store.store',options).then(function(extension){
						if (extension==null) {
							prefabware.lang.throwError('cannot find extension for store for type '+type.name);
						}else{ 
							var store= extension.createStore(options);
							that.storeId++;
							store.storeId=that.storeId;
							return store;
						};
					});
				},
				createMemoryStore : function(type) {
					//async
					//creates a memorystore for options.type
					//uses a special extension for that
					return this.fetchExtensionById('prefabware.store.store.intern.MemoryStore').then(function(ext){
						return ext.createStore({type:type});
					});
				},
				
			});
});
