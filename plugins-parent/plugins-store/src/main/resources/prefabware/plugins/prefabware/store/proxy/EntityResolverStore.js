//to resolve a reference
//uses a store to resolve the a reference
//needs the id and the type
//the returned entity may than itself contain unresolved references
define('prefabware/plugins/prefabware/store/proxy/EntityResolverStore'
		, [ 'dojo'
          , 'dojo/_base/declare'
          ,'prefabware/lang'
                                 ], function(dojo) {
	dojo.declare("prefabware.plugins.prefabware.store.proxy.EntityResolverStore", null, {
		storePlugin:null,
		constructor : function(storePlugin) {
			this.storePlugin=storePlugin;
		},
			resolve : function(reference) {
				//async!
				//expected : reference.id = the id
				//expected : reference.type = the type of the entity
				//returns a entity
				prefabware.lang.assert(reference!=null);
				prefabware.lang.assert(reference.id!=null);
				prefabware.lang.assert(reference.type!=null);
				
				var id=reference.id;
				var type=reference.type;
				return this.storePlugin.createStore({type:type}).then(
						function(store) {
							var pResult = store.get(id);
							return pResult.then(function(result) {
								return result;
							});
							;
						});
			}
	});
});

