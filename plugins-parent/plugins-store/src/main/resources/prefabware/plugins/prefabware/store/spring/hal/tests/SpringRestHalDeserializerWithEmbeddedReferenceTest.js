dojo.provide("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithEmbeddedReferenceTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("prefabware.test");
dojo.require("dojo");
dojo.require("prefabware.plugins.prefabware.store.spring.SpringRestSerializer");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.store.spring.hal.tests.SpringRestHalDeserializerWithEmbeddedReferenceTest", [ 
{
	name : "SpringRestHalDeserializer",
	timeout : 500,
	
	doSetup : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.store.spring.hal.tests',
			fileName : 'plugin-registry.json'
		});
		var that = this;

		return this.registry.startup().then(function(registry) {
			that.modelPlugin = registry.findPlugin('prefabware.model');
			that.storePlugin = registry.findPlugin('prefabware.store');
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertTrue(that.typeRegistry != undefined);
			that.storePlugin = that.registry.findPlugin('prefabware.store');
			
			that.entityType=that.modelPlugin.findType('com.prefabware.js.plugins.store.jpa.WithEmbeddedReference');
			
			return that.storePlugin.createStore({type:that.entityType}).then(function(store){
				that.entityStore=store;
				return that;
			}).then(function(that){
				return that;
			});
		});
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
		prefabware.test.runSetup();
	},		
	runTest : function() {
		var deferred=new doh.Deferred();
		this.setup=prefabware.test.runSetup().then(function(setup){
			var store=setup.entityStore;
				return store.get(1).then(function(entity){
					doh.assertTrue(entity!=null);
					doh.assertEqual("withEmbeddedReference1",entity.code1);
					doh.assertTrue(entity.embeddedWithReference.__isComposite);					
					doh.assertTrue(entity.embeddedWithReference.one.__isComposite);
					doh.assertTrue(entity.embeddedWithReference.one.__isProxy);
					doh.assertEqual("http://localhost:8080/withString/1",entity.embeddedWithReference.one.__reference.href);
					return entity.embeddedWithReference.one.__resolveProxies().then(function(){
						doh.assertEqual("code1",entity.embeddedWithReference.one.code);
						doh.assertEqual(1,entity.embeddedWithReference.one.pfw_id);
						deferred.callback(true);
					});
				});
		});
		return deferred;
	},
	tearDown : function() {
	}
}
]);