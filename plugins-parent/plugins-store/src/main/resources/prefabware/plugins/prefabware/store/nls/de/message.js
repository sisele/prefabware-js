define({
	DLT0001:{title:'{entity} gelöscht',text:'{entity} wurde erfolgreich gelöscht'},
	DLT0002:{title:'Löschen von {entity} fehlgeschlagen !',text:'Konnte {entity} nicht löschen'},
	DLT0003:{title:'Löschen bestätigen !',text:'Wollen Sie {entity} wirklich löschen'},
	SAV0001:{title:'{entity} gespeichert',text:'{entity} wurde erfolgreich gespeichert'},
	SAV0002:{title:'Speichern fehlgeschlagen !',text:'{entity} konnte nicht gespeichert werden.'},
});