//TODO not in use
define('prefabware/plugins/prefabware/store/ExtensionStore', [ 
       'dojo', 
       "dojo/_base/lang",
       'prefabware/lang',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo,lang) {
	dojo.declare("prefabware.plugins.prefabware.store.ExtensionStore", [prefabware.plugin.Extension], {
		type :null,
		cacheCreateStore:true,//calls to createStore are cached, the same instance will be used many times
		constructor : function(definition) {			
			if (this.json.cacheCreateStore!=undefined) {
				this.cacheCreateStore=this.json.cacheCreateStore;
			}
			if (this.cacheCreateStore) {
				this.__createStoreCache=prefabware.commons.Cache(this,['createStore'],lang.hitch(this, '__createStoreCacheKey'));
			}
		},	
		__createStoreCacheKey : function(options) {
			//one store for one type
			return options.type.name;
		},
	});
	
});
