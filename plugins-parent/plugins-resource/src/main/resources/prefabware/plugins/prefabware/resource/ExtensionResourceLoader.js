//to load resources like scripts and css 
define('prefabware/plugins/prefabware/resource/ExtensionResourceLoader', [ 
       'dojo',
       'prefabware/plugin/Extension',
       'prefabware/util/ResourceI18n',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.resource.ExtensionResourceLoader", [prefabware.plugin.Extension], {
		loaderClass:null,//the classname of the ResourceLoader
		resourceLoader:null,//the resource loader
		constructor : function(options) {
			this.loaderClass=options.json.loaderClass;
		},	
		startup : function() {
			//async
			//create the instance of the resource loader
			var that=this;
			var p=this.loader.createInstance(this.loaderClass)
				.then(function(instance){
					that.resourceLoader=instance;
				});
			return p;
		},
		load : function(path) {
			//loads the resource asynchronusly			
			//path is the path to the resource relative to dojo.js, using '/' as path seperator
			//path can be a string or an array of strings
			return this.resourceLoader.load(path);
		},
	});
});
