// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugins.prefabware.resource.tests.fixLocale.FixLocaleTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
dojo.require("dojo.Deferred");
doh.register("prefabware.plugins.prefabware.resource.tests.fixLocale.FixLocaleTest", [ 
{
	name : "fixLocale test",
	coment : "to test a resource extension with a fixed language",
	timeout:1000,
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.resource.tests.fixLocale',fileName:'fixLocaleTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var resourcePlugin=that.registry.findPlugin('prefabware.resource');
			doh.assertEqual(resourcePlugin.declaredClass,"prefabware.plugins.prefabware.resource.Plugin");
			resourcePlugin.getLocalization('greeting','label').then(function(label){
				doh.assertEqual("Hola mundo !",label);
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);