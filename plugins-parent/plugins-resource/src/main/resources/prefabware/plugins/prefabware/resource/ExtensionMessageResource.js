//to load a dojo i18n resource
// see http://dojotoolkit.org/reference-guide/1.9/dojo/i18n.html
define('prefabware/plugins/prefabware/resource/ExtensionMessageResource', [ 
       'dojo',
       'dojo/_base/array',
       'prefabware/plugins/prefabware/resource/ExtensionI18nResource',
       'prefabware/plugins/prefabware/resource/message/Message',
       'prefabware/util/ResourceJsonFile',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, array) {
	dojo.declare("prefabware.plugins.prefabware.resource.ExtensionMessageResource", [prefabware.plugins.prefabware.resource.ExtensionI18nResource], {
		//each extension needs 
		//a message.js, maybe in many languages
		//this.resource is defined in ExtensionI18nResource
		messageFile:null,//a message.json
		constructor : function(options) {			
			this.messageDefs=new dojox.collections.Dictionary();
		},	
		startup : function() {
			// async
			var that=this;
			return this.inherited(arguments).then(function(){
				that.messageFile = new prefabware.util.ResourceJsonFile({namespace:that.namespace,name:'message.json'});
			}).then(function(){
				var json=that.messageFile.getContentAsJson();
				array.forEach(json.messages,function(messageDef){
					that.messageDefs.add(messageDef.id,messageDef);
				},that);
			});
		},
		__findMessageDefinition : function(id) {
			return this.messageDefs.item(id);
		},
		
		createMessage : function(id,msgParms) {
			var json=this.__findMessageDefinition(id);
			if (json==null||json==undefined) {
				return null;
			}
			var localized=this.getLocalization(id);
			var message = new prefabware.plugins.prefabware.resource.message.Message(
					{id:id,icon:json.icon,title:localized.title,text:localized.text,msgType:json.msgType,msgParms:msgParms});
			message.replace();
			return message;
		},
	});
});
