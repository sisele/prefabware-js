// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugins.prefabware.resource.tests.script.ScriptTest");
//require all plugins 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugins.prefabware.resource.tests.script.ScriptTest", [ 
{
	name : "script test",
	timeout:4000,
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.resource.tests.script',fileName:'scriptTest-registry.json'});
	},
	runTest : function() {
	var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var resourcePlugin=that.registry.findPlugin('prefabware.resource');
				doh.assertEqual(null,window.prefabware_plugins_prefabware_resource_tests_script_ScriptTest_counter);
			resourcePlugin.loadScript('prefabware/plugins/prefabware/resource/tests/script/script1.js').then(function(p){
				doh.assertFalse(null==window.prefabware_plugins_prefabware_resource_tests_script_ScriptTest_counter);
				testDeferred.callback(true);
			});
		});
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);