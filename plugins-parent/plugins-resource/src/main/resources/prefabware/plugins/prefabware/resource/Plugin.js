//a Plugin
define('prefabware/plugins/prefabware/resource/Plugin', [ 'dojo', 
		'dojo/_base/array', 'dojo/_base/lang','dojo/promise/all',
		'prefabware/lang',
		'prefabware/commons/Cache',
		'dojo/_base/declare',
		'prefabware/plugin/Plugin' ], function(dojo, array, lang,all) {
	dojo.declare("prefabware.plugins.prefabware.resource.Plugin",
			[ prefabware.plugin.Plugin ], {
		__locCache:null,
		startup : function() {
			//TODO as soon as options are used, they need a toString-method !
			this.__locCache=prefabware.commons.Cache(this,['getLocalization']);
		},
		_sanitized : function(key) {
			//if the key is reserved word, apend a '_' to make it a valid key
			// e.g. 'delete' will be 'delete_'
			if (prefabware.lang.isReservedWord(key)) {
				return key+'_';
			}else{
				return key;
			}
		},
		getLocalization : function(key,name,options) {
			//async
			var that=this;
			prefabware.lang.assert(key!=null,'key must not be null');
			prefabware.lang.assert(name!=null,'name must not be null');
			if (options==null){
				options={};
			};
			if (dojo.isArrayLike(key)) {
				return that.__manyLocalizations(key,name,options); 
			}else{
				return that.__oneLocalization(key,name,options);
			}
		},
		__oneLocalization : function(key,name,options) {
			//async
			//returns the localized text for the key, which must be a string
			var that=this;
			var sKey=this._sanitized(key);//a healthy key name, that is not a reserved word
			//to get a localized string for the given key
			//returns the key if no localization can be found
			var label=key;
			return this.matchExtensions('prefabware.resource.resource',{value:name}).then(function(extensions){
				array.some(extensions,function(extension){
					label=extension.getLocalization(sKey);
					return label!=null;//returning true=something was found, break the loop
				});
				if (label!=null) {
					return label;
				}else{
					//return the key as a default
					return key;
				}
			});	
		},
		__manyLocalizations : function(keys,name,options) {
			//async
			//returns an object with the localized textes for the given keys
			//key must be an array with the keys e.g. ['name','street','city']
			//the result will be an object like {'name':'Name','street':'Straße','city':'Stadt'}
			var result={};
			var promises=array.map(keys,function(key){
				return this.__oneLocalization(key,name,options).then(function(text){
					result[key]=text;
				});
			},this);
			return all(promises).then(function(){return result});
		},
		loadScript : function(path) {
			return this.fetchExtensionById("prefabware.resource.loader.script")
				.then(function(extension){
					return extension.load(path);
				});
		},
		loadCss : function(path) {
			return this.fetchExtensionById("prefabware.resource.loader.css")
			.then(function(extension){
				return extension.load(path);
			});
		},
		createMessage : function(key,msgParms) {
			prefabware.lang.assert(key!=null,'key must not be null');
			var sKey=this._sanitized(key);//a healthy key name, that is not a reserved word
			//async
			var that=this;
			//to create a message for the given key
			//the message will be localized, if possible
			var message=null;
			return this.matchExtensions('prefabware.resource.message',{value:'message'}).then(function(extensions){
				array.some(extensions,function(extension){
					message=extension.createMessage(sKey,msgParms);
					return message!=null;//returning true=something was found, break the loop
				});
				if (message!=null) {
					return message;
				}else{
					//TODO return a default message
					return {text:'no message found with key '+sKey};
				}
			});					
		},
	 });
});
