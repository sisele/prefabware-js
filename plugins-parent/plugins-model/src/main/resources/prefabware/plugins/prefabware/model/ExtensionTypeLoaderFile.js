//a typeloader loading types from resources
//you can use this extension for  
//extensionPoint prefabware.model.typeLoader if you want to load types from files only.
//than this will be the only typeLoader of the application
//or
//extensionPoint prefabware.model.typeLoaderFile to create one of many type loaders that than may be registered to
//the typeLoader of the application
define('prefabware/plugins/prefabware/model/ExtensionTypeLoaderFile', [ 'dojo',
                                        'dojo/_base/lang',
                                        'dojo/_base/array',
                                        'prefabware/model/to/TypeLoaderFile',
                                        'prefabware/plugin/Extension',
                                        'prefabware/util/ResourceJsonFile',
                                        'dojo/_base/declare' ], function(
		dojo, lang,array) {
	dojo.declare('prefabware.plugins.prefabware.model.ExtensionTypeLoaderFile', [prefabware.plugin.Extension], {
		typeLoader:null,
		constructor : function(options) {
			var that=this;
			this.typeLoader=new prefabware.model.to.TypeLoaderFile();
			array.forEach(options.json.resources,function(jsResource){
				var resource=new prefabware.util.ResourceJsonFile({qualifiedName:jsResource.resource});
				that.typeLoader.addJsonResource(resource);
			});
		},
		getTypeLoader : function() {
			return this.typeLoader;
		},		
		
	});
});

