//to get access to the TypeRegistry 
define('prefabware/plugins/prefabware/model/TypeRegistryAccessMixin', [ 
       'dojo',
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/_base/Deferred',
       'prefabware/model/TypeRegistry',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.model.TypeRegistryAccessMixin", null, {
		modelPlugin:{inject:'prefabware.model',type:'plugin'},//will be injected
		typeRegistry:null,
		typeResolver:null,
		getTypeRegistry : function() {
			if (this.typeRegistry==null) {			
				this.typeRegistry=this.modelPlugin.getTypeRegistry();
			}
			return this.typeRegistry;
		},		
		getTypeResolver : function() {
			if (this.typeResolver==null) {			
				this.typeResolver=this.modelPlugin.getTypeResolver();
			}
			return this.typeResolver;
		},	
			
	});
});
