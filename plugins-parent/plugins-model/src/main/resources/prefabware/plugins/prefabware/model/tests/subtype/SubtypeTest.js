// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugins.prefabware.model.tests.subtype.SubtypeTest');
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugins.prefabware.model.tests.subtype.SubtypeTest', [ {
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.model.tests.subtype',
			fileName : 'subtypeTest-registry.json'
		});
	},
	runTest : function() {
		var testDeferred = new doh.Deferred();
		var that=this;
		
		this.registry.startup().then(function(registry){
			var modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			var typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertFalse(typeRegistry==null);
			
			var STRING = typeRegistry.STRING;
			var DECIMAL = typeRegistry.DECIMAL;
			var BOOLEAN = typeRegistry.BOOLEAN;
			var TYPE = typeRegistry.TYPE;
			var COMPOSITE = typeRegistry.COMPOSITE;
			var ENTITY = typeRegistry.ENTITY;

			var partyType = new prefabware.model.EntityType('test.Party');
			partyType.addAttribute({
				name : 'name',
				type : STRING,
				length : 30,
			});
			var personType = new prefabware.model.EntityType('test.Person');
			personType.superType=partyType;
			personType.addAttribute({
				name : 'firstName',
				type : STRING,
				length : 30,
			});
			typeRegistry.registerType(partyType);
			typeRegistry.registerType(personType);
			
			doh.assertEqual( partyType,typeRegistry.findType('test.Party'));
			doh.assertEqual( personType,typeRegistry.findType('test.Person'));
			doh.assertEqual( partyType,personType.superType);
			doh.assertTrue( partyType.isSuperTypeOf(personType));
			doh.assertEqual( 1,partyType.superTypeDistance(personType));
			doh.assertEqual( 2,ENTITY.superTypeDistance(personType));
			doh.assertEqual( 1,ENTITY.superTypeDistance(partyType));
			doh.assertEqual( ENTITY,partyType.superType);
			doh.assertTrue( ENTITY.isSuperTypeOf(partyType));
			
			var testPlugin = registry.findPlugin('prefabware.model.tests.subtype');
			var p1=testPlugin.matchExtension('prefabware.model.tests.subtype.converter',{type:partyType})
				.then(function(extension){
					doh.assertTrue(extension!=null);
				});
			var p2=testPlugin.matchExtension('prefabware.model.tests.subtype.converter',{type:personType})
			.then(function(extension){
				doh.assertTrue(extension!=null);
			});
			
			dojo.promise.all(p1,p2)
				.then(function(){
					testDeferred.callback(true);
				});
		});
		
	return testDeferred;
	},
	tearDown : function() {
	}
} ]);