//to accept a type 
//you may need to use prefabware.plugins.prefabware.model.TypeRegistryAccessMixin as well !
//or code your own method getTypeRegistry()
define('prefabware/plugins/prefabware/model/TypeAcceptorMixin', [ 
       'dojo',
       'dojo/_base/array',
       'dojo/json',
       'prefabware/plugins/prefabware/model/TypeRegistryAccessMixin',
       'dojo/_base/Deferred',
       'dojo/_base/declare' ], function(
		dojo, array,JSON) {
	dojo.declare("prefabware.plugins.prefabware.model.TypeAcceptorMixin", [prefabware.plugins.prefabware.model.TypeRegistryAccessMixin], {
		__type:null,
		getType : function() {
			this.__assertJsonType();
			//json.type may be generic, containing a '*'
			//in that case its impossible to load the type
			var type=null;
			if (this.json.type!==undefined) {
				if (this.json.type.indexOf('*')>=0) {
					return null;
				}
				type=this.getTypeRegistry().findType(this.json.type);
			}
			return type;
		},
		__assertJsonType : function() {
			if (this.json.type==undefined) {
				throw 'no type found in json of '+this.declaredClass+' id='+this.id+'\n json='+JSON.stringify(this.json);
			}
		},
		acceptsType : function(type) {
			this.__assertJsonType();
			// returns true, if this extension can handle the given type
		    //if the typeResolver considers this.json.type matches type
			//this extension can handle it
			return this.getTypeResolver().resolve(type,[this.json.type])!=null;
		},
	});
});
