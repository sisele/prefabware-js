//to customize the types of the TypeRegistry
//TODO its customizes attributes, not types !
define('prefabware/plugins/prefabware/model/ExtensionTypeCustomizer', [ 
       'dojo','dojo/_base/lang','dojo/_base/array',
       'prefabware/plugin/Extension',
       'dojox/collections/Dictionary',
       'dojo/_base/declare' ], function(
		dojo, lang,array,Extension) {
	dojo.declare("prefabware.plugins.prefabware.model.ExtensionTypeCustomizer",[prefabware.plugin.Extension], {
		options:{},
		instanceClass:null,
		constructor : function(id) {
		},
		startup : function() {
			if (this.json.options!=undefined) {
				this.options=this.json.options;
			}
			this.typeClass     = this.json.typeClass;
			var instanceClassName = this.json.instanceClass;
			if (instanceClassName!=null) {
				var that=this;
				//return the promise
				return this.loader.load(instanceClassName).then(function(clazz){
					that.instanceClass=clazz;
				});
			}else{
				//or nothing
				return;
			}

		},
		customize : function(jsonType) {
			//customizes the options that are used to create the type
			//jsonTypes has a property 'attributes' that contains
			//an array atribute definitions.
			//this.options may also have a property 'attributes' that contains the ATTRIBUTES NAMES ONLY
			//sort the attributes here in the order that this.options.attributes dictates
			var customized=lang.mixin(lang.clone(jsonType),this.options);
			if (this.options.attributes!=undefined) {
				//build a map of the attributes keyed by name
				var map=new dojox.collections.Dictionary();
				array.forEach(jsonType.attributes,function(attribute){
					map.add(attribute.name,attribute);
				});
				customized.attributes=array.map(this.options.attributes,function(name){
					return map.item(name);
				});
			}
			//mixin the options declared for this extension
			return customized;
		},
		acceptsType : function(type) {
			//type is a string here !!!
			//only exact matches are allowed
			return this.json.type==type;
		},
		getCustomTypeClass:function(typeName){
			if (this.typeClass==null) {
				return null;
			}
			return this.getClassforName (this.typeClass);
		},
		getCustomInstanceClass:function(typeName){
			//TODO the parameter is useless here !
			return this.instanceClass;
		},
	});
});
