// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugins.prefabware.model.tests.service.TypeLoaderTest');
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugins.prefabware.model.tests.service.TypeLoaderTest', [ {
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.model.tests.service',
			fileName : 'typeLoaderTest-registry.json'
		});
	},
	runTest : function() {
		var testDeferred = new doh.Deferred();
		var that=this;
		
		this.registry.startup().then(function(registry){
			var modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(modelPlugin==null);
			that.typeRegistry = modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);

			var withSuperT=that.typeRegistry.findType('com.prefabware.js.plugins.model.type.WithSuperType');
			doh.assertTrue(withSuperT!=null);
			var withSuper=withSuperT.create();
			doh.assertTrue(withSuper!=null);
			doh.assertTrue(withSuperT.superType!=null);
			doh.assertTrue(withSuperT.superType.name!=null);
			
			var withStringT=that.typeRegistry.findType('com.prefabware.js.plugins.model.type.WithString');
			doh.assertTrue(withStringT!=null);
			var withString=withStringT.create();
			doh.assertTrue(withString!=null);
			
			var withDateT=that.typeRegistry.findType('com.prefabware.js.plugins.model.type.WithDate');
			doh.assertTrue(withDateT!=null);
			var withDate=withDateT.create();
			doh.assertTrue(withDate!=null);
			
			
			var currencyT=that.typeRegistry.findType('com.prefabware.business.commons.Currency');
			doh.assertTrue(currencyT!=null);
			var currency=currencyT.create();
			doh.assertTrue(currency!=null);
			
			testDeferred.callback(true);
		});
		
	return testDeferred;
	},
	tearDown : function() {
	}
} ]);