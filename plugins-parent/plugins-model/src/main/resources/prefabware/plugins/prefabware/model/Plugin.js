//a Plugin
define('prefabware/plugins/prefabware/model/Plugin', [ 
       'dojo','prefabware/plugin/Plugin','dojo/_base/array',
       'dojo/aspect',
       'dojo/promise/all',
       'prefabware/model/TypeRegistry',
       'prefabware/model/TypenameResolver',
       'prefabware/plugins/prefabware/model/ExtensionAttributeCustomizer',
       'prefabware/plugins/prefabware/model/ExtensionTypeCustomizer',
       'prefabware/plugins/prefabware/model/ExtensionTypeLifecycle',
       'dojo/_base/declare' ], function(
		dojo, Plugin,array,aspect,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.model.Plugin", [prefabware.plugin.Plugin], {
		typeRegistry:null,
		typeResolver:null,
		afterStartup : function() {
			this.typeRegistry=new prefabware.model.TypeRegistry();
			this.typeResolver=new prefabware.model.TypenameResolver(this.typeRegistry);		
			var plugin=this;
			//the typeimporter does not know about plugins and extension
			//the the extensions here are connected bay aspects
			aspect.after(this.typeRegistry.importer,"customizeTypeOptions",dojo.hitch(plugin,plugin.__customizeTypeOptions),true);
			aspect.after(this.typeRegistry.importer,"customizeAttributeOptions",dojo.hitch(plugin,plugin.__customizeAttributeOptions),true);
			aspect.after(this.typeRegistry.importer,"getCustomTypeClass",dojo.hitch(plugin,plugin.__getCustomTypeClass),true);
			aspect.after(this.typeRegistry.importer,"getCustomInstanceClass",dojo.hitch(plugin,plugin.__getCustomInstanceClass),true);
			//attach lifecycle listeners to registry
			this.typeRegistry.on('registered',function(event){
				//attach lifecycle listeners to registered type
				event.type.on('defaultsApplied',dojo.hitch(plugin,plugin.__defaultsApplied));
				event.type.on('changesApplied',dojo.hitch(plugin,plugin.__changesApplied));
			});
			var p1=this.fetchExtension('prefabware.model.typeLoader').then(function(ext){
				if (ext!=undefined) {
					//loader is optional
					plugin.getTypeRegistry().loader=ext.getTypeLoader();
				}
			});
			
			var p2=this.fetchExtensions('prefabware.model.types').then(function(extensions){
				array.forEach(extensions,function(extension){
					extension.instance.registerAt(this.typeRegistry);
				},this);
			});
			return promiseAll(p1,p2);
		},
		applyBehaviour: function(options) {
			//options.entity
			//options.event
			//returns {entity:entity}
			//the matcher of the extension excpects a property 'value'
			var bo={
					value:{type:options.entity.__type.name
						  ,sender:options.event.sender
						  ,trigger:options.event.trigger
						  }};
			var f=function(extension){
				//this function will be called for every extension found
				return extension.apply(options);
			};
			
			return this.matchExtensions('prefabware.model.behaviour',bo).then(function(ext){
				if (ext.length>0) {
					//extensions mu,st be applied one after the other
					//so we cannot use promise.all which would execute them inpredictable and may be parralel 
					return prefabware.lang.deferredList(ext,f);
				}else{
					//nothing to do just return the given entity
					return prefabware.lang.deferredValue({entity:options.entity});
				}
				});
		},
		getTypeLifecycle : function(type) {
			var pointId='prefabware.model.type.lifecycle';
			var extension=this.findExtensionForType(type,pointId);					
			return extension;
		},
		getTypeRegistry : function() {
			return this.typeRegistry;
		},		
		findType : function(name) {
			//sync !!
			return this.getTypeRegistry().findType(name);
		},	
		getTypeResolver : function() {
			return this.typeResolver;
		},	
		__defaultsApplied:function(event){
			//is called after defaults for the type were applied
			//event ={instance:instance,type:type,...};
			this.matchExtensions('prefabware.model.type.lifecycle',this.__optionsType(event.type)).then(function(ext){
				if (ext.length>0) {
					array.forEach(ext,function(extension){
						extension.defaultsApplied(event);
					});
				}
			});
			return;
		},
		__optionsType:function(typeOrName){
			//returns the options to use for matchExtension by typename
			//to keep extensions as lazy as possible, the matcher use typenames
			//and not types. otherwise we would have to create the type of all
			//matchers, just to match !
			if (typeOrName instanceof prefabware.model.Type) {
				return {value:typeOrName.name};
			} else {
				return {value:typeOrName};
			}
		},
		__changesApplied:function(event){
			//is called after changes to the instance of the type were applied
			//event ={instance:instance,type:type,...};
			this.fetchExtensions('prefabware.model.type.lifecycle',this.__optionsType(event.type)).then(function(ext){
			if (ext.length>0) {
				array.forEach(ext,function(extension){
					extension.defaultsApplied(event);
					extension.changesApplied(event);
				});
			}
			});
			return;
		},
		__getTypeCustomizer:function(type){
			var result=null;
			this.matchExtension('prefabware.model.typeCustomizer',{type:type}).then(function(ext){
				if (ext!=null) {
					result= ext;
				}
				});
			return result;
		},
		__getCustomTypeClass:function(type){
			var customizer=this.__getTypeCustomizer(type);
			if (customizer!=null) {
				return customizer.getCustomTypeClass(type.name);}			
			else{
				return null;
			}			
		},
		__getCustomInstanceClass:function(type){
			var customizer=this.__getTypeCustomizer(type);
			if (customizer!=null) {
				return customizer.getCustomInstanceClass(type);}			
			else{
				return null;
			}	
		},
		__customizeTypeOptions:function(options){
			//options.name is the types name
			this.matchExtension('prefabware.model.typeCustomizer',{type:options.name})
			.then(function(ext){
				if (ext!=undefined) {
					options=ext.customize(options);
					return options;
				}
				
			});
			return options;
		},
		__customizeAttributeOptions:function(options,type){
			//synchron
			//access the asynchron matchExtension, but because the extension is lazy=false
			//it is alrready resolved
			//options.name is the unqualified attribute name
			var matcherOptions={qattributeName:type.name+'.'+options.name};
			this.matchExtension('prefabware.model.attributeCustomizer',matcherOptions)
			.then(function(ext){
				if (ext!=undefined) {
					options=ext.customizeAttributeOptions(options,type);
					return options;
				}
				
			});
			return options;			
		},
	});
});
