//to provide behaviour for a type
//the type may react to events of an instance of the type
//and e.g calculate some attribute values
define('prefabware/plugins/prefabware/model/behaviour/Behaviour', [ 
       'dojo',
       'dojo/_base/declare','prefabware/lang'
       ],function(dojo) {
		dojo.declare('prefabware.plugins.prefabware.model.behaviour.Behaviour', null, {
		constructor : function(options) {
			return;
		},
		apply:function(options){			
			//async
			//options.entity
			//options.event
			//applies this behaviour to the given entity
			//returns the modified entity as {entity:entity}
			return prefabware.lang.deferredValue({entity:options.entity});
		},
	});
});
