//to hook into the lifecycle of CompositeTypes
define('prefabware/plugins/prefabware/model/ExtensionTypeLifecycle', [ 
       'dojo',
       'prefabware/plugin/TypeExtension',
       'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare('prefabware.plugins.prefabware.model.ExtensionTypeLifecycle', [prefabware.plugin.TypeExtension], {
		id :null,
		constructor : function() {
			return;
		},
		defaultsApplied:function(event){
			//is called after defaults were applied
			//implementers may set or change defaults here
			prefabware.lang.log('applied defaults to instance of type '+event.instance.__type.name);
			return;
		},
		changesApplied:function(before,after){
			//is called after changes to the instance of the type were applied
			//before= instance before changes, may be null if the instance was new
			//after = instance after changes			
			prefabware.lang.log('applied changes to '+after.__label()+' of type '+after.__type.name);
			return;
		}
	});
});
