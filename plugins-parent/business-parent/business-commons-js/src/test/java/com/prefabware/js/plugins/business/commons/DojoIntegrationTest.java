package com.prefabware.js.plugins.business.commons;
import org.junit.Before;
import org.junit.Test;

import com.prefabware.js.test.DojoTestSupport;

public class DojoIntegrationTest {
    private DojoTestSupport testSupport;
    @Before
    public void setUp() throws Exception {
    	testSupport=new DojoTestSupport("http://localhost:8080/business-commons-js/js");
    	//testSupport.add("prefabware/plugins/prefabware/business/commons/tests/action/ActionTest.html");    	
    	//testSupport.add("prefabware/plugins/prefabware/business/commons/tests/documentService/DocumentServiceTest.html");    	
    	testSupport.add("prefabware/plugins/prefabware/business/commons/tests/service/TypeLoaderTest.html");    	
    }
    
    @Test
    public void testAll() throws Exception {
    	testSupport.testAll();    	
    } 
}