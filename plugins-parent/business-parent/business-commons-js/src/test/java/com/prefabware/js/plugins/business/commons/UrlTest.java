package com.prefabware.js.plugins.business.commons;

import static org.junit.Assert.assertEquals;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;

import com.prefabware.rest.server.UrlBuilder;

public class UrlTest {

	@Before
	public void setUp() throws Exception {}

	@Test
	public void test() throws Exception{
		String scheme="http";
		 String userInfo=null;
		 String host="localhost";
		 String path="/context";
		 String query=null;
		 String fragment=null;
		 int port=8080;
		URI uri = new  URI(scheme,userInfo
	              ,  host,  port,
	                path,  query,  fragment);
		assertEquals("http://localhost:8080/context",uri.toString());
	}
	
	@Test
	public void testBuilder() throws Exception{
		UrlBuilder builder = new UrlBuilder();
		builder.scheme="http/1.1";
		builder.userInfo=null;
		builder.host="localhost";
		builder.query=null;
		builder.fragment=null;
		builder.contextPath="context";
		builder.port=8080;
			URI uri = builder.createUri();
			assertEquals("http://localhost:8080/context",uri.toString());
	}
	@Test
	public void testBuilderHTTP() throws Exception{
		UrlBuilder builder = new UrlBuilder();
		builder.scheme="HTTP/1.1";
		builder.userInfo=null;
		builder.host="localhost";
		builder.query=null;
		builder.fragment=null;
		builder.contextPath="context";
		builder.port=8080;
		URI uri = builder.createUri();
		assertEquals("http://localhost:8080/context",uri.toString());
	}
	@Test
	public void testBuilderWithOutContext() throws Exception{
		UrlBuilder builder = new UrlBuilder();
		builder.scheme="HTTP/1.1";
		builder.userInfo=null;
		builder.host="localhost";
		builder.query=null;
		builder.fragment=null;
		builder.port=8080;
		URI uri = builder.createUri();
		assertEquals("http://localhost:8080",uri.toString());
	}

}
