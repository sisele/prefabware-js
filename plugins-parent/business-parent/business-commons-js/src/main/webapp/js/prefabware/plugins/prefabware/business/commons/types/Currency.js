//
define('prefabware/plugins/prefabware/business/commons/types/Currency', [ 
                                        'dojo',
                                        'dojo/currency',
                                        'dojo/_base/declare' ],
                                function(dojo,localeCurrency) {
	dojo.declare("prefabware.plugins.prefabware.business.commons.types.Currency",null, {
		__label : function() {
			return this.code;
		},
		__format : function(value) {
			return localeCurrency.format(value,{currency:this.code});
		},

	});
});
