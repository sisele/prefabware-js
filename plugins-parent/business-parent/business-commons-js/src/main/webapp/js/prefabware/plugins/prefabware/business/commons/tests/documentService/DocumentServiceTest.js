// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide('prefabware.plugins.prefabware.business.commons.tests.documentService.DocumentServiceTest');
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("doh");
doh.register('prefabware.plugins.prefabware.business.commons.tests.documentService.DocumentServiceTest', [ {
	name : "store",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.business.commons.tests.documentService',
			fileName : 'documentServiceTest-registry.json'
		});
	},
	runTest : function() {
		var testDeferred = new doh.Deferred();
		var that=this;
		
		this.registry.startup().then(function(registry){
			var servicePlugin = registry.findPlugin('prefabware.service');
			doh.assertFalse(servicePlugin==null);
			servicePlugin.findService('prefabware.service.document').then(function(service){
				service.createReport("com.prefabware.business.party.Person",5)
					.then(function(){
						testDeferred.callback(true);
					},function(args){
						testDeferred.errback(args);
					});
			});
		});
		
	return testDeferred;
	},
	tearDown : function() {
	}
} ]);