define({
	root : {
		createReport : 'fa fa-file-pdf-o',
		masterData : 'fa fa-database',
		basicData : 'fa fa-cogs',
		documents : 'fa fa-file',
		com_prefabware_business_commons_QuantityUnit : 'fa fa-dashboard',
		com_prefabware_business_commons_Currency : 'fa fa-money',
		com_prefabware_business_commons_PaymentTerm : 'fa fa-money',
		com_prefabware_business_address_Country : 'fa fa-globe',
		com_prefabware_business_address_City : 'fa fa-globe',
		contacts : 'fa fa-group',
		com_prefabware_business_party_Branch : 'fa fa-bars',
		com_prefabware_business_party_Person : 'fa fa-user',
		com_prefabware_business_party_Company : 'fa fa-group',
		com_prefabware_business_product_Product:'fa fa-gift',
		com_prefabware_business_product_ProductGroup:'fa fa-gift',
		com_prefabware_business_letter_Letter : 'fa fa-file',
		com_prefabware_business_commons_document_DocumentLink : 'fa fa-link',
		com_prefabware_business_commons_document_DocumentType : 'fa fa-file',
		com_prefabware_business_commons_document_DocumentTemplate : 'fa fa-file',
		system: 'fa fa-sitemap',
		com_prefabware_rest_server_security_SystemUserJpa : 'fa fa-user',
		com_prefabware_business_message_Message:'fa fa-text-width',
		com_prefabware_business_account_Account: 'fa fa-credit-card'
	},

});