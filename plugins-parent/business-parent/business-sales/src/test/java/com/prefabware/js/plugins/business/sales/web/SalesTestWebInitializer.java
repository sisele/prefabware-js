package com.prefabware.js.plugins.business.sales.web;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.prefabware.business.sales.include.SalesTestConfiguration;
import com.prefabware.business.sales.web.include.SalesWebTestConfiguration;
import com.prefabware.environment.bootstraped.StartupSupport;
import com.prefabware.rest.server.Constants;


public final class SalesTestWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements WebApplicationInitializer{
	@Override
	protected WebApplicationContext createRootApplicationContext() {
		WebApplicationContext rootCtx = super.createRootApplicationContext();
		new StartupSupport((ConfigurableApplicationContext) rootCtx);
		return rootCtx;
	}
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[]{SalesWebTestConfiguration.class};
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class<?>[]{SalesTestConfiguration.class};
	}
	@Override
	protected String[] getServletMappings() {
		return new String[]{ Constants.API_MAPPING};
	}
}

