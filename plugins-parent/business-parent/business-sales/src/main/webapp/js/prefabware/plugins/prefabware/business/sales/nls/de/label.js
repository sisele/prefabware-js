define({
	sales : 'Vertrieb',
	com_prefabware_business_sales_entity_Customer : 'Kunde',
	com_prefabware_business_sales_invoice_Invoice : 'Rechnung',
	com_prefabware_business_sales_invoice_InvoiceLine : 'Rechnungspositionen',
	Invoice:'Rechnung',
	CurrencyAndValues:'Währung und Beträge',
	Customer : 'Kunde',
	customer : 'Kunde',
	paymentTerms : 'Zahlungsbedingung',
	totalNet : 'Summe netto',
	totalVat : 'Summe MwSt.',
	totalGross : 'Summe brutto'
});