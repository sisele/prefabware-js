//
define('prefabware/plugins/prefabware/business/sales/types/Customer', [ 
                                        'dojo',
                                        'dojo/number',
                                        'dojo/_base/declare' ],
                                function(dojo,localeCurrency) {
	//do NOT extend Entity here !!
	dojo.declare("prefabware.plugins.prefabware.business.sales.types.Customer",null, {
		
		__label : function() {
			var label= this.number;
			if (this.party!=null) {
				label=label+' '+this.party.name;
			}
			return label;
		},
		__resolveThisProxy : function() {
			var that=this;
			return this.inherited(arguments).then(function(resolved){
				//if customer is a proxy and is resolved, allways also resolve the
				//party, we need customer.party.name for the label
				if (that.party!=null&&that.party.__isProxy&&!that.party.__isResolved) {
					return that.party.resolve().then(function(){
						return prefabware.lang.deferredValue(resolved);
					});
				}else{
					return prefabware.lang.deferredValue(resolved);
				}
			});
		}
	});
});
