//to load the totals for an invoice
define('prefabware/plugins/prefabware/business/sales/behaviour/InvoiceEditorBehaviour', [ 
       'dojo',
       'dojo/promise/all',
       'dojo/_base/array',       
       'dojo/_base/declare',
       'prefabware/lang'],function(dojo,all,array) {
		dojo.declare('prefabware.plugins.prefabware.business.sales.behaviour.InvoiceEditorBehaviour', null, {
		storePlugin:{inject:'prefabware.store',type:'plugin'},   
		constructor : function(options) {
			return;
			},
		
		apply:function(options){
			//as
			var that=this;
			var invoice=options.entity;
			var appendPath="totals";
			var query={id:invoice.pfw_id};
			if (invoice==null) {
				return prefbware.lang.deferredValue(null);
			}else{
				return that.storePlugin.createStore({type:invoice.__type}).then(
						function(store) {
							var entity= store.query(query,{entity:invoice,appendPath:appendPath});
							return entity.then(function(results){
								//the values are merged into the invoice, so just return it
								//a behavior returns a {entity:...}
								return {entity:invoice};
							});
						});
			}
		}
	});
});
