//to provide behaviour for a type
//the type may react to events of an instance of the type
//and e.g calculate some attribute values
define('prefabware/plugins/prefabware/business/sales/behaviour/InvoiceLineEditorBehaviour', [ 
       'dojo',
       'dojo/promise/all',
       'dojo/_base/array',       
       'dojo/_base/declare',
       'prefabware/lang'],function(dojo,all,array) {
		dojo.declare('prefabware.plugins.prefabware.business.sales.behaviour.InvoiceLineEditorBehaviour', null, {
		constructor : function(options) {
			return;
		},  
		
		resolveProxies:function(entity,names,i){
			var that=this;
			if (i>names.length) {
				return prefabware.lang.deferredValue();
			}
			//the current nested property
			var current=names.slice(0,i).join(".");
			//entity = invLine
			//nestedName = product.price
			var value=entity.__valueOf(current);
			if (value==null) {
				//finish
				return prefabware.lang.deferredValue();
			}else if(!value.__isProxy){
				//do not resolve this but digg deeper
				return that.resolveProxies(entity, names, i+1);
			}else  if(value.__isComposite){	
				//doesnt work for array atributes
				//resolve this and digg deeper
				return value.__resolveProxies().then(function(){					
					return that.resolveProxies(entity, names, i+1);
				});
			}else{
				return prefabware.lang.deferredValue();
			}
		},
		apply:function(options){
			//as
			var that=this;
			var invLine=options.entity;
			if (invLine==null) {
				return prefbware.lang.deferredValue(null);
			}else{
				return this.resolveProxies(invLine,["product","price","currency"],1).then(function(){
					return that.__apply(options);
				});
			}
		},
		__apply:function(options){
			var invLine=options.entity;
			//var event;
			//triggers the behaviour with the given event
			if (invLine.number==null||invLine.number==0) {				
				//if the number is empty set it to the highest number + 10
				//TODO find the highest number, thats tricky !
				//need access to the temp. array that the invline will be added to on save
				//even acessing the invoice in the store doesnt help here !
				//may be its easier to just set a very high number and renumber on save ?
				invLine.number=990;
			};
			if (invLine.text==null||invLine.text.length==0) {
				if (invLine.product!=null) {
					//if the text is empty, set it to the decription of the selected product
					invLine.text=invLine.product.description;
				}
			}
			if (invLine.quantity==null) {
				//no quantity ? create one
				invLine.quantity=invLine.__type.getAttribute("quantity").type.create();
			}
			if (invLine.quantity.quantityUnit==null&&invLine.product!=null) {
				//use the quantity unit  of the product by default
				invLine.quantity.quantityUnit=invLine.product.quantityUnit;
			}
			if (invLine.price==null&&invLine.product!=null) {
				//use the price of the product by default
				invLine.price=invLine.product.price.__clone();
			}
			if (invLine.price!=null&&invLine.product!=null) {
				//even if the price !=null, value and currency may still be null
				if (invLine.price.value==null||invLine.price.value==0) {
					invLine.price.value=invLine.product.price.value;
				}
				if (invLine.price.currency==null) {
					invLine.price.currency=invLine.product.price.currency;
				}
			}
			return prefabware.lang.deferredValue({entity:invLine});
		},
	});
});
