define({
  root: {
	  	applicationTitle:'Credit Risk Management',
	  	title:'Titel',
		code:'Code',
		city:'City',
		country:'Country',
		street:'Street',
		customer:'Customer',
		symbol:'Symbol',
		lines:'Items',
	    decimals:"Decimals",	
	    description:"Description",	
	    value:"Value",	
	    amount:"Amount",	
	    product:"Product",	
	    price:"Price",	
	    quantity:"Quantity",	
	    quantityValue:"Value",	
	    quantityUnit:"Unit",	
	    baseUnit:"Base Unit",	
	    factor:"Factor",	
	    info:"Info",	
	    invoice:"Invoice",	
	    invoiceDate:"Invoice date",
	    basicData:'Basic data',
	    masterData:'Master data',
	    transactionData:'Transaction data',
	    
	    currency:"Currency"
  },
  de: true
});