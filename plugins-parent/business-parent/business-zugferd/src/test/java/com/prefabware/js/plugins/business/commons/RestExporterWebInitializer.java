package com.prefabware.js.plugins.business.commons;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.prefabware.business.zugferd.server.JpaConfiguration;
import com.prefabware.business.zugferd.server.RestConfiguration;
import com.prefabware.model.rest.server.SpringConfiguration;

public final class RestExporterWebInitializer implements  WebApplicationInitializer {
	protected final Log logger = LogFactory.getLog(getClass());

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext rootCtx = new AnnotationConfigWebApplicationContext();
		rootCtx.register(
				SpringConfiguration.class
				);
		DispatcherServlet dispatcherServlet = new DispatcherServlet(rootCtx);
		ServletRegistration.Dynamic reg = servletContext.addServlet("dispatcher", dispatcherServlet);

		reg.addMapping("/service/*");
		reg.setLoadOnStartup(1);
		
		AnnotationConfigWebApplicationContext webCtx = new AnnotationConfigWebApplicationContext();
	    webCtx.register(JpaConfiguration.class,RestConfiguration.class);

	    DispatcherServlet dispatcherServlet2 = new DispatcherServlet(webCtx);
		ServletRegistration.Dynamic reg2 = servletContext.addServlet("rest-exporter", dispatcherServlet2);
	    
		reg2.addMapping("/api/*");
	    reg2.setLoadOnStartup(1);

	}

}