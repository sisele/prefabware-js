// DID NEVER RUN !
dojo.provide("prefabware.plugins.prefabware.business.receivables.tests.invoiceLine.InvoiceLineTest");
// require all plugins
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.promise.all");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.business.receivables.tests.invoiceLine.InvoiceLineTest", [ {
	name : "plugin-business-receivables",
	timeout : 1000,
	typeRegistry : null,
	storePlugin:null,
	modelPlugin:null,
	registry : null,

	setUpDeferred : null,
	setUp : function() {
		var that = this;
	
		this.setUpDeferred = new doh.Deferred();
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.business.receivables.tests.invoiceLine',
			fileName : 'plugin-registry.json'
		});
		this.registry.startup().then(function(registry) {
			var receivablesPlugin = registry.findPlugin('prefabware.business.receivables');
			doh.assertFalse(receivablesPlugin==null);
			that.storePlugin = registry.findPlugin('prefabware.store');
			doh.assertFalse(that.storePlugin==null);
			
			that.modelPlugin = registry.findPlugin('prefabware.model');
			doh.assertFalse(that.modelPlugin==null);
			
			that.typeRegistry = that.modelPlugin.getTypeRegistry();
			doh.assertFalse(that.typeRegistry==null);
			that.setUpDeferred.resolve();
		});
	},

	runTest : function() {
		var that = this;
		var testDeferred = new doh.Deferred();
		this.setUpDeferred.then(function() {
						
			dojo.promise.all([
			     that.testBehaviour(),
			    // that.testLabel(),
			     ]).then(function(results){
				 testDeferred.callback(true);
			  });
		});
		return testDeferred;
	},
	testBehaviour : function() {
		//to test the behaviour
		var def = new dojo.Deferred();
		var that = this;
		
		var invoiceType=that.typeRegistry.findType('com.prefabware.business.receivables.entity.Invoice');
		var invoicLineType=that.typeRegistry.findType('com.prefabware.business.receivables.entity.InvoiceLine');
		doh.assertFalse(invoiceType==null);
		
		that.storePlugin.createStore({type:invoiceType})
			.then(function(store) {
					var pResult = store.get(1);
					doh.assertNotEqual(null, pResult);
					return pResult;
				})
			.then(function(invoice) {
				//resolveProxies returns a promise that does NOT resolve to the entity 
				//so we need another promise just to return the resolved invoice
					return invoice.__resolveProxies().then(function(){
						return invoice;
					});
				})
				.then(function(invoice) {
					//the array invoice.lines was resolved, but the lines itself not ! 
					var line=invoice.lines[0];
					return line.__resolveProxies().then(function(){
						return line;
					});
				})
			.then(function(invoiceLine) {
				invoiceLine.number=0;
				invoiceLine.text="";
				//invoiceLine.quantity.quantityUnit=null;
				invoiceLine.price=null;
				
				return that.modelPlugin.applyBehaviour({entity:invoiceLine,event:{sender:'editor',trigger:'onChange'}}).then(function(result){
					var invoiceLine=result.entity;
					doh.assertFalse(invoiceLine==null);
					doh.assertEqual(990,invoiceLine.number);
					doh.assertEqual(invoiceLine.product.description,invoiceLine.text);
					def.callback(true);
				});
				});
		return def;
	},
	
	
	testLabel : function() {
		//to test how to get a label
		
	},
	tearDown : function() {
	}
} ]);