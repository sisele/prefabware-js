package com.prefabware.js.plugins.business.receivables.web;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.prefabware.js.test.DojoTestSupport;

public class DojoIntegrationTest {
    private DojoTestSupport testSupport;
    @Before
    public void setUp() throws Exception {
    	testSupport=new DojoTestSupport("http://localhost:8080/business-receivables/js");
    	testSupport.add("prefabware/plugins/prefabware/business/receivables/tests/invoiceLine/InvoiceLineTest.html");    	
    }
    @Ignore
    @Test
    public void testAll() throws Exception {
    	testSupport.testAll();    	
    } 
}