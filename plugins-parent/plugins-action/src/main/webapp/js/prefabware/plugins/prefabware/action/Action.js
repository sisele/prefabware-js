// something that can be executed through a user action
// widgets like ActionButton or ActionLink delegate to an action
// a action has a label and an icon
// to execute an action call execute(..)
// a action may need informations about its context to execute
// the context can be passed as argument to the execute method

define('prefabware/plugins/prefabware/action/Action', [ 
       'dojo', 
       'dojo/_base/array',
       'dojo/when',
       'prefabware/util/OptionsMixin',
       'dojo/_base/declare' ], function(
		dojo,Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.action.Action", [prefabware.util.OptionsMixin], {
		id:null,
		actionId:null,// the id of the action like 'create'
		label :null,//the label of the action, will not be set here, may be set by the ui 
		icon:null,
		type:null,
		constructor : function(options) {
			this.label=this.id;
			//mix in only those options, for which a property exists in this class
			this.mixinOptions(options);
		},
		startup : function() {
			//will be called after inject
			//subclasses may overwrite this method
			return true;
		},	
		confirm : function(context,action) {
			// clients can hook in here to e.g. open a dialog that 
			// asks for confirmation
			// this method may also change the context for the execution.
			// may return
			//			 - true   = confirmed, so execute the commans
			//           - false  = not confirmed, execution canceled
			//           - a promise that will be resolved to true or false
			//default=true
			return true;
		},	
		onExecute : function(context) {
			//clients can hook here 
			//to run when the action is executed			
		},	
		execute : function(context) {
			//clients call this method to execute it, including confirm etc.
			//confirmation may be asynchron
			var that=this;
			var pConfirmed=this.confirm(context,this);
			return dojo.when(pConfirmed, function(confirmed){
				if (confirmed) {
					return that.onExecute(context);
				}
			  });
		},	
	});
});