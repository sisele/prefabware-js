define('prefabware/plugins/prefabware/action/ActionCreate', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.action.ActionCreate", [prefabware.plugins.prefabware.action.Action], {
		//this is not the extension but the instance !! injection into instances is not implemented yet
		modelPlugin:null, 
		isPersistenceAction:true,
	});
});