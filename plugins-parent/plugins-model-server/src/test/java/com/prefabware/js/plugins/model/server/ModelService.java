package com.prefabware.js.plugins.model.server;

import java.io.Reader;
import java.net.URL;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.prefabware.commons.ResourceUtil;
import com.prefabware.commons.StringUtil;

@Component
@Path("/modelService")
public class ModelService {
	
	public ModelService() {
		super();
	}

	@GET
	@Path("{typeName}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getType(@PathParam("typeName") String typeName) {
		if ("com.prefabware.business.commons.Amount".equals(typeName)) {
			return read("amount-type.json");
		}
		if ("com.prefabware.business.commons.Currency".equals(typeName)) {
			return read("currency-type.json");
		}else{
			throw new IllegalArgumentException("unknown type "+typeName);
		}
	}

	private String read(String fileName) {
		URL resource;
		resource = ResourceUtil.getResource(getClass(), fileName);
		Reader reader = ResourceUtil.reader(resource);
		List<String> read = ResourceUtil.read(reader);
		StringBuffer buf = new StringBuffer();
		for (String string : read) {
			buf.append(string);
			buf.append(StringUtil.LF);
		}
		return buf.toString();
	}
}
