//a proxy for ValueTypes
//this Proxy is used to extend an existing data object, that contains the data for all its attributes.
//create an instance of this EntityProxy, pass the type and the data as constructor arguments
//CAUTION : than the data will be as well a Entity as a EntityProxy.
//continue using the data object, but forgett the created proxy
define('prefabware/model/server/proxy/ValueProxy',
		[ 'dojo', 'dojo/_base/lang','prefabware/model/Value','prefabware/model/server/proxy/CompositeProxy','prefabware/model/server/proxy/ReferenceProxy',
		  'prefabware/model/server/proxy/EntityResolver',
		  'dojo/_base/declare' ],
		  function(dojo, lang,Value,CompositeProxy) {
	dojo.declare("prefabware.model.server.proxy.ValueProxy", [prefabware.model.server.proxy.CompositeProxy], {			
		constructor : function(type,data) {			
			//data now is a proxy, it all its attributes did allready exist in data
			//so the only thing we do is proxy the references
			if (data.pfw_id!=undefined) {
				throw 'object must NOT have a pfw_id';
			};
			data.__isResolved=true;
		},
		__initializeProxy : function() {
			//keep original arrays
			//do not create trigger properties, because this proxy is completely resolved 
			//and uses doGet doSet as accessors
			this.__target={};
			var attrs=this.__type.getAttributes();
			for (var i=0; i<attrs.length; i++){
	    		var attr=attrs[i];
	    		var attrName=attr.name;
	    		if (!attr.isSystem) {
	    			//the system properties are directly accessible and have values set,
	    			//no need to resolve before acessing them
	    			//reference attributes may here be only ValueTypes itself. a value type may not have references to e
	    			if (attr.declaredClass=='prefabware.model.ReferenceAttribute') {
	    				//store the original value in the target
		    			this.__target[attrName]=this[attrName];
		    			//if a caller touches the property, the acessor will be invoked,
		    			//and will wrap the value 
	    				this.createPropertyAccessors(attrName);
					}
				}
	    		
	    	  }	 
			},
	});
});
