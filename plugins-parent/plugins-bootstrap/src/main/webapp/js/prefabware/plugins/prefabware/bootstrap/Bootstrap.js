//workplace cannot be a widget because layout container inside a widget are not supported
define('prefabware/plugins/prefabware/bootstrap/Bootstrap', [
        'dojo',
        "dojo/_base/lang", // lang.hitch lang.trim
        "dojo/_base/array", //forEach
        "dojo/i18n", // i18n.getLocalization
        'dojo/aspect',
        'prefabware/plugin/PluginRegistry',
        "prefabware/lang",
        'dojo/_base/Deferred',        
		'dojo/_base/declare',
		], function(dojo, lang,array,i18n,aspect) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.Bootstrap", null, {
		pluginRegistry:null,
		controller:null, 
		actionPlugin:null,  
		uiPlugin:null,
		bootstrapPlugin:null,
		storePlugin:null,
		editorRegistry:null,
		viewRegistry:null,
		app : null,
		tabCounter:0, 
		menuBar : null,
		tabContainer : null,
		tabContainerBottom : null,
		editors : new Object(),
		contextPath: '',//the contextPath
		constructor : function(options) {
			this.pluginRegistry=options.pluginRegistry;
		},
		startup : function() {
			var that=this;
			 this.pluginRegistry.startup().then(function(value){
				    // Do something when the process completes
					that.actionPlugin=that.pluginRegistry.findPlugin('prefabware.action');
					that.uiPlugin=that.pluginRegistry.findPlugin('prefabware.ui');
					//make this workplace the controller
					that.storePlugin=that.pluginRegistry.findPlugin('prefabware.store');
					that.bootstrapPlugin=that.pluginRegistry.findPlugin('prefabware.bootstrap');					
					
					that.uiPlugin.createMenus();
				  }, function(err){
				    prefabware.lang.throwError(err);
				  }, function(update){
				    // Do something when the process provides progress information
				  });
		},
	});
});
