define('prefabware/plugins/prefabware/bootstrap/WidgetBindingSelectEnumeration', [
	'dojo',
	 
    'dojo/aspect',
    'dojo/_base/array',
	'prefabware/app/WidgetBinding',
	'dojo/_base/declare' ], function(dojo, aspect,array,WidgetBinding) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.WidgetBindingSelectEnumeration", [prefabware.app.WidgetBinding], {
		options:null,
		constructor : function(attribute,widget) {
			this.options=new dojox.collections.Dictionary();	
			var that=this;
			aspect.after(widget,"onLoadOptions",function(){
				that.options.clear();
				that.__addOptions();
			});
		},
		__addOptions : function() {	
			array.forEach(this.attribute.type.elements.getValueList(),function(element){
				this.widget.addOption(element,element.name);
			},this);
		},

		getValue : function() {
			return this.widget.getSelected();	
		},	
		setValue : function(value) {
			this.widget.setSelected(value);
		},
	});
});
