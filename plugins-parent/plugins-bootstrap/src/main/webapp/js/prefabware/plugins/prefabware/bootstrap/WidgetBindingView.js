define('prefabware/plugins/prefabware/bootstrap/WidgetBindingView', [ 
       'dojo'
      ,'dojo/_base/array'
      , 'prefabware/app/WidgetBinding'
      ,'dojo/_base/declare' 
      ], function(dojo, array,WidgetBinding) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.WidgetBindingView", [prefabware.app.WidgetBinding], {

		getValue : function() {
			// get the value of the widget
			// returns an array of all entities contained in the grid
			// the store is always in sync with the grid. changes from
			// editors opened for a row
			// are allways made to the store of the grid.
			// so we can easily get the data from the store here
			return this.widget.store.data;
		},
		setValue : function(compositeArray) {
			if (compositeArray == undefined || compositeArray == null) {
				this.widget.store.setData([]);
				return;
			}
			//value is an array of composites
			//do not pass the given array to the widget
			//use a clone instead
			//the given array must maintain unchanged, untill save was klicked on the form !
			var clone=array.map(compositeArray,function(composite){
				return composite.__clone();
			});
			this.widget.store.clear();
			this.widget.store.setData(clone);
			this.widget.refresh();
		},
		setToDefault : function() {
			//the default for a grid is an empty store
			this.setValue(null);
		},	
	});
});