//the ui controller
//can openEditor,openView, showError,showWarning etc.
define('prefabware/plugins/prefabware/bootstrap/ExtensionController', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/aspect',
       'dojo/dom-construct',
       'dojo/dom-style',
       'dojo/dom-class',
       "dojo/query",
       "dojo/window",
       "dojox/fx/scroll",
       'dojo/_base/lang',
       'dojo/promise/all',
       'dojox/collections/Dictionary',
       "dijit/registry",
       'prefabware/lang',
       'prefabware/plugins/prefabware/bootstrap/widgets/Panel',
       'prefabware/plugins/prefabware/bootstrap/widgets/Navbar',
       'prefabware/plugins/prefabware/bootstrap/widgets/MenuNode',
       'prefabware/plugins/prefabware/bootstrap/widgets/MenuItem',
       'prefabware/plugins/prefabware/bootstrap/widgets/MenuSubItem',
       'prefabware/plugins/prefabware/bootstrap/widgets/SidebarNode',
       'prefabware/plugins/prefabware/bootstrap/widgets/SidebarItem',
       'prefabware/plugins/prefabware/bootstrap/widgets/SidebarSubItem',
       'prefabware/plugins/prefabware/bootstrap/widgets/Row',
       'prefabware/plugins/prefabware/bootstrap/widgets/Modal',
       'prefabware/plugins/prefabware/bootstrap/widgets/Loading',
       'prefabware/plugins/prefabware/bootstrap/widgets/Dialog',
       'prefabware/plugins/prefabware/bootstrap/widgets/ActionLink',
       'prefabware/plugins/prefabware/bootstrap/widgets/ActionButton',
       'prefabware/plugins/prefabware/ui/ExtensionController',
       'prefabware/plugins/prefabware/bootstrap/widgets/EditorWidget',
       'dojo/_base/declare' ], function(
		dojo, array,aspect,domConstruct,domStyle,domClass,domQuery,win,fx,lang,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionController", [prefabware.plugins.prefabware.ui.ExtensionController], {
		storePlugin:{inject:'prefabware.store',type:'plugin'},   
		serverPlugin:{inject:'prefabware.server',type:'plugin'},   
		uiPlugin:{inject:'prefabware.ui',type:'plugin'},   
		actionPlugin:{inject:'prefabware.action',type:'plugin'},   
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'}, 
		injector:   {inject:'injector',type:'injector'},  
		loader:   {inject:'classLoader',type:'bean'},  
		loading:null,//loading indicator
		navbar:null,
		sidebar:null,
		constructor : function() {
		},
		startup : function() {
			this.inherited(arguments);
			var that=this;
			//tracking navigation records calls to the tracked methods 
			//when the user navigates e.g. to an editor he opened before, teh same call, using the same arguments
			//will be executed again. this allows the editor to use the same store as before
			// and will keep it in sync with the tabel he originally was opened from
			this.uiPlugin.trackNavigation({target:this,methodName:"openPage",fragment:function(){return "openPage"+"."+arguments[0];}})
				.then(function(){
					that.openHomepage();
				}).then(function(){
				 return that.uiPlugin.trackNavigation({target:that,methodName:"openView",fragment:function(){return "openView"+"."+arguments[0].simpleName();}});					
				}).then(function(){
				 return that.uiPlugin.trackNavigation({target:that,methodName:"openEditor",fragment:"openEditor"});					
				});
			//subscribe to blocking events to show the progress bar
			dojo.subscribe("blockingStart", function(options){
				that.__onBlockingStart(options);
			});
			dojo.subscribe("blockingEnd", function(options){
				that.__onBlockingEnd(options);
			});

			return that.bootstrapPlugin.fetchSidebarWidget().then(function(sidebar){
				that.sidebar=sidebar;
				if (sidebar!=null) {
					return that.sidebar.startup();
				}{
					return prefabware.lang.deferredValue();}
			}).then(function(){
				return that.bootstrapPlugin.fetchNavbarWidget().then(function(navbar){
					that.navbar=navbar;
					if (navbar!=null) {
						return that.navbar.startup();
					}{
						return prefabware.lang.deferredValue();}
				});
			});
		},
		newRow:{
			//the position where to put a new row, first or last
			position:'first'
				},
//		closeAllRows : function(){
//			array.forEach(this.rows.getValueList(),function(row){
//				row.destroy();
//				this.rows.remove(row);
//				},this);	
//		},
//		onRowClose : function(row){
//			//call this method when a row is closed
//			this.rows.remove(row.id);
//			if (this.rows.count==0) {
//				if (this.addingRow) {
//					//if adding a new row do not show homepage
//					return;
//				}
//				//this.openHomepage();
//			}
//		},
//		addingRow:false,
//		addRow : function(row) {
//			this.addingRow=true;
//			//closes and removes all existing rows
//			this.closeAllRows();
//			//adds the new row, so that only the new row is visible
//			prefabware.lang.assert("row must have an id",row.id!=null&&row.id!=undefined);
//			this.rows.add(row.id,row);
//			row.placeAt(this.getParent(),this.newRow.position);
//			this.addingRow=false;
//			},
		newModal:{
			//the position where to put a new modal, first or last
			position:'first'
			},
		fadeIn : function(node) {
				  //before the caller must set
			      //domStyle.set(node, "opacity", "0");
			      var fadeArgs = {
			        node: node
			      };
			      dojox.fx.fadeIn(fadeArgs).play();
		},
		scrollTo : function(node) {
			//scroll to the node
			 dojox.fx.smoothScroll({
			 node: node,
			 win: window,
			 duration: 1000 }).play();
				},
		clearContent : function() {
			//we support only one open view so remove all content before opening a new one
			//TODO destroy widgets before !
			var widgets = dijit.registry.findWidgets(this.getParent());
			array.forEach(widgets,function(w){
				if(w.destroyRecursive){
					w.destroyRecursive();
				}else if(w.destroy){
					w.destroy();
				}
			});
			domConstruct.empty(this.getParent());
		},
		addView : function(view) {
			this.views.add(view.pfw_type.name,view);
			},
		openHomepage : function() {
			var that=this;
			this.clearContent();
			//var row=this.createRow();
					this.bootstrapPlugin.getHomepageWidgetClass().then(function(homepageClass) {
						if (homepageClass == null) {
							return prefabware.lang.deferredValue();
						} else {
							return that.openPage(homepageClass);
						}
					});
		},
		openPage : function(widgetClassName) {
			//pagewidgets are destroyed when closed
			//so to support back navigation the argument must be the class and not the 
			//widget itself
			var that=this;
			this.clearContent();
			return this.serverPlugin.findServer("prefabware.server.api").then(function(server){
				return that.loader.createInstance(widgetClassName,{api:server.getUrl()})
				.then(function(pageWidget){
					pageWidget.placeAt(that.getParent(),that.newRow.position);
					return pageWidget.startup();
				});
			});
		},
		
		blockingTimer:null,
		blocking:0,
		__onBlockingStart : function() {
			var that=this;
			this.blocking++;
			//a process blocking the ui was started, show the progress bar
			this.blockingTimer = setTimeout(function() {
				//the blockingEnd event may allready have occured, but the timer is still waiting to start
				//so if blocking==0, do not show 'loading'
				if (that.blocking>0) {
				if (that.loading==null) {
					//create lazy
					that.loading=new prefabware.plugins.prefabware.bootstrap.widgets.Loading({label:"Loading"});
					that.loading.placeAt(that.getParent(),that.newModal.position);
					that.loading.startup();//that is necessary to create and place the modal.domeNode
					}
					that.loading.show();
				}
			}, 250);
		},
		__onBlockingEnd : function() {
			if (this.blocking>0) {
				this.blocking--;
			}
			//a process blocking the ui was ended, hide the progress bar
			if (this.blockingTimer!==null) {
				clearTimeout(this.blockingTimer);
				this.blockingTimer=null;
			}
			if (this.loading!==null) {
				this.loading.hide();
			}
		},
		
		notifier:null,
		sendMessage : function(msgId,msgParms) {
			var that=this;
			this.uiPlugin.createMessage(msgId,msgParms).then(function(msg){
				that.uiPlugin.notify(msg).then(function(notifier){
					that.notifier=notifier;
				});
			});
		},
		createTable : function(type,parms) {
			//async
			//creates the table and all widgets
			//creates the actions and their buttons and links
			//everything else is delayed until an action is executed
			var that=this;
			
			return this.bootstrapPlugin.createTable(type,parms).then(function(table){

				aspect.after(table,"sendMessage",function(msgId,msgParams){
					that.sendMessage(msgId, msgParams);
				},true);
			
				aspect.after(table,"destroy",function(){
					//remove all messages
					if (that.notifier!=null) {
						//TODO #24 doesnt work
					//	that.notifier.closeAll();
					}
				});
				aspect.after(table,"beforeDataLoading",function(){
					dojo.publish("blockingStart", ["createTable"]);
				});
				aspect.after(table,"onDataLoaded",function(){
					//the data was loaded, remove loading now
					dojo.publish("blockingEnd", ["createTable"]);
				});
				
				var actionNew=null;
				var actionDelete=null;
				var buttonNew=null;
				var actionOpenEditor=null;
				//the table has allready all actions set, but now add buttons etc.
				actionOpenEditor=table._findRowAction('openEditor');
				if (actionOpenEditor!=null) {
					var onOpenEditorToEdit=dojo.hitch(that, that.__onOpenEditor);
					aspect.after(actionOpenEditor,"onExecute",onOpenEditorToEdit,true);
				}

				actionDelete=table._findRowAction('delete');
				if (actionDelete!==null) {
					actionDelete.confirm=dojo.hitch(that, that.__onConfirmDelete);
				}
				return table;
			}).then(function(table){
					var actionId='create';
					return that.uiPlugin.fetchAction(actionId,{type:type,i18nKey:actionId}).then(function(action){
					//TODO use view.actions !
					var onOpenEditorToCreate=dojo.hitch(that, that.__onOpenEditor,{type:type,table:table});
					action.label=" ";//do not show a text for that button
					button=that.createActionButton(action);
					button.set("class","btn btn-default btn-sm");
					table.addActionButton(button);	
					aspect.after(button,"onClick",onOpenEditorToCreate,true);					
					return table;
					});
				}).then(function(table){
					var actionId='refresh';
					return that.uiPlugin.fetchAction(actionId,{type:type,i18nKey:actionId}).then(function(action){
						//TODO use view.actions !, add only the action, the container can build the button		
						//ISSUE #37
						action.label=" ";//do not show a text for that button
						button=that.createActionButton(action);
						button.set("class","btn btn-default btn-sm");
						table.addActionButton(button);					
						aspect.after(button,"onClick",function(){
							button.loading();
							var refreshM=dojo.hitch(table,table.refresh);
							
							var promise=refreshM();
							promise.then(function(){
								button.notLoading();
							});
						},true);					
						return table;
				});	
			});
		},
		__onOpenEditor:function(context){
			//async
			var that=this;
			var table=context.table;
			var typeOrEntity=null;
			if (context.entity!=undefined) {
				//this is the entity of the table row.
				typeOrEntity=context.entity;
			}else{
				typeOrEntity=context.type;
			}
			//create a root editor
			var options={typeOrEntity:typeOrEntity,store:table.store};
			return that.openEditor(options).then(function(editor){
				return editor;
			});
			},
		__onConfirmDelete:function(context,actionDelete){
			var type=context.type;
			var that=this;
			var dialog=null;
			var okButton=null;
			var closeButton=null;
			var pConfirm=new dojo.Deferred();
			return that.uiPlugin.createMessage('DLT0003',{entity:context.entity.__fullLabel()}).then(function(msg){
				msg.replace();
				dialog=that.createDialog({title:msg.title,text:msg.text});
				return dialog;
			}).then(function(dialog){
				var actionId='delete';
				return that.actionPlugin.fetchAction(actionId,{type:type,i18nKey:actionId});
			}).then(function(actionOk){
				okButton=that.createActionButton(actionOk);
				aspect.after(okButton,"onClick",function(){
					pConfirm.resolve(true);
					//actionDelete.onExecute(context);
					dialog.destroy();
					return;
				});
			}).then(function(){
				var actionId='close';
				return that.actionPlugin.fetchAction(actionId,{type:type,i18nKey:actionId}).then(function(actionClose){
					actionClose.label="";
					closeButton=that.createActionButton(actionClose);
					aspect.after(closeButton,"onClick",function(){
						pConfirm.resolve(false);
						dialog.destroy();
						return;
					});
					dialog.addActionButton(okButton);
					dialog.addActionButton(closeButton);
					dialog.startup();
					//return the promise 
					return pConfirm;
				});
			});
		},		
	
		openView : function(type,embedded) {
			//async
			var that=this;
			this.clearContent();
			return that.uiPlugin.fetchLocalizations({key:type,types:["icon","label"]}).then(function(il){
				return that.createTable(type,{icon:il.icon,label:il.label});
			}).then(function(typeTable){
				that.__placeAt(typeTable).then(function(box){
					if (!typeTable.renderTableActions) {
						//if the table does not render the actions, render them on the box
						//TODO just add actions to the box, it than will render the buttons the way it needs them 
						array.forEach(typeTable.actionButtons.getValueList(),function(button){
							box.addActionButton(button);
						});
					}
					//connect the searchfield of the box with the table search
					//TODO refactor
					if (typeof box.search=="function" ) {
						if (typeof box.showSearch=="function") {
							box.showSearch();
						}
						aspect.after(box,"search",function(options){
							typeTable.search(options);
						},true);
					}
					else
						//connect the searchfield of the navbar with the table search
						if (that.navbar!=null&&typeof that.navbar.search=="function" ) {
							//TODO refactor
							aspect.after(that.navbar,"search",function(options){
								typeTable.search(options);
							},true);
						}
					
					box.startup();
					typeTable.startup();
					return typeTable;
				});
					});
				},
		
		getParent : function() {
			var parent = dojo.query('[data-appdriver-center]')[0];
			return parent;
			},
		createDialog : function(params) {
			var dialog = new prefabware.plugins.prefabware.bootstrap.widgets.Dialog(params);
			return dialog;
			},
		createModal : function(options) {
				//async
			//async
			//creates a box for content like a form or a view
			var that=this;
			return this.bootstrapPlugin.fetchModalWidget(options).then(function(modal){				
					//modal.startup();startup later, when its in its position !! if not z-index is wrong
					return modal;
				});
			},
		createBox : function(options) {
				//async
				//creates a box for content like a form or a view
				var that=this;
				return this.bootstrapPlugin.fetchBoxWidget(options).then(function(box){
					if (box!=null) {
						box.placeAt(that.getParent(),that.newRow.position);
						box.startup();
						return box;
					}else{
						//if no box is declared, place directly on the parent
						return that.getParent();
					}
				});
				
			},
			__placeAt : function(widget) {
				//async
				//places the widget in the box
				//and returns the box
				var that=this;
				return this.createBox({icon:widget.icon,label:widget.label}).then(function(box){
					//widget.placeAt(box);
					box.addChild(widget);
					return box;
				});
			},
			
		/**
		 * creates a new row and adds it using the stratagy
		 * @returns {prefabware.plugins.prefabware.bootstrap.widgets.Row}
		 */
//		createRow : function() {
//			var that=this;
//			var params={};
//			var row=new prefabware.plugins.prefabware.bootstrap.widgets.Row(params);
//			//adds the new row, so that only the new row is visible
//			this.addRow(row);
//			aspect.after(row,"destroy",function(){
//				that.onRowClose(row);
//			});
//			return row;
//		},
//		createActionLink : function(action) {
//			var link=new prefabware.plugins.prefabware.bootstrap.widgets.ActionLink({label:action.label,icon:action.icon,action:action});
//			return link;
//		},
		createActionButton : function(action,style) {
			//async
			var options={label:action.label,icon:action.icon,action:action};
			if (style!=null) {
				options.style=style;
			}
			var link=new prefabware.plugins.prefabware.bootstrap.widgets.ActionButton({label:action.label,icon:action.icon,action:action});
			return link;
			//return this.bootstrapPlugin.fetchActionButtonWidget(options);
		},
		fetchLabel : function(label) {
			//async
			//returns the label in the users language for the given untranslated label
			return this.uiPlugin.labelOf(label);
		},
		getIcon : function(type) {
			//returns the icon for the given type
			return this.uiPlugin.getIcon(type);
		},
		
		__createEditor : function(type,embedded) {
			//async
			var that=this;
			return this.bootstrapPlugin.createEditor(type,embedded).then(function(editorBW){
				var editor=editorBW.widget;
				aspect.after(editor.controller,"sendMessage",function(msgId,msgParams){
					that.sendMessage(msgId, msgParams);
				},true);
				
				return editor;
			});
		},
		
		
		openEditor : function(options) {
			//opens an editor
			//async
			//options=
			//typeOrEntity
			//embedded
			//store - optional, the store to use
			var typeOrEntity=options.typeOrEntity;
			var embedded=options.embedded;
			
			var cfg={};//configuration for the editor,
			cfg.store=options.store;
			
			var that=this;
			var type=null;
			var entity=null;
			var entityLabel=null;
			var entityIcon=null;
			var isNew=null;
			if (typeOrEntity instanceof prefabware.model.Type) {
				type=typeOrEntity;
				isNew=true;//its an editor for a new entity
			}else{
				entity=typeOrEntity;				
				type=entity.__type;
				isNew=false;//its an editor for an existing entity
			}
			//allways fetch label and icon of the type
			var pI18n={};
			pI18n.type=that.uiPlugin.fetchLocalizations({key:type,types:["icon","label"]});
			if (isNew) {
				//if its new, fetch the label for 'new'
				pI18n._new=this.uiPlugin.labelOf('new');					
			}else{
				//TODO we have to resolve proxies here, because customer.label uses to customer.party.name
				//but further down we reload the entity again.....
				//if its not new use the label of the entity
				pI18n.post=entity.__resolveProxies().then(function(){
					return entity.__label();
					});
				}			
			return promiseAll(pI18n).then(function(i18n){				
				entityLabel = prefabware.lang.concat(i18n._new," ",i18n.type.label," ",i18n.post).trim();
				entityIcon  = i18n.type.icon;
			var editor=null;
			var mp=null;
			var modal=null;
			return that.__createEditor(type,embedded).then(function(newEditor){
				editor=newEditor;
				
				if (editor.model.modal) {
					mp=that.createModal({label:entityLabel,icon:entityIcon});
				}else{
					//if its NOT modal, remove the underlying table
					that.clearContent();
					//modal=new prefabware.plugins.prefabware.bootstrap.widgets.Panel();
					mp=that.createBox({label:entityLabel,icon:entityIcon});
				}
				return mp.then(function(xmodal){
					modal=xmodal;
					modal.placeAt(that.getParent(),that.newModal.position);
					modal.startup();//this is necessary to create and place the modal.domeNode
					if (entity==null||entity.__store==null) {
						return;
					}
					// make shure, all atributes the editors shows are loaded
					//TODO its suboptimal to do it here, a little to late, the enitity was allready set into the controller
					//and we will do it again here. But we need the editor to know the list of properties..
					var pReload;
					if (entity.__store.isMemoryStore){
						//no need to reload from memorystore, TODO doesnt work, either, because the store is empty here....
						pReload=prefabware.lang.deferredValue(entity);
					}else{ 
						pReload=entity.__store.get(entity.pfw_id,{entity:entity,properties:newEditor.controller.binding.bindings.getKeyList()});
					}
					return pReload;
				});
			}).then(function(reloadedEntity){
				entity=reloadedEntity;
				var pactions={};
				array.forEach(editor.model.actions,function(action){
					switch (action.actionId) {
					case "save":
						pactions.save=that.__createSaveAction(editor,modal,embedded);
						break;
					case "close":
						pactions.close=that.__createCloseAction(editor,modal,type);
						break;
					case "createReport":
						pactions.report=that.__createCreateReportAction(editor,modal,type);
						break;
					default:
						break;
					}
				});
				return promiseAll(pactions);
//				return promiseAll(pactions).then(function(actions){
//					aspect.after(actions.save,"onExecute",function(){
//						//call close on save
//						//TODO close only if no errors ocured
//						actions.close.onExecute();
//					});
//				});
			}).then(function(){
				modal.addChild(editor);
				//resolve all direct proxied attributes of the entity	
				if (entity==null) {
					modal.startup();
					editor.controller.setEntity(entity);
					return prefabware.lang.deferredValue(editor);  
				}else{
				return entity.__resolveProxies().then(function(){
					modal.startup();
					editor.controller.setEntity(entity);
					return editor;   
				});}		
			}).then(function(){
				if (cfg.store==null) {
					//TODO the store should be created by the uiplugin, not here
					return that.storePlugin.createStore({type:type}).then(
							function(store) {
								cfg.store=store;
								return ;
							});
				}else{
					return prefabware.lang.deferredValue();
				}
			}).then(function(){
				
			var onSave=function(toSave){
				var store=cfg.store;
				//TODO try and catch errors here !!
				//tell the store which properties to return with the result
				var options={"properties":editor.binding().bindings.getKeyList()};
				var method;
				if (toSave.__isNew()) {
					method='add';
				}else{
					method='put';
				}
				store[method](toSave,options).then(function(result){
					result.__resolveProxies().then(function(entity) {
						//let the editor show the returned entity
						editor.controller.setEntity(result);
						//table.addRow(result);//TODO allways add ? if its only an update
						//save succesfull
						that.sendMessage('SAV0001',{entity:toSave.__fullLabel()});							
					});
				},function(err){
					//error
					that.sendMessage('SAV0002',{entity:toSave.__fullLabel()});
				});
				return;
			};
			//attach to onSave, so validation etc. is allready passed
			aspect.after(editor.controller,"onSave",onSave,true);
			})
			});
		},
		__createSaveAction : function(editor,modal,embedded) {
			//async
			//creates the action savefor the editor
			var controller=editor.controller;
			//save button
			var that=this;
			var actionId='save';			
			return that.actionPlugin.fetchAction('save',{type:editor.model.type,i18nKey:actionId}).then(function(actionSave){
				aspect.after(actionSave,"onExecute",function(){
					controller.save();
					return;
				});
				//TODO find a better way
				if (modal.addAction!=null) {
					modal.addAction(actionSave);
				}else{
					editor.addAction(actionSave);
				}
				return actionSave;
			});
		},
		__createCloseAction : function(editor,modal,type) {
			var that=this;
			var actionId='close';
			return that.actionPlugin.fetchAction(actionId,{type:type,i18nKey:actionId}).then(function(actionClose){
				
				aspect.after(actionClose,"onExecute",function(){
					if (editor.model.modal) {
						modal.destroy();
					}else{
						that.uiPlugin.navigateBack();
					}
					return;
				});
				//TODO find a better way
				if (typeof modal.addAction=="function") {
					modal.addAction(actionClose);
				}else{
					editor.addAction(actionClose);
				}
				return actionClose;
			});
		},
		__createCreateReportAction : function(editor,modal,type) {
			//async
			//creates the button to run a report
			var that=this;
			var actionId='createReport';
			return that.actionPlugin.fetchAction(actionId,{type:type,i18nKey:actionId}).then(function(action){
				// set fix parameter for action.execute
				aspect.before(action,"execute",function(context){
					//return value will be used as arguments for the original function
					var entity=editor.controller.before;
					return [{type:entity.pfw_type,id:entity.pfw_id}];
				});				
				//TODO find a better way
				if (typeof modal.addAction=="function") {
					modal.addAction(action);
				}else{
					editor.addAction(action);
				}
			}, function(err){
			    // if no action createReport is defined, no problem but log out
				// TODO
				prefabware.lang.warn('could not fetch action "createReport", may be its not declared in no plugin');
			  });
		},
		
		getActionContext : function() {
			//provides functions for the basic methods like ActionCreate,ActionSave,ActionOpenEditor
			var context={create:this.create};
			return context;
		},
		
		openEditorForType : function(type,embedded) {
			// shows an editor for a new entity of the given type
			// allways creates a new editor
			return this.openEditor(entity,embedded);		
		}
	});
});