//to send notifications to the user with bootstrap-notifier
//a notifier is a message that pops up and closes after a while
//to use it you need the following in your html (and adjust pathes)
//<link href="../../../../../bootstrap-notify/css/bootstrap-notify.css" media="all" rel="stylesheet" type="text/css" />
//<script src="../../../../../bootstrap-notify/js/bootstrap-notify.js" type="text/javascript"></script>
//an element with every positon you want to use e.g. top-left,top-right etc.
//to use this, you need something like : <div class='notifications top-right'></div>

define('prefabware/plugins/prefabware/bootstrap/ExtensionNotifier', [ 
       'dojo','dojo/_base/array',  
       'prefabware/plugins/prefabware/ui/ExtensionNotifier',
       'dojo/_base/declare' ], function(
		dojo,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionNotifier", [prefabware.plugins.prefabware.ui.ExtensionNotifier], {
		hookSelector:'.top-right',
		notifiers:null,
		constructor : function(options) {
			this.notifiers=[];
			if (options.json.hookSelector!=null) {
				this.hookSelector=options.json.hookSelector;
			}
		},
		startup : function() {
//			return this.resourcePlugin.loadScript([
//			                                       ,
//			                  				      ]);
		},		
		closeAll : function(options) {
			//closes all notifiers
			array.forEach(this.notifiers,function(n){
				n.onClose();
			});
			this.notifiers=[];//clear
		},
		
		notify : function(options) {
			//options={title:'a title',text:'a text',msgType:'success'}
			// sends a notification with the given text using the type
			// type can be one of : 'success','info','warning','danger'
			//				these are the bottstrapp alert styles without leading 'alert'
 			//              if no type is given, info will beused
			var type=options.msgType;
			if (type==null) {
				type='info';
			}else if (type=='error') {
				//TODO configurable
				//bootstrap 3 uses alert-danger
				type='danger';			
			}
			// text is the text to show on alert,
			var text=options.text;
			var that=this;
			// you can use either html or text. HTML will override text.
			// message: { html: false, text: 'This is a message.' }
			var notifier=$(this.hookSelector).notify({
				message: { html: "<h4>"+options.title+"</h4><p>"+options.text+"</p>"  },
				type: type,
				fadeOut: { enabled: true, delay: 3000 }
			}).show(); 
			this.notifiers.push(notifier);
			return this;
		},		
	});
});