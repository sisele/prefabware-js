//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/Box', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Box.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Box", [dijit._Widget, dijit._Container,dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._ActionContainer  ], {
		templateString: html,
		span:12,//full width
		icon:' icon-warning-sign',//icon 
		label:' box label',//icon
		id:null,
		constructor : function(params) {
//			if (params.id) {
//				this.id=params.id
//			}
//			if (params.icon) {
//				this.icon=params.icon;
//			}
//			if (params.label) {
//				this.label=params.label;
//			}
		},
		startup : function() {
			this.inherited(arguments);
			parser.parse(this.srcNodeRef); 
		},		
		addChild: function(/*dijit/_WidgetBase*/ widget, /*int?*/ insertIndex){
			// do NOT call inherited here !!
			// by default dijit adds children allways to the containerNode
			// but here we want to attach children to the data-attach-nod children
			var refNode = this.childrenNode;			
			domConstruct.place(widget.domNode, refNode, insertIndex);
		}
		
	});
});
