//this plugin provides integration with the bootstrap template
define('prefabware/plugins/prefabware/bootstrap/Plugin', [ 'dojo',  
		'dojo/_base/array', 'dojo/_base/lang','dojo/aspect',
		 'dojo/promise/all',
		'prefabware/lang',
		'prefabware/model/QualifiedAttribute',
		'prefabware/plugins/prefabware/bootstrap/widgets/Table',
		'prefabware/plugins/prefabware/bootstrap/widgets/MenuItem',
		'dojo/_base/declare',
		'prefabware/plugin/Plugin' ], function(dojo,  array, lang, aspect,promiseAll) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.Plugin",
			[ prefabware.plugin.Plugin ], {
			storePlugin    : {inject:'prefabware.store',type:'plugin'},  	
			uiPlugin       : {inject:'prefabware.ui',type:'plugin'},
			
			nextId:{id:1},
			startup : function() {
				
			},
	        isBreakpoint: function( alias ) {
	        	// Return true if current breakpoint matches passed alias
	        	// copied from https://github.com/maciej-gurban/responsive-bootstrap-toolkit
	        	// needs JQuery, that must be available anyway
	        	//the following elements must exist in the page 
//	        	<div class="device-xs visible-xs"></div>
//	            <div class="device-sm visible-sm"></div>
//	            <div class="device-md visible-md"></div>
//	            <div class="device-lg visible-lg"></div>
	        	// TODO rewrite to dojo
	        	//alias : 
	            return $('.device-' + alias).is(':visible');
	        },
			getHomepageWidgetClass : function(options,srcNode) {
				//async
				return this.fetchExtension('prefabware.bootstrap.homepage').then(function(ext){
					if (ext==null) {
						return prefabware.lang.deferredValue();
					}else{
					return ext.getHomepageWidgetClass();
					}
				});
			},
			fetchSidebarWidget : function(options,srcNode) {
				//async
				//creates and returns the sidebar widget
				return this.fetchWidget('prefabware.bootstrap.sidebar',options);		
			},
			fetchSidebarNode : function(options,srcNode) {
				//async
				//creates and returns the sidebar widget
				return this.fetchWidget('prefabware.bootstrap.sidebarNode',options,srcNode);
			},
			fetchNavbarWidget : function(options,srcNode) {
				//async
				//creates and returns the navbar widget
				return this.fetchWidget('prefabware.bootstrap.navbar',options);				
			},
			fetchBoxWidget : function(options,srcNode) {
				//async
				//creates and returns a box widget
				return this.fetchWidget('prefabware.bootstrap.box',options);
			},
			fetchModalWidget : function(options,srcNode) {
				//async
				//creates and returns a box widget
				return this.fetchWidget('prefabware.bootstrap.modal',options);
			},
			fetchActionButtonWidget : function(options,srcNode) {
				//async
				//creates and returns a box widget
				return this.fetchWidget('prefabware.bootstrap.actionButton',options);
			},
			fetchWidget : function(extId,options,srcNode) {
				//async
				//creates and returns a widget
				//extId the extensionId
				return this.fetchExtension(extId).then(function(ext){
					if (ext==null) {
						return prefabware.lang.deferredValue();
					}
					return ext.createWidget(options,srcNode);
				});
			},
			    createTable : function(type,params) {
			    	//async
			    	var that=this;
			    	var p1= this.fetchExtension('prefabware.bootstrap.table');
					var p2= this.uiPlugin.createTableParms(type,params);
			        return promiseAll({ext:p1,tableParams:p2}).then(function(results){
			    		return results.ext.createWidget(results.tableParams);
					});
			    },
			    createEditor : function(type,params,srcNode) {
			    	//async
			    	var qattribute=type.getQualifiedAttribute('self');
					return this.uiPlugin.createBindingAndWidget(qattribute,params).then(function(editor){
						return editor;
					});
				},
//				
				fetchController : function(){
					//returns the controller for the ui
						return this.uiPlugin.fetchController();
				},
				__assertParams : function(params) {
					if (params==null||params==undefined) {
						params={};
					}
					return params;
				},
			});
});
