//a widget to select an entity from a :1 relation
//uses https://github.com/ivaynberg/select2
define('prefabware/plugins/prefabware/bootstrap/widgets/BsSelectReferenceWidget2', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dojo/dom-style',
       "dojo/query", 
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/BsSelectReferenceWidget2.html',
       "dojo/NodeList-data",
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct,domStyle,query,template) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.BsSelectReferenceWidget2', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		pageSize:10,//maximum number of element per page
		select2:null,//the select2 node
		templateString: template,
		store:null,
		isEmbedded:false,
		resourcePlugin       : {inject:'prefabware.resource',type:'plugin'},
		select:null,//the label that should be selected, but we have to wait for the load request
		constructor : function(params,parent) {
			if (params.label==undefined) {
				throw new 'params.label must be set';
			}
			//TODO theres a mess with embedded, its false or an object
			if (params.embedded===false) {
				this.isEmbedded=false;
			}else{
				this.isEmbedded=true;
			}
		},	
				
		setSelected : function(entity) {
			this.selected=entity;
			if (this.select2!=null) {
				if (entity!=null) {
					this.select2.select2("val",entity.pfw_id);
				}else{
					this.select2.select2("val",null);
				}
			}
		},
		getSelected : function() {
			return this.selected;
		},
		__beautify : function(){
			//this has to run AFTER the value was set !!
			var widget=this;
			//asynchron
  			widget.select2=$(widget.attachPoint_input).select2({
  				placeholder: "Select a "+widget.attribute.name,  				//TODO 18i
  			    allowClear: true,
  			  containerCssClass: "form-control",
  			  minimumInputLength: 0,//TODO depnds an number of elements and length of the attribute
  				//selected:widget.selected,
  				//style:"full-width"
  				//style:widget.domNode.style.width
  			initSelection : function (element, callback) {
  					var entity = null;
  					var id=element.val();
  					if (widget.selected!=null&&id==widget.selected.pfw_id) {
  						//its the same as allready selected
						var data=widget.__entityToItem(widget.selected);
						callback(data);
					}{
						widget.store.get(id).then(function(entity){
							var data=widget.__entityToItem(entity);
							callback(data);
						})
					}
			    },
  			query: function(options){
  				//is called with
//  				 element: opts.element,
//                 term: search.val(),
//                 page: this.resultsPage,
//                 context: null,
//                 matcher: opts.matcher,
//                 callback: this.bind(function (data) {
  				var name=widget.attribute.type.keyAttribute.name;
  				//term is the string entered by the user. append a '*' so the story will search for all beginning with the string
  				var p_query={};
  				p_query[name]=options.term+'*';
  				//options.page is the requested page (1based)
  				//the store expects start, the number of the first record to fetch, 0-based
  				//so the first row to query (start) is the first row of the page 
  				var p_options={start:(options.page-1)*widget.pageSize,
  							   count:widget.pageSize};
  				return widget.store.query(p_query, p_options).then(function(entities){
  					var formatted=array.map(entities,function(entity){
  						return widget.__entityToItem(entity);
  					})
  					//entities.total  = number of all available entites for thet query
  					//entities.length = number of entities in the current result
  					//p_options.start = number of the first entity in the current result
  					var more=false;
  					if (entities!=null) {
  						more=(entities.total-(p_options.start+entities.length))>0
					}
  					options.callback({more:more,results:formatted});
  				});
  			},
  			formatResult:function(item, container ){
  				var entity=item.entity;
  				return entity.__label();  				
  			},
  			formatSelection:function(item, container ){
  				var entity=item.entity;
  				return entity.__label();  				
  			},
  		    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
  		    escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
  			});
  			$(widget.domNode).on("change",widget.select2,function(event){
  				if (event.added) {
  					widget.selected=event.added.entity;
				}else{
					widget.selected=null;
				}
  				widget._onChange(event);
  				});
  			
		},
		__entityToItem : function(entity) {
			if (entity) {
				//formats the entity so that it can be used as a value for select2				
				return {id:entity.pfw_id,text:entity.__label(),entity:entity};
			}else{return null;}
		},
		startup : function() {
			this.inherited(arguments);	
			this.__beautify();
			
		},
	});
});



