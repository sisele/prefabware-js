//a html label
define('prefabware/plugins/prefabware/bootstrap/widgets/Label', [ 
       'dojo',  
       'dojo/dom-attr',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Label.html',
       'dijit/_Widget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(dojo,domAttr,html) {
	var __static=dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Label", [dijit._Widget, dijit._Templated ], {
		templateString: html,
		constructor : function(params) {
			if (params.label==undefined) {
				throw 'params.label must be set';
			}
		},	
		startup : function() {
			this.inherited(arguments);	
		},
	});
});
