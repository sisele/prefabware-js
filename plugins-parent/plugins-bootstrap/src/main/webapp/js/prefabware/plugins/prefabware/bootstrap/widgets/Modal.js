//a widget that is shown modal, with a disabled background
define('prefabware/plugins/prefabware/bootstrap/widgets/Modal', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Modal.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionContainer',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'dojo/_base/declare' ], function(
		dojo, array,parser,domConstruct,html) {
	//the superlassconstructors are called from right to left
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Modal", [dijit._Widget, dijit._Container, dijit._Templated,prefabware.plugins.prefabware.bootstrap.widgets._ActionContainer ], {
		templateString: html,
		span:12,//full width
		label:' modal label',//icon
		patch:{active:false},
		modal:null,//the jquery modal 
		constructor : function(params) {
			// see https://github.com/jschr/bootstrap-modal/
			if (!this.patch.active) {
				this.patch.active=true;
				$.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner = 
					'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
					'<div class="progress progress-striped active">' +
					'<div class="progress-bar" style="width: 100%;"></div>' +
					'</div>' +
					'</div>';
			}
		},
		startup : function() {
			this.inherited(arguments);
			this.modal = $(this.attachPointModal).modal({keyboard: true,backdrop:'static'});
		},
		show : function() {
			$(this.modal).modal('show');
		},
		hide : function() {
			$(this.modal).modal('hide');
		},
		destroy : function() {
			$(this.modal).modal('hide');
			this.inherited(arguments);
		},			
	});
});
