define('prefabware/plugins/prefabware/bootstrap/action/ActionOpenView', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.action.ActionOpenView", [prefabware.plugins.prefabware.action.Action], {
		bootstrapPlugin:{inject:'prefabware.bootstrap',type:'plugin'},
		type:null,
		constructor : function() {
		},
		fragment : function() {
			//the fragment for this action if used as a link
			return "openView"+"."+this.type.name;
		},
		onExecute : function(context) {
			//async
			var that=this;
			return this.bootstrapPlugin.fetchController().then(function(controller){
				return controller.openView(that.type);
			});
		},	
	});
});