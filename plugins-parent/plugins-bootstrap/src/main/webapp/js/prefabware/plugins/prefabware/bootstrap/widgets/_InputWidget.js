//a widget for a html input or select
//should have an attachPoint_input, where e.g. validation message will be set
define('prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dojo/dom-class',
       'dojo/dom-style',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/_InputError_span.html',
       'prefabware/plugins/prefabware/ui/widgets/_Widget',
       'prefabware/plugins/prefabware/bootstrap/widgets/_FeedbackWidget',
       'dojo/NodeList-traverse',
       'dijit/_Widget',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct,domClass,domStyle,messageTemplate) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets._InputWidget", [prefabware.plugins.prefabware.ui.widgets._Widget,prefabware.plugins.prefabware.bootstrap.widgets._FeedbackWidget,dijit._Widget], {
		// messageTemplate:
		// dojo.cache("prefabware.plugins.prefabware.bootstrap.widgets",
		// "_InputError_span.html"),
		embedded:null,
		disabled:false,
		validationMarker:null,
		attachPoint_marker:null,// hwere to put the validation image and text
		attachPoint_marker_class:null,// where to put the validation class
										// like 'error'
		constructor : function(params,parent) {
			this.validationMarker={class_:null,span:null};
			if (params.qattribute) {
				//TODO refactor to _Widget, _WidgetUnbound
				this.disabled=params.qattribute.attribute.isReadOnly;
			}
		},		
		startup : function() {
			this.inherited(arguments);			
			if (this.attachPoint_input) {				
				this.attachPoint_marker=this.attachPoint_input.parentNode;
			}
			this.attachPoint_marker_class=this.__findAttachPoint_marker_class();
			this.disable(this.disabled);
			return;
		},
		disable : function(disabled) {
			dojo.query(this.attachPoint_input).attr("disabled",disabled);
			this.disabled=disabled;
			return;
		},
		_setValueAttr : function(value) {
			this.attachPoint_input.value=value;
			return;
		},
		__findAttachPoint_marker_class : function() {
			if (this.attachPoint_marker_class!=null) {
				// explicitely set ? use it
				return this.attachPoint_marker_class;
			}
			else if (this.domNode!=null && this.domNode.parentNode!=null && dojo.hasClass(this.domNode.parentNode,"form-group")) {
				// this is the default for bootstrap 3
				return this.domNode.parentNode;
			}else{
				// legacy, may be null
				return this.attachPoint_input;
			}
		},
		setValidationMessage : function(message) {
			// TODO the error marker must be set in the FormGroup, not here !
			if (this.attachPoint_marker_class==null) {
				return;
			}
			// remove the old message
			dojo.removeClass(this.attachPoint_marker_class, this.validationMarker.class_);
			if (this.validationMarker.span!=null) {
				// remove the old error message
				dojo.destroy(this.validationMarker.span);
				this.validationMarker.span=null;
			}
			this.validationMarker.class_=message.messageType;
			// 'error', 'warning', 'success', 'none'
			if (message.messageType!='none') {
				// add the error message
				var icon=null;
				if (message.messageType=='error') {					
					icon='icon-exclamation-sign';
					this.validationMarker.class_= "has-error";
				}else if (message.messageType=='warning') {
					icon='icon-warning-sign';
					this.validationMarker.class_= "has-warning";
				}else if (message.messageType=='success') {
					icon='icon-success-sign';
					this.validationMarker.class_= "has-success";
				}
				dojo.addClass(this.attachPoint_marker_class, this.validationMarker.class_);
				var spanTemplate = lang.replace(messageTemplate, {
					marker : message.messageType,
					label : message.text,
					icon : icon
				});
				var span=domConstruct.place(spanTemplate,this.attachPoint_marker,'last');
				this.validationMarker.span=span;
			}
			return;
		},
		_getValueAttr : function() {
			return this.attachPoint_input.value;
		},
		_onChange: function(e){ 
			return this.onChange(e);
		}, 
		onChange: function(e){ 
			// if you need to pass a contxet to action.execute, hook in there,
			// not here
             /*
				 * aspect may not work with this mehod, after the widget was
				 * started up
				 */ 
			/*
			 * because this original method is allready registered as listener
			 * to the dom
			 */
			// event.stop(e);
			if (this.action!=null) {
				this.action.execute();
			}
		} ,
		show: function(){
			// TODO original styles may be lost
			// cannot easily save the style because
			// get() gets the style value and uses the computed style for the
			// node, so the value will be a calculated value, not just the
			// immediate node.style value.
			if (this.domNode!=null) {
				domStyle.set(this.domNode, "display", "");
				
			}
		},
		hide: function(){
			if (this.domNode!=null) {
			domStyle.set(this.domNode, "display", "none");		       
		}
		}
	});
});
