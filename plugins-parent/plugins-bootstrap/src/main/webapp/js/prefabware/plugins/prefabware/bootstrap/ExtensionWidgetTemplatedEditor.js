//to register widgets for types
define('prefabware/plugins/prefabware/bootstrap/ExtensionWidgetEditor', [ 
       'dojo',  
       'dojo/_base/lang',
       'prefabware/lang',
       'prefabware/plugin/Extension',
       'prefabware/plugins/prefabware/bootstrap/ExtensionWidget',
       'dojo/_base/declare' ], function(
		dojo, lang) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionWidgetEditor", [prefabware.plugins.prefabware.bootstrap.ExtensionWidget], {
		uiPlugin:{inject:'prefabware.ui',type:'plugin'},  

		createWidget : function(qattribute,params, srcNode) {
			//async
			var that=this;
			var _arguments=arguments;
			if (params==null) {
				params={};
			}
			var outer=params.outer;
			return that.uiPlugin.createEditorModelAndController(qattribute.attribute.type,outer).then(function(editorMC){
				params.model=editorMC.model;
				params.controller=editorMC.controller;
				return that.inherited(_arguments);
			});
		},
	});
});
