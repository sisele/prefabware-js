define({
  root: {
	prefabware_bootstrap_tests_AllAttributes: "fa fa-wrench",  
	openEditor: "fa fa-pencil",
	close: "fa fa-times",
	openView: "fa fa-list-alt",
	save: "fa fa-save",
	create: "fa fa-plus",
	refresh: "fa fa-refresh",
	new_: "fa fa-plus",
	delete_: "fa fa-trash-o"
  },
  
});