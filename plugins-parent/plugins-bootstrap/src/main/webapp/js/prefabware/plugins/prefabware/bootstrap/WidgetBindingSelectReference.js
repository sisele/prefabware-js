define('prefabware/plugins/prefabware/bootstrap/WidgetBindingSelectReference', [
	'dojo',
	 
    'dojo/aspect',
    'dojo/_base/array',
	'prefabware/app/WidgetBinding',
	'dojo/_base/declare' ], function(dojo, aspect,array,WidgetBinding) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.WidgetBindingSelectReference", [prefabware.app.WidgetBinding], {
		constructor : function(attribute,widget,store) {
			if (store==undefined) {
				throw 'a store is obligatory';
			}
			this.store=store;
			var that=this;
			
			var fquery=function(term){
			var keyAttribute=that.attribute.type.keyAttribute;
			var query={};
			if (term==undefined) {
				term='';
			}
			query[keyAttribute.name]=term+'*';

			var sort={};
			sort.attribute=keyAttribute.name;
			sort.descending=false;

			return that.store.query(query, {
			      start: 0,
			      count: 100,
			      sort:[sort]
			    }).then(function(results){
			  	array.forEach(results,function(entity){
			  		widget.addOption(entity,entity.__label());
			  	}); 
			  	return results;
			    });		
			};
			aspect.after(widget,"onLoadOptions",function(){
				return fquery('').then(function(result){
					widget.__select(widget.selected);
				});				
			},true);
		},
		setValue : function(entity) {	
			if (entity==null) {
				this.widget.setSelected(null);
			}else{
			this.widget.setSelected(entity);
			}
		},		
		getValue : function() {
			return this.widget.getSelected();			
		},		
	});
});
