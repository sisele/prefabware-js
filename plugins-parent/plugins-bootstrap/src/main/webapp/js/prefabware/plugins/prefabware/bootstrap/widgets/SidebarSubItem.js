define('prefabware/plugins/prefabware/bootstrap/widgets/SidebarSubItem', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/SidebarSubItem.html',
       'prefabware/plugins/prefabware/bootstrap/widgets/SidebarItem',
       'dojo/_base/declare' ], function(
		dojo, array,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.SidebarSubItem", [prefabware.plugins.prefabware.bootstrap.widgets.SidebarItem], {
		templateString: html,	    
	});
});

