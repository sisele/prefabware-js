//a widget to select an element from an enum
//uses https://github.com/ivaynberg/select2
define('prefabware/plugins/prefabware/bootstrap/widgets/SelectEnumWidget2', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dojo/dom-style',
       "dojo/query", 
       "dojo/NodeList-data",
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct,domStyle,query) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.SelectEnumWidget2', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		pageSize:10,//maximum number of element per page
		select2:null,//the select2 node
		templateString: null,
		isEmbedded:false,
		select:null,//the label that should be selected, but we have to wait for the load request
		constructor : function(params,parent) {
			if (params.label==undefined) {
				throw new 'params.label must be set';
			}
			this.templateString=dojo.cache('prefabware.plugins.prefabware.bootstrap.widgets', 'BsSelectReferenceWidget2.html')				
			//TODO theres a mess with embedded, its false or an object
			if (params.embedded===false) {
				this.isEmbedded=false;
			}else{
				this.isEmbedded=true;
			}
		},	
				
		setSelected : function(element) {
			this.selected=element;
			if (this.select2!=null) {
				if (element!=null) {
					this.select2.select2("val",element.name);
				}else{
					this.select2.select2("val",null);
				}
			}
		},
		getSelected : function() {
			return this.selected;
		},
		__beautify : function(){
			//this has to run AFTER the value was set !!
			var widget=this;
			//asynchron
  			widget.select2=$(widget.attachPoint_input).select2({
  				placeholder: "Select a "+widget.attribute.name,  				//TODO 18i
  			    allowClear: true,
  			  containerCssClass: "form-control",
  			  minimumInputLength: 0,//TODO depnds an number of elements and length of the attribute
  			initSelection : function (element, callback) {
  					var entity = null;
  					var name=element.val();
  					if (widget.selected!=null&&name==widget.selected.name) {
  						//its the same as allready selected
						var data=widget.__entityToItem(widget.selected);
						callback(data);
					}{
							var entity=widget.attribute.type.__findElement(name);
							var data=widget.__entityToItem(entity);
							callback(data);
					}
			    },
  			query: function(options){
  				//is called with
//  			   element: opts.element,
//                 term: search.val(),
//                 page: this.resultsPage,
//                 context: null,
//                 matcher: opts.matcher,
//                 callback: this.bind(function (data) { 
			var formatted=array.map(widget.attribute.type.elements.getValueList(),function(entity){
					return widget.__entityToItem(entity);
  					});
				options.callback({more:false,results:formatted});
  			},  			
  		    dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
  		    escapeMarkup: function (m) { return m; } // we do not want to escape markup since we are displaying html in results
  			});
  			$(widget.domNode).on("change",widget.select2,function(event){
  				if (event.added) {
  					widget.selected=event.added.entity;
				}else{
					widget.selected=null;
				}
  				});
  			
		},
		__entityToItem : function(entity) {
			if (entity) {
				//formats the entity so taht it cn be used as a value for select2
				return {id:entity.name,text:entity.__label(),entity:entity};
			}else{return null;}
		},
		startup : function() {
			this.inherited(arguments);	
			this.__beautify();
		},
		destroy : function() {
			if (this.select2!=null) {
				$(this.select2).select2("destroy");
			}
			this.inherited(arguments);	
		},
	});
});



