//a bootstrap input-group
//Place one add-on or button on either side of an input. You may also place one on both sides of an input.
//bootstrap does not support multiple add-ons on a single side.
//bootstrap does not support multiple form-controls in a single input group.
//add each element using addChild
//in the order you want it
define('prefabware/plugins/prefabware/bootstrap/widgets/InputGroup', [ 
       'dojo',  
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/InputGroup.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(dojo,domConstruct,html) {
	var __static=dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.InputGroup", [dijit._Widget,dijit._Container,dijit._Templated ], {
		templateString: html,
		constructor : function(params) {			
		},	
		startup : function() {
			this.inherited(arguments);	
		},		
		prepend : function(widgetOrString) {
			this.prepend=widget	
		},		
		input : function(widget) {
			this.input=widget	
		},		
		append : function(widgetOrString) {
			this.append=widget	
		},		
		__place : function(widgetOrString) {
			if (widgetOrString==null) {
				//nothing to do
				return;
			}else if (typeof widgetOrString=="string") {
				this.domNode.place(widgetOrString,this.domNode);
			}else{
				//it must be a widget
				this.addChild(widgetOrString);
			}	
		},		
		buildRendering : function() {
			this.inherited(arguments);//at first create this dom, than append the widgets
			this.__place(this.prepend);
			this.addChild(input);
			this.__place(this.append);
			return;
		},
	});
});
