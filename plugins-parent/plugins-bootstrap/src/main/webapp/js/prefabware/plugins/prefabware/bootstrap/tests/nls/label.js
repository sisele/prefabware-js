define({
  root: {
	  //its not necessary to define messages in the defualt language here
	  //the default text and title are maintained in the messages.json
	  //and dojo.i18n finds translated string even if there is no entry for the default language
	  prefabware_bootstrap_tests_AllAttributes:'with all',
  },
  de: true
});