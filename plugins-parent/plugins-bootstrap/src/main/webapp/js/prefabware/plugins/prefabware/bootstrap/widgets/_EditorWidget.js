//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/_EditorWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/parser',
       'dojo/dom-construct',
       'dojo/dom-class',
       'dojo/dom-style',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionContainer',
       'dojo/_base/declare' ], function(
		dojo, array,lang,parser,domConstruct,domClass,domStyle) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets._EditorWidget", [prefabware.plugins.prefabware.bootstrap.widgets._ActionContainer], {
		embedded:false,
		type:null,
		model:null,
		controller:null,
		label:"editor label",
		constructor : function(params,parent) {
		if (params.model==undefined) {
			throw 'params.model must be set';
		}
		if (params.controller==undefined) {
			throw 'params.controller must be set';
		}
		return;
		},
		destroy : function() {
			this.controller.close();
			this.inherited(arguments);
		},
		binding : function() {
			return this.controller.binding;
		},
		bindings : function() {
			return this.controller.binding.bindings.getValueList();
		},
		addActionButton : function(actionButton) {
			this.inherited(arguments);
			domConstruct.place(actionButton.domNode, this.attachPointActionLink, 'last');
		},
		setValidationMessage : function(marker) {
			//'error', 'warning', 'success', 'none'
			//the widget must NOT access the binding
			//the binding controls the widget not the other way round
			//this.binding().setValidationMessage(marker);
		}
	});
});
