//a view that can be shown in the workspace
//for drag and drop tried
//<div class="control-group dojoDndItem" data-dojo-type="dojo/dnd/Moveable">
//could drag but not drop
define('prefabware/plugins/prefabware/bootstrap/widgets/InputTextAreaWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domStyle) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.InputTextAreaWidget', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		templateString: dojo.cache('prefabware.plugins.prefabware.bootstrap.widgets', 'InputTextAreaWidget.html'),
		rows:4,
		constructor : function(params,parent) {
		//this.rows=params.qattribute.attribute.length/80;
			
		},	
		startup : function() {
			this.inherited(arguments);	
		},
		
	});
});
