//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/ExtensionEditorBinding', [ 
       'dojo',  
       'dojo/_base/array',
       'prefabware/plugins/prefabware/ui/ExtensionEditorBinding',
       'prefabware/app/EditorBinding',
       'prefabware/plugins/prefabware/model/TypeRegistryAccessMixin',
       'prefabware/plugins/prefabware/model/TypeAcceptorMixin',
       'dojo/_base/declare' ], function(
		dojo, array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.ExtensionEditorBinding", [prefabware.plugins.prefabware.ui.ExtensionEditorBinding], {
		
		createBinding : function(model,srcNodeRef,params) {
			var uiPlugin=this.plugin;
			var that=this;
			var binding=new prefabware.app.EditorBinding(model.type,model.actions,model.embedded);
			binding.createBindings=function(){
				return array.map(model.qattributes,function(qattribute){
					var node=that._createWidgetNode(srcNodeRef);
					//do NOT pass the received parms here !! they belong to the model not the attribute !!
					return uiPlugin.createBindingAndWidget(qattribute,{}, node);
				});
			};
			return binding;
		},
		_createWidgetNode : function(srcNodeRef) {
			return dojo.create("div", null, srcNodeRef, "last");
		},
	});
});
