//a widget to select an entity from a :1 relation
//uses https://github.com/silviomoreto/bootstrap-select
//     http://silviomoreto.github.io/bootstrap-select/
define('prefabware/plugins/prefabware/bootstrap/widgets/BsSelectReferenceWidget', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/_base/lang',
       'dojo/dom-construct',
       'dojo/dom-style',
       "dojo/query", 
       "dojox/collections/Dictionary",
       "dojo/NodeList-data",
       'prefabware/plugins/prefabware/bootstrap/widgets/_InputWidget',
       'dijit/_Templated',
       'dojo/_base/declare' ], function(
		dojo, array,lang,domConstruct,domStyle,query) {
	dojo.declare('prefabware.plugins.prefabware.bootstrap.widgets.BsSelectReferenceWidget', [prefabware.plugins.prefabware.bootstrap.widgets._InputWidget, dijit._Templated ], {
		templateString: null,
		store:null,
		isEmbedded:false,
		isLoaded:false,
		lazy:true,//true=loads the options on the first click
		optionsMap:null,//the items used as options of the select
		select:null,//the label that should be selected, but we have to wait for the load request
		constructor : function(params,parent) {
			if (params.label==undefined) {
				throw new 'params.label must be set';
			}
			//TODO theres a mess with embedded, its false or an object
			this.templateString=dojo.cache('prefabware.plugins.prefabware.bootstrap.widgets', 'BsSelectReferenceWidget.html')				
			if (params.embedded===false) {
				this.isEmbedded=false;
			}else{
				this.isEmbedded=true;
			}
			//this.embedded is later changed somewhere
			this.optionsMap=new dojox.collections.Dictionary();
		},	
		placeAt : function(reference,rel) {
			this.inherited(arguments);
			//domStyle.set(this.domNode, "display", "inline");
			return;
		},
		onLoadOptions : function(term) {
			//callers can hook to this method to load the choices
			//for the given search term
			//the caller must than call addOption(entity) for each choice
			//that may happen async, than a deferred should be returned
		},
		addOption : function(item, label) { 
			if (this.optionsMap.item(label)!=null) {
				//option allready exists
				//TODO use unique identifier, but this is not only used for entites, but also enum-elements !
				return;
			}
			//adds the given entity as an option
			domConstruct.place('<option>' + label + '</option>', this.attachPoint_input, 'last');
			//query(option).data("item", item);
			this.optionsMap.add(label,item);
		},
		//a choice is an option selected by the entered term
		__loadOptions : function() {
			var that=this;
			var deferred=this.onLoadOptions();
		  	if (deferred==undefined) {
		  		//synchron
		  		this.__beautify();
		  		this.isLoaded=true;
		  	}else{
		  		deferred.then(function(){
		  			//asynchron
		  			that.__beautify();
		  			that.isLoaded=true;
		  		});
		  	};
		},
		onLoadChoices : function() {
			//callers can hook to this method to load the choices
			//for the given search term
			//the caller must than call addChoice(entity) for each choice
			//that may happen async
		},
		addChoice : function(label) {
			//adds the given entity as a choice
			//domConstruct.place('<li class="active-result">' + label + '</li>', this.chzn_results, 'last');
		},
		onClick : function() {
			//called when the button of the listbox is clicked
			if (!this.isLoaded) {
				this.__loadOptions();
			}
		},
		__select : function(item) {
			var that=this;
			if (item==null) {
				that.attachPoint_input.selectedIndex=-1;
			}
			//selects the option with the given item
			array.some(this.optionsMap.getValueList(),function(optionItem,index){
				if (optionItem.__equals(item)) {
					that.attachPoint_input.selectedIndex=index;
					return true;
				}
				return false; 
			});		
			if (this.attachPoint_input!=null) {
				$(this.attachPoint_input).selectpicker('refresh');
			}
		},
		setSelected : function(entity) {
			if (!this.isLoaded) {
				//the selected may be set, before all of the options are loaded
				if (entity!=null&&this.optionsMap.item(entity.__qid)==null) {					
					this.addOption(entity,entity.__label());
				}
				this.__beautify();
			}
			//remember the selection
			this.selected=entity;
			this.__select(entity);
		},
		getSelected : function() {
			var index=this.attachPoint_input.selectedIndex;
			if (this.optionsMap.getValueList().length>=index) {
				return this.optionsMap.getValueList()[index];
			}else{
				return null;
			}
		},
		__beautify : function(){
			//this has to run AFTER the value was set !!
			var widget=this;
			//asynchron
  			widget.__select(widget.selected);
  			$(widget.attachPoint_input).selectpicker({
  				selected:widget.selected,
  				//style:"full-width"
  				//style:widget.domNode.style.width
  			});
  			var next=dojo.query(this.attachPoint_input).next()[0];
  			var btn=dojo.query('button',next)[0];
  			if (btn!=null) {
  				dojo.connect(btn, "click", dojo.hitch(this,this.onClick));
			}
		},
		startup : function() {
			this.inherited(arguments);	
			var widget=this;
			if (!this.lazy) {
			  this.__loadOptions();
			}
			this.__beautify();
		},
	});
});



