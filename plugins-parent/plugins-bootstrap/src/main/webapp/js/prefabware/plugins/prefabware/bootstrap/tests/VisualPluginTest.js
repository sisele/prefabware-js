dojo.provide("prefabware.plugins.prefabware.bootstrap.tests.VisualPluginTest");

dojo.require("dojo");

dojo.require("doh");

//Define the HTML file/module URL to import as a 'remote' test.
doh.registerUrl("prefabware.plugins.prefabware.bootstrap.tests.VisualPluginTestPage", 
dojo.moduleUrl('prefabware/plugins/prefabware/bootstrap/tests','VisualPluginTestPage.html'));