//a view that can be shown in the workspace
define('prefabware/plugins/prefabware/bootstrap/widgets/Page', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/Page.html',
       'dijit/_Widget',
       'dijit/_Templated',
       'prefabware/plugin/Extension',
       'prefabware/plugins/prefabware/model/TypeRegistryAccessMixin',
       'prefabware/plugins/prefabware/model/TypeAcceptorMixin',
       'dojo/_base/declare' ], function(
		dojo, Extension,array,template) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.Page", [dijit._Widget, dijit._Templated ], {
		templateString: template,
		constructor : function(params,parent,label,iconClass) {			
			params=this.__assertParams(params);
		},
		__assertParams : function(params) {
			if (params==null||params==undefined) {
				params={};
			}
			return params;
		},
	});
});
