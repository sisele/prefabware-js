//to run reports for entities
define('prefabware/plugins/prefabware/bootstrap/widgets/mailchimp/ExtensionMailChimpService', [ 
       'dojo',
       'prefabware/plugins/prefabware/bootstrap/widgets/mailchimp/MailChimpService',
       'prefabware/plugins/prefabware/service/ExtensionService',
       'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.mailchimp.ExtensionMailChimpService", [prefabware.plugins.prefabware.service.ExtensionService], {
		__createService : function() {
			this.service=new prefabware.plugins.prefabware.bootstrap.widgets.mailchimp.MailChimpService(this.url);
		},
		createReport : function(id) {
			this.service.createReport(id);
		},		
	});
});