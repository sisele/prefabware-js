define('prefabware/plugins/prefabware/bootstrap/widgets/SidebarNode', [ 
       'dojo',  
       'dojo/_base/array',
       'dojo/dom-construct',
       'dojo/text!prefabware/plugins/prefabware/bootstrap/widgets/SidebarNode.html',
       'dijit/_Widget',
       'dijit/_Container',
       'dijit/_Templated',
       'prefabware/plugins/prefabware/bootstrap/widgets/_ActionWidget',
       'dojo/_base/declare' ], function(
		dojo, array,domConstruct,html) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.SidebarNode", [dijit._Widget, dijit._Container,dijit._Templated, prefabware.plugins.prefabware.bootstrap.widgets._ActionWidget ], {
		templateString: html,
		constructor : function(params) {
			this.preventDefault=true;
		},
		onClick: function(e){ 
			return;
		}, 
	});
});
