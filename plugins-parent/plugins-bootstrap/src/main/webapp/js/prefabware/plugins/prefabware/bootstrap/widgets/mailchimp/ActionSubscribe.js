define('prefabware/plugins/prefabware/bootstrap/widgets/mailchimp/ActionSubscribe', [ 
       'dojo', 
       'prefabware/plugins/prefabware/action/Action',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo,Action,array) {
	dojo.declare("prefabware.plugins.prefabware.bootstrap.widgets.mailchimp.ActionSubscribe", [prefabware.plugins.prefabware.action.Action], {
		servicePlugin:{inject:'prefabware.service',type:'plugin'},
		service:null,
		type:null,
		constructor : function() {
		},
		startup : function() {
			return this.servicePlugin.findService('prefabware.mailchimp.service').then(function(service){
				this.service=service;
			});
		},
		onExecute : function(context) {
			return service.subscribe(context.email)
			.then(function(downloadLink){
				return;
			});
		},
	});
});