package com.prefabware.js.plugins.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prefabware.js.test.DohTestUrlBuilder;
import com.prefabware.js.test.DojoTestSupport;
import com.prefabware.web.ResourceMappings.ResourceMapping;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootTestApplication.class)
// port:0 = use a random, free port
// but this one has to run on a fixed port, because the test tries to connect !
@WebIntegrationTest("server.port:8080")
public class DojoIntegrationTest {
	private DojoTestSupport testSupport;

	@Value("${local.server.port}") int port;

	@Autowired @Qualifier("rmWebJarDojoUtil") ResourceMapping rmWebJarDojoUtil;
	@Autowired @Qualifier("rmWebJarPrefabware") ResourceMapping rmWebJarPrefabware;

	private DohTestUrlBuilder urlBuilder;

	@Before
	public void setUp() throws Exception {
		String baseUrl = "http://localhost:" + Integer.toString(port);
		testSupport = new DojoTestSupport(baseUrl);
		urlBuilder = createUrlBuilder();
		int wait = 1000;

		add("prefabware.plugins.prefabware.service.tests.ServiceTest");    
		
	}

	public void add(String testJsClass) {
		testSupport.add(urlBuilder.withTestClass(testJsClass).build());		
	}
	public void add(String testJsClass,int wait) {
		testSupport.add(urlBuilder.withTestClass(testJsClass).build(),wait);		
	}
	public DohTestUrlBuilder createUrlBuilder() {
		DohTestUrlBuilder b = new DohTestUrlBuilder();
		b
				.withDohUtilBaseUrl(rmWebJarDojoUtil.urlPath())
				.withModule("prefabware", rmWebJarPrefabware.urlPath());
		return b;
	}

	@Test
	public void testAll() throws Exception {
		testSupport.testAll();
		return;
	}
}