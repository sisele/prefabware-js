//a Plugin
define('prefabware/plugins/prefabware/service/Plugin', [ 'dojo', 
		'dojo/_base/array', 'dojo/_base/lang',
		 'prefabware/plugins/prefabware/service/ExtensionService',
		'dojo/_base/declare',
		'prefabware/plugin/Plugin' ], function(dojo, array, lang) {
	dojo.declare("prefabware.plugins.prefabware.service.Plugin",
			[ prefabware.plugin.Plugin ], {
		
		findService : function(id) {
			//async
			//fetches the extension with the given extension-id
			//and returns the service of the extension
			return this.fetchExtensionById(id).then(function(ext){
						return ext.findService();
					});					
				},
			});
});
