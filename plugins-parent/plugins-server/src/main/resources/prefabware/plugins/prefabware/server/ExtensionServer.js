//a server
define('prefabware/plugins/prefabware/server/ExtensionServer', [ 
       'dojo',
       'prefabware/plugin/Extension',
       'dojo/_base/array',
       'dojo/_base/json',
	   'prefabware/lang',    
       'dojo/_base/lang',
       'dojo/_base/declare' ], function(
		dojo,Extension,array) {
	dojo.declare("prefabware.plugins.prefabware.server.ExtensionServer", [prefabware.plugin.Extension], {
		//	     scheme-specific-part →                        →                       → |
		//	     |
		//	http://hans:geheim@example.org:80/demo/example.cgi?land=de&stadt=aa#geschichte
		//	|      |    |      |           | |                 |                |
		//	|      |    |      host        | url-path          query            fragment
		//	|      |    password           port
		//	|      user
		//	scheme (hier gleich Netzwerkprotokoll)
		scheme:null,//incl. '://' e.g. http://
		user:null,
		password:null,
		host:null,
		port:null,
		urlPath:null,
		constructor : function() {
			this.scheme=this.json.scheme;
			this.user=this.json.user;
			this.password=this.json.password;
			this.host=this.json.host;
			this.port=this.json.port;
			this.urlPath=this.json.urlPath;
		},
		__assertString : function(value) {
			if (value==undefined||value==null) {
				return '';
			}else if (!dojo.isString(value)){
				throw 'value ' + value + ' must be a String';
			} 
			else{
				return value;
			}
		},
		getUrl : function() {
			return this.url;
		},
		startup : function() {
			this.inherited(arguments);
			
			var loc=dojo.doc.location;
			
			this.url=this.__assertString(this.json.url);
			//when the url is set in jsons, no other fields may be set,
			//thy are all ignored
			if (this.url.length>0) {
				if (this.scheme !=undefined) {
					throw 'scheme is not allowed, when url is set';
				}
				if (this.user !=undefined) {
					throw 'user is not allowed, when url is set';
				}
				if (this.password !=undefined) {
					throw 'password is not allowed, when url is set';
				}
				if (this.host !=undefined) {
					throw 'host is not allowed, when url is set';
				}
				if (this.port !=undefined) {
					throw 'port is not allowed, when url is set :' + dojo.toJson(this.json);
				}
				if (this.urlPath !=undefined) {
					throw 'urlPath is not allowed, when url is set';
				}
				return;
			}
			//else build the url
			this.scheme=this.__assertString(this.json.scheme);
			this.user=this.__assertString(this.json.user);
			this.password=this.__assertString(this.json.password);
			this.host=this.__assertString(this.json.host);
			if (this.host.length<1) {
				//if no host is defined, take the one that served the current document 
				this.host=loc.host;
				if (this.scheme.length<1) {
					//current host is used, no scheme defined
					//take the current scheme
					this.scheme=loc.protocol+'//';
				};
			};
			this.port=this.__assertString(this.json.port);
			this.urlPath=this.__assertString(this.json.urlPath);
			
			var url=this.scheme;
			if (this.user.length>0) {
				url=url+this.user;
				if (this.password.length>0) {
					url=url+':'+this.password;
				}
				url=url+'@';
			};
			url=url+this.host;
			if (this.port.length>0) {
				url=url+':'+this.port;
			};
			if (this.urlPath.length>0) {
				//append / fault tolerant
				if (!prefabware.lang.endsWith(url,"/")
					&& !prefabware.lang.startsWith(this.urlPath,"/")) {
					url=url+'/';
				}
				url=url+this.urlPath;
			};			
			this.url=url;
			return;
		}
			
	});
});
