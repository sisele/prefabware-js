// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugins.prefabware.server.cloudbees.tests.CloudbeesTest");
//require all plugins 
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
dojo.require("prefabware.lang");
doh.register("prefabware.plugins.prefabware.server.cloudbees.tests.CloudbeesTest", [ 
{
	name : "store",
	registry : null,
	typeRegistry:null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugins.prefabware.server.cloudbees.tests',fileName:'cloudbeesTest-registry.json'});
		this.typeRegistry=this.registry.typeRegistry;
	},
	runTest : function() {
		
		this.registry.startup();
		var servicePlugin=this.registry.findPlugin('prefabware.service');
		var service=servicePlugin.findService('prefabware.service.appdriver.rest');
		doh.assertTrue(service!=null);
		var url=service.getUrl();
		doh.assertTrue(url=='http://localhost:8080/rest');
		
		var modelService=servicePlugin.findService('prefabware.service.model');
		doh.assertTrue(modelService!=null);
		var url=modelService.getUrl();
		doh.assertTrue(url=='http://localhost:8080/rest/modelService');
		
	},
	tearDown : function() {
	}
}
]);