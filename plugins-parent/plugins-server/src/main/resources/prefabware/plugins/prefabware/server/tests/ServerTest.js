// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugins.prefabware.server.tests.ServerTest");
// require all plugins
dojo.require('prefabware.plugins.prefabware.server.Plugin');
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo.promise.all");

dojo.require("prefabware.lang");
dojo.require("dojo");
dojo.require("dojo.Deferred");

dojo.require("doh");
doh.register("prefabware.plugins.prefabware.server.tests.ServerTest", [ {
	name : "store",
	timeout:1000,
	registry : null,
	
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry({
			namespace : 'prefabware.plugins.prefabware.server.tests',
			fileName : 'serverTest-registry.json'
		});
		
	},
	assertServer : function(serverId,expectedUrl) {
		return this.serverPlugin.findServer(serverId).then(function(server){
			doh.assertTrue(server != null);
			var url = server.getUrl();
			doh.assertTrue(url == expectedUrl);
		});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		
		this.registry.startup().then(function(){
			
		that.serverPlugin = that.registry.findPlugin('prefabware.server');
		var results=[
		that.assertServer('allDetails','http://aUser:aPassword@ahost.com:1234/a/b/c'),
		that.assertServer('allDetailsWoUserPwd','http://ahost.com:1234/a/b/c'),
		that.assertServer('allDetailsWoUserPwdPort','http://ahost.com/a/b/c'),
		that.assertServer('allDetailsWoUserPwdPortUrlPath','http://ahost.com'),
		//should return the current server incl. port
		that.assertServer('woAll','http://localhost:8080'),
		that.assertServer('urlOnly','http://localhost:8080/prefabware-appdriver')
		];
		dojo.promise.all(results).then(function(){
			testDeferred.callback(true);
		});
		
		
		});
		return testDeferred;
			},
	tearDown : function() {
	}
} ]);