package com.prefabware.js.model.client;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.prefabware.commons.logging.LoggingSupport;
import com.prefabware.environment.boot.junit.DevEnvironment;
import com.prefabware.jersey.SpringBeanHandlerInstantiator;
import com.prefabware.spring.commons.BeanFactory;
import com.prefabware.spring.commons.BeanFactorySupport;
public class SpringConfigTest {
	private BeanFactorySupport support;
	private BeanFactory beanFactory;

	private LoggingSupport log;

	@Before
	public void setUp() throws Exception {
		DevEnvironment environment = new DevEnvironment();

		this.support = new BeanFactorySupport();
		Package pkg = this.getClass().getPackage();
		this.support.addAllSpringBeansXml();
		this.support.add(pkg, "applicationContext.xml");
		this.beanFactory = this.support.getBeanFactory();

		log = new LoggingSupport(pkg.getName());
		
	}

	@Test
	public void testConfig() throws Exception {
		SpringBeanHandlerInstantiator handler = this.beanFactory.getBean(SpringBeanHandlerInstantiator.class);
		assertNotNull(handler);
		 ModelService modelService = this.beanFactory.getBean(ModelService.class);
		assertNotNull(modelService);
	}
	
	@After
	public void tearDown() {
		if (this.beanFactory != null) {
			this.beanFactory.close();
		}
	}

	private void log(String string, Object... args) {
		System.out.println(String.format(string, args));
	}

}
