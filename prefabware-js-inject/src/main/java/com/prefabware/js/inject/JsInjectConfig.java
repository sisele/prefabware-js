package com.prefabware.js.inject;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.commons.maven.Maven;
import com.prefabware.js.commons.JsCommonsConfig;
import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

@Configuration
@Import(JsCommonsConfig.class)
public class JsInjectConfig {
	@Bean
	Maven jsInjectMaven() {
		return new Maven(this.getClass().getPackage().getName()+".filtered");
	}
	@Bean
	WebJar jsInjectWebJar(@Qualifier("jsInjectMaven") Maven m) {
		return new WebJar(m.artifactId(), m.version());
	}
	
	@Bean
	ResourceMapping rmWebJarPrefabwareInjector(@Qualifier("jsInjectWebJar") WebJar wj) {
		String path = "prefabware/injector/";
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}
}
