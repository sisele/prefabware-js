package com.prefabware.js.inject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.web.ResourceConfig;

/**
 * @author stefan
 * 
 */
@Configuration
@EnableAutoConfiguration
@Import({JsInjectConfig.class,ResourceConfig.class})
public class SpringBootTestApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringBootTestApplication.class);
    }   

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTestApplication.class);
    }

}
