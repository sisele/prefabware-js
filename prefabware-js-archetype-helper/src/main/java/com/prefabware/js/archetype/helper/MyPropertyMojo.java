package com.prefabware.js.archetype.helper;

import java.util.Properties;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.project.MavenProject;

/**
 * @goal calc-properties
 */
public class MyPropertyMojo extends AbstractMojo {
	/**
	 * @parameter expression="${project}" required="true"
	 */
	private MavenProject mavenProject;

	public void execute() throws MojoExecutionException, MojoFailureException {
		final Properties projectProps = mavenProject.getProperties();
		String package_ = (String) projectProps.get("groupId");
		String jsPath = calcPath(package_);
		projectProps.put("jspath", "calculated path");
		getLog().info(
				"calculated jspath=" + jsPath + " from package=" + package_);
	}

	public String calcPath(String package_) {
		if (package_ == null || package_.length() == 0) {
			return "";
		}
		String[] splitResult = package_.split("\\.");
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < splitResult.length; i++) {
			if (i > 0) {
				String string = splitResult[i];
				if (i > 1) {
					builder.append("/");
				}
				builder.append(string);
			}
		}
		String jsPath = builder.toString();
		return jsPath;
	}
}