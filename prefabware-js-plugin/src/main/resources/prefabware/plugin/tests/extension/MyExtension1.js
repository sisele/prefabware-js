//a Descriptor for a Plugin
define('prefabware/plugin/tests/extension/MyExtension1', [ 'dojo',
                                        'prefabware/plugin/Extension',
                                        'dojo/_base/declare' ], function(
		dojo, Extension) {
	dojo.declare("prefabware.plugin.tests.extension.MyExtension1",[prefabware.plugin.Extension], {
		aPlugin:{inject:'extension.a',type:'plugin'},  	
	});
});

