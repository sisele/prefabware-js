dojo.provide("prefabware.plugin.tests.extension.FindExtensionTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginDefinition");
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");

dojo.require("dojo");
dojo.require("dojo.Deferred");
dojo.require("doh");
doh.register("prefabware.plugin.tests.extension.FindExtensionTest", [ 

{
	name : "load registry" +
			"",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry(
				{namespace:'prefabware.plugin.tests.extension',fileName:'extensionTest-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		this.registry.startup().then(function(){
			var pluginA=that.registry.findPlugin('extension.a');//its a bad name for a plugin..			
			var pluginB=that.registry.findPlugin('extension.b');//its a bad name for a plugin..			
			doh.assertEqual(pluginA.declaredClass,'prefabware.plugin.Plugin');
			doh.assertEqual(pluginB.declaredClass,'prefabware.plugin.Plugin');
			doh.assertTrue(pluginA.started,"plugin a should be started");
			doh.assertTrue(pluginB.started,"plugin a should be started");
			
			pluginA.fetchExtensions('a.extensionpoint1').then(function(exts){
				doh.assertEqual(2,exts.length);
				doh.assertEqual('prefabware.plugin.tests.extension.MyExtension1',exts[0].declaredClass);
				var myExtension=exts[0];
				doh.assertEqual('prefabware.plugin.Plugin',myExtension.aPlugin.declaredClass,'should have aPlugin injected');
				doh.assertEqual(pluginA,myExtension._definition.extendedPlugin,'def should have referenced to the extended Plugin ');
				doh.assertEqual(pluginB,myExtension._definition.plugin,'def should have referenced to the extending Plugin ');
				
				doh.assertEqual('prefabware.plugin.Extension',exts[1].declaredClass);
				testDeferred.callback(true);
			});
			
			
		});
		return testDeferred;
	},
	tearDown : function() {
	}
},

]);