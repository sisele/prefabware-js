dojo.provide("prefabware.plugin.tests.extension.ExtensionByIdTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.ExtensionDefinition");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginDefinition");
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");

dojo.require("dojo");
dojo.require("dojo.Deferred");
dojo.require("doh");
doh.register("prefabware.plugin.tests.extension.ExtensionByIdTest", [ 
{
	name : "matchExtension",
	timeout : 3000,
	extensionPoint : null,
	extension : null,
	registry : null,
	setUp : function() {
		this.registry=new prefabware.plugin.PluginRegistry();
	},
	createExtensionDef : function(plugin,value) {
		var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
		doh.assertTrue(eP!=undefined);
		
		var json={'extensionPoint':'a.extensionpoint1',
				'id':'extension'+'_'+value,
				'matcher'   : {
					'className' : 'prefabware.commons.match.EqualMatcher',
					'value'     : value			 
				}};
		var ext= new prefabware.plugin.ExtensionDefinition({json:json,extensionPoint:eP,extendedPlugin:plugin});
		this.registry.injector.injectInto(ext);
		return ext;
	},
	createExtensionDefWoMatcher : function(plugin) {
		//creates a plugindefinition wthout matcher, so a default matcher will be assigned
		var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
		doh.assertTrue(eP!=undefined);
		
		var json={'extensionPoint':'a.extensionpoint1',
				'id':'extension_withoutmatcher'
		};
		var ext= new prefabware.plugin.ExtensionDefinition({json:json,extensionPoint:eP,extendedPlugin:plugin});
		this.registry.injector.injectInto(ext);
		return ext;
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		var that=this;
		var jsonPlugin={'id':'a.b',
				'comment'    : 'comment of plugin',   
				'extensionPoints' : [
				                     {'id':'a.b.extensionpoint1','min':1,'max':99999},
				                     ],
				                     'dependencies' : [ 
				                                       {'id':'dependency.c',
				                                       },
				                                       {'id':'dependency.d'
				                                       },
				                                       ]
		};
		var def=new prefabware.plugin.PluginDefinition();
		that.registry.injector.injectInto(def);
		def.createPlugin(jsonPlugin)
		.then(function(plugin){
			doh.assertTrue(plugin!=null);
			//this is normally done by the registry
			plugin.injector=that.registry.injector;//allways ! use the injector of the registry 
			var extd0=that.createExtensionDefWoMatcher(plugin);
			var extd1=that.createExtensionDef(plugin,'value1');
			var extd2=that.createExtensionDef(plugin,'value2');
			var extd3=that.createExtensionDef(plugin,'value3');
			var eP=plugin.findExtensionPoint('a.b.extensionpoint1');
			eP.registerExtensionDef(extd0);
			eP.registerExtensionDef(extd1);
			eP.registerExtensionDef(extd2);
			eP.registerExtensionDef(extd3);
			plugin.registerExtensionDef(extd0);
			plugin.registerExtensionDef(extd1);
			plugin.registerExtensionDef(extd2);
			plugin.registerExtensionDef(extd3);
			plugin.fetchExtensionById('extension_withoutmatcher').then(function(extension){
				doh.assertFalse(extension==null);
				doh.assertEqual('extension_withoutmatcher',extension.id,'should match the second extension');
				doh.assertEqual('prefabware.plugin.Extension',extension.declaredClass);
				doh.assertEqual('prefabware.commons.match.NeverMatcher',extension.matcher.declaredClass);
				testDeferred.callback(true);
			});
		});
		
		return testDeferred;
	},
	tearDown : function() {
	}
}
]);