// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.OptionalTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.OptionalTest", [ 
{
	name : "load many plugins from registry",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.optional',fileName:'optionalTest-registry.json'});
	},
	runTest : function() {
		var that=this;
		this.registry.startup().then(function(){
			var plugin=that.registry.findPlugin('optional.a');
			doh.assertTrue(plugin!=null);
		});
			
	},
	tearDown : function() {
	}
},
]);