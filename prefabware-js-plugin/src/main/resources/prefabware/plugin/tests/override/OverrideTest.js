// to test overriding extensiondefinition
dojo.provide("prefabware.plugin.tests.override.OverrideTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.override.OverrideTest", [ 
{
	name : "override",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.override',fileName:'overrideTest-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		this.registry.startup().then(function(registry){
			var aPlugin=registry.findPlugin('override.a');
			aPlugin.fetchExtensions('override.a.typeicon').then(function(extensions){
				doh.assertEqual(1,extensions.length,'1 extension expected');
				var extensionA=extensions[0];
				doh.assertFalse(extensionA==null);				
				doh.assertEqual('overridingIcon',extensionA.json.icon,'expected the icon from the overriding extension');
				
				testDeferred.callback(true);
			});
		});
		return testDeferred;
		
	},
	tearDown : function() {
	}
}
]);