// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.CycleRepeatedTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("dojo.Deferred");
dojo.require("dojo.aspect");
dojo.require("doh");
doh.register("prefabware.plugin.tests.CycleRepeatedTest", [ 
{
	//this test was meant to reproduce an error that happend in app-crm
	//where a plugin cycle was detected, but wasnt really a cicle
	//it was just a dependency, laoded a second time
	//anyway cannot reproduce that here
	name : "",
	registry : null,
	extensionPoint : null,
	extension : null,
	 timeout: 5000, // 
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.cycleRepeated',fileName:'cycleRepeatedTest-registry.json'});
		var calls=0;
		 dojo.aspect.after(this.registry, "loadPlugin", function(deferred){
			   return deferred.then(function(){
				   var d2=new dojo.Deferred();
				   setTimeout(function(){d2.resolve();}, 1000-calls*200);
				   calls++;
				   d2.promise.debug="the timeout";
				   return d2.promise;
			   });
			  });
	},
	runTest : function() { 
		var td=new doh.Deferred();
		this.registry.startup()
			.then(function(registry){
				td.callback(true);	
			});
		return td;
	},
	tearDown : function() {
	}
},
]);