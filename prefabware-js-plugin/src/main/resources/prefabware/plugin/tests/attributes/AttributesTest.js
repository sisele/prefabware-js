// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.plugin.tests.attributes.AttributesTest");
// Import in the code being tested.
dojo.require("prefabware.plugin.Extension");
dojo.require("prefabware.plugin.ExtensionPoint"); 
dojo.require("prefabware.plugin.PluginRegistry");
dojo.require("prefabware.plugin.Plugin");
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.plugin.tests.attributes.AttributesTest", [ 
{
	name : "attributes",
	registry : null,
	extensionPoint : null,
	extension : null,
	setUp : function() {
		this.registry = new prefabware.plugin.PluginRegistry( 
				{namespace:'prefabware.plugin.tests.attributes',fileName:'attributesTest-registry.json'});
	},
	runTest : function() {
		var testDeferred=new doh.Deferred();
		this.registry.startup().then(function(registry){
			var aPlugin=registry.findPlugin('attributes.a');
			doh.assertFalse(aPlugin==null);
			var bPlugin=registry.findPlugin('attributes.b');
			doh.assertFalse(bPlugin==null);
			
			bPlugin.fetchExtensions('attributes.b.typeicon').then(function(extensions){
				var extensionA=extensions[0];
				doh.assertFalse(extensionA==null);				
				doh.assertEqual('prefabware.plugin.Extension',extensionA.declaredClass,'no special extension class expected');
				doh.assertEqual('theicon',extensionA.json.icon,'icon expexted');
				
				testDeferred.callback(true);
			});
		});
		return testDeferred;
		
	},
	tearDown : function() {
	}
}
]);