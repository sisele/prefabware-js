//a view that can be shown in the workspace
define('prefabware/plugin/tests/registry/a/Extension1', [ 
       'dojo',
       'prefabware/plugin/ExtensionPoint',
       'dojo/_base/array',
       'dojo/_base/declare' ], function(
		dojo, Extension,array) {
	dojo.declare("prefabware.plugin.tests.registry.a.Extension1", [prefabware.plugin.ExtensionPoint], {
		modelPlugin:{inject:'prefabware.model',type:'plugin'},  	
		constructor : function() {
		},
		startup : function() {
		},	
		doSomething : function() {
			return "doSomething";
		},
	});
});
