//Plugin defines ExtionsPoints, in which other Plugins can hook in
//and  declares Extensions for ExtionPoints of other plugins
define('prefabware/plugin/Plugin', [ 'dojo', 'dojo/_base/array', 'dojo/aspect',
		'dojo/promise/all','dojo/_base/lang',
		'prefabware/lang',
		'dojox/collections/Dictionary',
		'dojo/_base/Deferred', 'dojo/_base/declare' ], function(dojo,
		array, aspect,promiseAll,lang) {
	dojo.declare("prefabware.plugin.Plugin", null, {
		namespace:null,
		id : null,
		startLevel:50000,// normal startlevel, after system and typeregistry
		extensionPoints : null,
		extensionDefinitions:null,
		pluginRegistry : null,
		injector:{inject:'injector',type:'injector'},//to inject into async created extensions!	
		started:false,// true if this plugin was started
		constructor : function(id) {
			this.id = id;
			this.extensionPoints = new dojox.collections.Dictionary();
			this.extensionDefinitions = new dojox.collections.Dictionary();
		},
		qualifiedId : function() {
			return this.namespace+'.'+this.id;
		},		
		__assertExactlyOne : function(extensions, pointId) {
			// TODO should not throw exceptions but check the min/max
			// constraints of the extension
			// returns exactly one extension, the first of the given array
			// if more are in the array, or none , an exception is thrown
			var point=this.findExtensionPoint(pointId);
			if (point==undefined) {
				throw 'cannot find point '+ pointId+' for plugin '+this.id;
			}
			if (!extensions||extensions.length===0) {
				if (point.min<=0) {
					return undefined;
				}else{
					throw 'no extension found for extensionPoint ' + pointId;
				}
			} else if (extensions.length > 1) {
				throw 'found ' + extensions.length
						+ ' extensions, expected exactly 1' + pointId;
			} else {
				// return the only one
				return extensions[0];
			}
		},
		getInstances : function(pointId) {
			var result = new Array();
			var point = this.findExtensionPoint(pointId);
			var extensions = this.findExtensionsByPoint(pointId);
			for ( var index = 0; index < extensions.length; index++) {
				var extension = extensions[index];
				var instance = extension.getInstance();
				point.postCreate(instance);
				result.push(instance);
			}
			// returns an array with all registered extension for the given
			// ExtensionPointId
			return result;
		},
		startup:function(){
			//sync or async
			//returns a promise or nothing
			//subclasses may override this to inizialize the plugin
			//if a promise is returned, startup-phase will wait until resolved
		},				
		__startup:function(){
			//async
			//returns a promise or nothing
			//internal method to inizialize the extension,subclasses MUST NOT override this 
			array.forEach(this.extensionPoints.getValueList(), function(point, i){
				point.__startup();
			});
			this.started=true;
			var p=this.startup();
			return this.__promiseOrNullPromise(p);
		},

		afterStartup : function(extension) {
			//sync or async
			//returns a promise or nothing
			// is called after all plugins have gone through startup
			//if a promise is returned, startup-phase will wait until resolved
			
		},
		__afterStartup : function(extension) {
			//async
			//returns a promise or nothing
			// is called after all plugins have gone through startup
			var that=this;
			var promises=array.map(this.extensionPoints.getValueList(), function(point, i){
				return point.__afterStartup();
			});
			return promiseAll(promises).then(function(){
				var p= that.afterStartup();
				return that.__promiseOrNullPromise(p);
			});
		},
		__promiseOrNullPromise : function(p) {
			//if p is a promise it is returned
			//if p is undefined a promise resolving to null is returned
			if (p!=undefined) {
				return p;
			}else{
				return prefabware.lang.deferredValue(null);
			}
		},
		registerExtensionPoint : function(extensionPoint) {
			this.extensionPoints.add(extensionPoint.id, extensionPoint);
		},
		fetchExtensionById : function(extensionId,optional) {
			//!!CAUTION!! extensionId not pointId !!!
			//async
			//optional=true - the extension is optional, so do not throw an exception here
			//returns a promise with the extension with the given extension id
			var def =this.extensionDefinitions.item(extensionId);
			if (def!=null) {
				return def.createExtension();
			}else if (optional) {
				//extension is optional, return null
				return prefabware.lang.deferredValue();
			}else {
				throw 'cannot find extension with id '+extensionId+' of plugin '+this.id;
			}
		},
		fetchExtension : function(pointId) {
			//async
			//returns the extension for the point with the given id
			//see ExtensionPoint.fetchExtension for details
			var point =this.findExtensionPoint(pointId);			
			return point.fetchExtension();
		},
		fetchExtensions : function(pointId) {
			//async
			//returns all registered extension for the point with the given id
			//see ExtensionPoint.fetchExtensions for details
			var point =this.findExtensionPoint(pointId);			
			return point.fetchExtensions();
		},
		findExtensionPoint : function(extensionPointId) {
			var point =this.extensionPoints.item(extensionPointId);
			if (point==undefined) {
				throw 'cannot find extensionPoint '+extensionPointId+' of plugin '+this.id;
			}
			return point;
		},
		filterExtensions : function(filter, pointId) {
			// filter : a method that returns true for extionsion that should be
			// included in the result
			// 1 arguments is passed to this method, the extension
			var extensions = this.findExtensionsByPoint(pointId);
			var filtered = array.filter(extensions, filter);
			// TODO check result.length against min/max of the ExtensionPoint
			return filtered;
		},
		matchExtension : function(pointId,options) {
			//async
			var that=this;
			// returns the best matching extension for the given point and
			// options
			// the typ of 'options' depends on the ExtensionPoint. some may
			// expect a type or an attribute or something else
			var point =this.findExtensionPoint(pointId);
			return point.matchExtension(options)
				.then(function(extension){
					if (extension==null&&point.hasDefault) {
						return that.createDefaultExtension(point,options)
							.then(function(defExtension){
								point.cacheBest(options,defExtension);
								return defExtension;
							});
					}else{
						return extension;
					}
			});						
		},		
		matchExtensions : function(pointId,options) {
			//async
			var that=this;
			// returns an array with all matching extensions for the given point and
			// options
			// the typ of 'options' depends on the ExtensionPoint. some may
			// expect a type or an attribute or something else
			var point =this.findExtensionPoint(pointId);
			return point.matchExtensions(options)
				.then(function(extensions){
					if (extensions.length==0&&point.hasDefault) {
						return that.createDefaultExtension(point,options)
							.then(function(extension){
								point.cacheAll(options,[extension]);
								return [extension];
							});
					}else{
						return extensions;
					}
				});						
		},		
		createDefaultExtension : function(point,matcherOptions) {
			//async
			// creates and registers a default extension
			// the extensionPoint must be defined with
			// defaultExtensionClass:'<CLASSNAME>'
			//create a extensiondefinition
			var cloned=lang.clone(point.defaultExtension);
			//TODO use the matcher to convert the options into a string, he knows best
			var matcherString=point.matchOptionsToString(matcherOptions);
			cloned.matcher=dojo.fromJson(matcherString);
			return this.__createExtension(cloned,point);
		},
		__createExtension : function(jExtension, point) {
			//async
			// creates an extension for this plugin
			// for the extensionpoint of this plugin
			// using the json data of jExtension
			var extensionDef = this.pluginRegistry.__createExtension(jExtension,
					this, this, point);
			this.pluginRegistry.__registerExtension(extensionDef);
			// inject beans into the new extension
			this.pluginRegistry.injector.injectInto(extensionDef);
			// may be caller wants to do this himself ?
			return extensionDef.createExtension();
		},
		registerExtensionDef : function(extensionDef) {
			//registers the given extension
			//so the plugin can access them by id
			//returns nothing
			//if (this.extensionDefinitions.containsKey(extensionDef.id)) {
				//do not throw an exception here, the point takes care of override
				//just store it here, if an extension allredy is registered with the same id
				//it will be overridden
				//throw "a extension with id "+extensionDef.id+' is allready registered';
			//}
			this.extensionDefinitions.add(extensionDef.id,extensionDef);
		},
		__registerExtensions : function(plugin) {
			// before all Extensionpoints were registered through
			// registerPlugin.
			// now we can register the extensions
			// register all the extensions that the plugin offers
			array.forEach(plugin.json.extensions, function(jextension, i){
				var point=this.findExtensionPoint(jextension.extensionPoint);
				if (!point) {
					throw('could not find extensionPoint '+jextension.extensionPoint+' extended by plugin '+plugin.id);
				}
				var owner=point.plugin;
				var extension=this.__createExtension(jextension,plugin,owner,point);
				this.__registerExtension(extension);
			},this);
			prefabware.lang.log('finished 2. pass for plugin '+plugin.id);
		},
	});
});
