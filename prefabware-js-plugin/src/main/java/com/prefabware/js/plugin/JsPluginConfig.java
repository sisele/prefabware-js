package com.prefabware.js.plugin;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.commons.maven.Maven;
import com.prefabware.js.commons.JsCommonsConfig;
import com.prefabware.js.inject.JsInjectConfig;
import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

@Configuration
@Import({JsInjectConfig.class,JsCommonsConfig.class})
public class JsPluginConfig {
	@Bean
	Maven jsPluginMaven() {
		return new Maven(this.getClass().getPackage().getName()+".filtered");
	}
	@Bean
	WebJar jsPluginWebJar(@Qualifier("jsPluginMaven") Maven m) {
		return new WebJar(m.artifactId(), m.version());
	}
	
	@Bean
	ResourceMapping rmJsPluginWebJar(@Qualifier("jsPluginMaven") Maven m,@Qualifier("jsPluginWebJar") WebJar wj) {
		String path = m.getProperty("pfw.static.path");
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}
}
