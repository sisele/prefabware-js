 package com.prefabware.js.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prefabware.js.test.DohTestUrlBuilder;
import com.prefabware.js.test.DojoTestSupport;
import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootTestApplication.class)
// port:0 = use a random, free port
@WebIntegrationTest("server.port:0")
public class DojoIntegrationTest {
	private DojoTestSupport testSupport;

	@Value("${local.server.port}") int port;

	@Autowired @Qualifier("jsCommonsWebJar") WebJar cwj;
	@Autowired @Qualifier("thirdpartyDojoWebJar") WebJar dwj;
	@Autowired @Qualifier("rmWebJarDojoUtil") ResourceMapping rmWebJarDojoUtil;
	@Autowired @Qualifier("rmWebJarPrefabware") ResourceMapping rmWebJarPrefabware;

	@Before
	public void setUp() throws Exception {
		String baseUrl = "http://localhost:" + Integer.toString(port);
		testSupport = new DojoTestSupport(baseUrl);

		DohTestUrlBuilder b = new DohTestUrlBuilder();
		b
				.withDohUtilBaseUrl(rmWebJarDojoUtil.urlPath())
				.withModule("prefabware", rmWebJarPrefabware.urlPath());

		testSupport.add(b.withTestClass("prefabware.injector.tests.InjectorTest").build());
		testSupport.add(b.withTestClass("prefabware.model.to.tests.multiTypeFile.LoadTypesFromFileTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.tests.RecursiveTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.to.tests.LoadTypeFromFileTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.tests.AttributeMatcherTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.tests.TypeMatcherTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.tests.TypenameResolverTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.tests.TypeTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.tests.TypeResolverTest").build()); 	
    	testSupport.add(b.withTestClass("prefabware.model.tests.TypeDefaultTest").build()); 	
	}

	@Test
	public void testAll() throws Exception {
		testSupport.testAll();
		return;
	}
}