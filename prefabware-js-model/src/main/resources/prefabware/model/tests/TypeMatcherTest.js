// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.model.tests.TypeMatcherTest");
// Import in the code being tested.
dojo.require("prefabware.model.ValueType");
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.TypeResolver");
dojo.require("prefabware.model.match.TypeMatcher");
dojo.require("prefabware.model.EntityType");
dojo.require("dojo._base.lang");
doh.register("prefabware.model.tests.TypeMatcherTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	registry:null,
	typeResolver:null,
	setUp : function() {
		this.registry = new prefabware.model.TypeRegistry();
		this.typeResolver=new prefabware.model.TypeResolver(this.registry);
		this.STRING = this.registry.STRING;
		this.DECIMAL = this.registry.DECIMAL;
		this.BOOLEAN = this.registry.BOOLEAN;
		this.TYPE = this.registry.TYPE;
		this.COMPOSITE = this.registry.COMPOSITE;
		this.ENTITY = this.registry.ENTITY;
	},
	runTest : function() {
		var cityType = new prefabware.model.EntityType('test.City');
		cityType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
			//use a simple value
			default_:'Cologne'
		});
		var partyType = new prefabware.model.EntityType('test.Party');
		partyType.addAttribute({
			name : 'name',
			type : this.STRING,
			length : 30,
			//use a simple value
			default_:'Smith'
		});
		partyType.addAttribute({
			name : 'city',
			type : cityType
		});
		partyType.addAttribute({
			name : 'firstName',
			type : this.STRING,
			length : 30,
			//use a function to calculate the default
			default_:function(instance){return instance.name+'y';}
		});
		var personType = new prefabware.model.EntityType('test.Person');
		personType.superType=partyType;
		personType.addAttribute({
			name : 'firstName',
			type : this.STRING,
			length : 30,
			//use a simple value
			default_:'Cologne'
		});
		
		this.registry.registerType(cityType);
		this.registry.registerType(partyType);
		this.registry.registerType(personType);
		
		doh.assertEqual( cityType,this.registry.findType('test.City'));
		doh.assertEqual( partyType,this.registry.findType('test.Party'));

		var subTypeMatcher=new prefabware.model.match.TypeMatcher({
			type:'prefabware.model.EntityType',
			subTypes:true,
			});
		doh.assertEqual( 50,subTypeMatcher.matches({type:cityType}).degree);
		
		var matcher=new prefabware.model.match.TypeMatcher({type:cityType.name});
		
		doh.assertEqual( -1,matcher.matches({type:partyType}).degree);
		doh.assertEqual( 100,matcher.matches({type:cityType}).degree);
		
		var cacheMatcher=new prefabware.model.match.TypeMatcher({type:cityType.name,matches:'options.cache==true'});
		doh.assertEqual( -1,cacheMatcher.matches({type:cityType}).degree);
		doh.assertEqual( 100,cacheMatcher.matches({type:cityType,cache:true}).degree);
		
		
		var allMatcher=new prefabware.model.match.TypeMatcher({type:'.*'});
		
		return;
	},
	tearDown : function() {
	}
} ]);