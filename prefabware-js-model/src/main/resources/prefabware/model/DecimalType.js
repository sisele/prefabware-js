//a numeric type , will be right adjusted
define('prefabware/model/DecimalType', 
		[ 'dojo',  'dojo/_base/lang', "dojo/date/locale",'prefabware/model/NumberType','prefabware/model/Attribute',
		  'dojo/_base/declare' ], function(dojo,lang,locale,Type) {
	dojo.declare('prefabware.model.DecimalType', [prefabware.model.NumberType], {
		decimals:null,
		constructor : function(name) {
		},
		create : function() {
			return 0;
		},
	labelOf : function(value) {
		var pattern='###,###,##0.00';
		var formatted =dojo.number.format(value, {
			pattern: pattern
		  });

		return formatted;
	},
		
	});
});
