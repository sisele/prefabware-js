//can be mixed into a Type to make it an Enumeration
define('prefabware/model/_EnumerationType', 
		[ 
		  'dojo', 
		  'dojo/_base/lang', 
		  "dojo/date/locale",
		  'dojox/collections/Dictionary',
		  'dojo/_base/declare' ], function(dojo,lang,locale) {
	dojo.declare('prefabware.model._EnumerationType', null, {
		isEnumeration:true,//true if this is an enumeration
		elements:null,
		constructor : function(name) {
			this.elements=new dojox.collections.Dictionary();
		},
		__addElement : function(jElement) {
			//a element must have a name and a value
			var element  = this.__create(jElement);
			this.elements.add(element.name,element);
		},	
		__create : function(jelement) {
			var element=this.create(jelement);
			dojo.mixin(element,jelement);
			//enhance the element
			//name the methods as they are named in composite or entity
			element.__type=this;
			this[element.name.toUpperCase()]=element;
			//give an equals method to the element
			element.__equals=function(other){
				if (other==null||other==undefined) {
					return false;
				};
				if (this.type!=other.type) {
					return false;
				}
				//they are equal, if the value of the key attribute is equal
				return this.name===other.name;
			};
			element.__label = function() {				
				return element.name;
			};
			element.__serializable = function() {		
				//enums are serialized by there name only
				return element.name;
			};
			return element;
		},	
		__findElement : function(value) {
			return this.elements.item(value);
		},
		serializableOf : function(element) {
			if (element==null) {
				return null;
			}
			return element.__serializable();
		},
	});
});
