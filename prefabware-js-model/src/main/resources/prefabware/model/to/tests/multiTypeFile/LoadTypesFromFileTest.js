// to test the typeimporter and type customization
dojo.provide("prefabware.model.to.tests.multiTypeFile.LoadTypesFromFileTest");
// Import in the code being tested.
dojo.require("prefabware.model.TypeRegistry");
dojo.require("prefabware.model.to.TypeImporter");
dojo.require("prefabware.model.to.TypeLoaderFile");
dojo.require("prefabware.model.EntityType");
dojo.require("prefabware.util.ResourceJsonFile");
dojo.require("dojo._base.lang");
dojo.require("dojo.cache");
dojo.require("dojo.aspect");
dojo.require("prefabware.model.to.tests.Currency");
doh.register("prefabware.model.to.tests.multiTypeFile.LoadTypesFromFileTest", [ {
	name : "extend",
	STRING : null,
	DECIMAL : null,
	registry:null,
	loader:null,
	importer:null,
	setUp : function() {
		this.loader = new prefabware.model.to.TypeLoaderFile();
		this.registry = new prefabware.model.TypeRegistry({loader:this.loader});
		
		this.importer = this.registry.importer;
		this.STRING = this.registry.STRING;
		this.DECIMAL = this.registry.DECIMAL;
		this.BOOLEAN = this.registry.BOOLEAN;
		this.TYPE = this.registry.TYPE;
		this.COMPOSITE = this.registry.COMPOSITE;
		this.ENTITY = this.registry.ENTITY;
	},
	runTest : function() {
		var jr = new prefabware.util.ResourceJsonFile({qualifiedName:"prefabware.model.to.tests.multiTypeFile.types.json"});
		this.loader.addJsonResource(jr);
				
		var baseUnitType=this.registry.findType("com.prefabware.business.commons.BaseQuantitiyUnit");
		doh.assertTrue(baseUnitType!=null);
		doh.assertEqual('com.prefabware.business.commons.BaseQuantitiyUnit',baseUnitType.name);
		doh.assertEqual(6,baseUnitType.elements.count);
		doh.assertEqual("GRAM",baseUnitType.GRAM.name);
		
		var quantityUnitType=this.registry.findType("com.prefabware.business.commons.QuantityUnit");
		doh.assertTrue(quantityUnitType!=null);
		var qu=quantityUnitType.create();
		qu.baseUnit=baseUnitType.GRAM;
		doh.assertEqual("GRAM",qu.__labelOf("baseUnit"));
		doh.assertEqual("GRAM",qu.baseUnit.__serializable());
		
		doh.assertEqual("GRAM",qu.__serializable().baseUnit,"enums should be serialized as there name only");
		doh.assertEqual("GRAM",qu.__clone().__serializable().baseUnit,"enums should be serialized as there name only");
		
		var currency2Type=this.registry.findType("com.prefabware.business.commons.Currency2");
		doh.assertTrue(currency2Type!=null);
		doh.assertEqual('com.prefabware.business.commons.Currency2',currency2Type.name);

		var currencyType=this.registry.findType("com.prefabware.business.commons.Currency");
		doh.assertTrue(currencyType!=null);
		doh.assertEqual('com.prefabware.business.commons.Currency',currencyType.name);
		
		return;
	},
	tearDown : function() {
	}
} ]);