//
define('prefabware/model/to/tests/Currency', [ 
                                        'dojo',
                                        'dojo/currency',
                                        'prefabware/lang',
                                        'dojo/_base/declare' ],
                                function(dojo,localeCurrency) {
	dojo.declare("prefabware.model.to.tests.Currency", null, {
		myType:"prefabware.model.to.tests.Currency",
		constructor : function(type) {
			prefabware.lang.assert(type!=null);
		},
		__label : function() {
			return 'label of Currency';
		}	

	});
});
