//a numeric type , will be right adjusted
define('prefabware/model/BooleanType', 
		[ 'dojo',  'dojo/_base/lang', "dojo/date/locale",'prefabware/model/Type','prefabware/model/Attribute',
		  'dojo/_base/declare' ], function(dojo,lang,locale,Type) {
	dojo.declare('prefabware.model.BooleanType', [prefabware.model.Type], {
		constructor : function(name) {
		},
		create : function() {
			return 0;
		},
	
	stringOf : function(value) {
		return value+'';
	},
	isValue : function(value) {
		return value===true || value===false;
	},
	convertFrom : function(rawValue) {	
		if (!rawValue) {
			return false;
		}
		//parses a boolean
		switch(rawValue){
		case true:case 1:case "true": case "1": return true;
		case false:case 0:case "false": case "0": return false;
		default: throw 'canot convertFrom '+rawValue+' to value of BooleanType';
		}
	},
	});
});
