//matches a type against the configured type name
//can use a regex pattern for types e.g. '.*' for all types
define('prefabware/model/match/TypeMatcher', [ 'dojo',
		'prefabware/commons/match/Matcher', 'prefabware/lang',
		'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.model.match.TypeMatcher",
			[ prefabware.commons.match.Matcher ], {
			subTypes:false,//match subtypes also
				constructor : function(options) {
					this.type=options.type;//it must be a string !
					if (this.type==null) {
						//may happen when an instance of a subclass is created
						return;
					}
					if (options.subTypes!=null) {
						prefabware.lang.assert(this.type.indexOf('*')<0,'if matcher.subTypes=true, the type must not be generic');
						this.subTypes=options.subTypes;
					}
					this.degree = this.GENERIC_MATCH.degree;// generic match
					if (this.type.indexOf('*')>0) {
						//regex search
						this.regEx=new RegExp(this.type);
					} else {
						//exact search
						this.regEx=new RegExp('^'+this.type+'$');
					}
				},
				optionsToString : function(options) {
					var clone=dojo.mixin({}, options);
					if (clone.type!=null&&clone.type.name!=null) {
						clone.type=clone.type.name;
					}
					return dojo.toJson(clone);
				},
				matches : function(options) {
					if (options.type==null) {
						throw 'options.type must not ne null !!';
					};
					//if evalMatcher is defined
					if (this.evalMatcher!=null) {
						var type=options.type;//defined for easy access in the eval
						var matchesEval=eval(this.evalMatcher);
						if (!matchesEval) {
							prefabware.lang.log('matcher with evalMatcher='+this.evalMatcher+' matches options.type='+type.name+' result: NO_MATCH');
							return this.NO_MATCH;
						}
					}
					//this.type is a string, may be a pattern
					//options.type must be a type
					if (this.subTypes==true) {
						var type=options.type;
						var this_type=type.typeRegistry.findType(this.type);//this.type is just a string
						if (this.subTypes==true&&this_type.isSuperTypeOf(options.type)) {
							var that=this;
							result=	{
									matcher:that,
									degree:that.degree
							};
							prefabware.lang.log('matcher with this.type='+this.type+' matches options.type='+options.type.name+' result.degree='+result.degree);
							return result;
						}
					}
					var name=null;
					if (options.type!=null) {
						name=options.type.name;
					}else if (options.typeName!=null) {
						name=options.typeName;
					}else {
						throw 'options.type or options.typeName must be set';
					}
					
					var result=null;
					if (this.type===name) {
						var that=this;
						result=	{
								matcher:that,
								degree: this.EXACT_MATCH.degree
								};
						prefabware.lang.log('with this.type='+this.type+' matches options.type.name='+name+' result.degree='+result.degree);
						return result;
					}
					else if ( this.subTypes==false&&this.regEx.test(name)) {
							var that=this;
							result=	{
									matcher:that,
									degree:that.degree
							};
							prefabware.lang.log('with this.type='+this.type+' matches options.type.name='+name+' result.degree='+result.degree);
							return result;
					}
					 else{
						prefabware.lang.log('with this.type='+this.type+' matches options.type.name='+name+' result: NO_MATCH');
						return this.NO_MATCH;
					}
				},
			});
});
