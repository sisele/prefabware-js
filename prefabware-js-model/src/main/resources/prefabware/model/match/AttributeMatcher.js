//to match qualified attribute names like "com.prefabware.business.commons.Amount.value"
//is configured with a qualified attribute name and an optional typename
//matches this against a QualifiedAttribute
//uses regex to match
//generic matches must use '.*' instead of '*'
//com.prefabware.commons.Customer.address matches     com.prefabware.commons.Customer.address
//com.prefabware.commons.Customer.a.*     matches     com.prefabware.commons.Customer.address 
//com.prefabware.commons.Customer.a.*     matches NOT com.prefabware.commons.Customer.zipCode 
//com.prefabware.commons..*.address       matches     com.prefabware.commons.Customer.address
//can also match to a type, meaning that it applies to all attributes of the given type
define('prefabware/model/match/AttributeMatcher', [ 'dojo','dojo/_base/lang',
		'prefabware/commons/match/Matcher',
		'prefabware/model/match/TypeMatcher',
		'prefabware/model/QualifiedAttribute',
		'prefabware/lang',
		'dojo/_base/declare' ], function(dojo,lang) {
	dojo.declare("prefabware.model.match.AttributeMatcher",
			[ prefabware.commons.match.Matcher ], {
		qattribute:null,//the pattern,its a string !	
		type:null,//the pattern its a string, the type of the attribute to match
		typeMatcher:null,//may delegate to this
		optionsToString:function(options) {
			var clone=dojo.mixin({}, options);
			if (clone.qattribute!=null&&clone.qattribute.name!=null) {
				clone.qattribute=clone.qattribute.name;
			}
			if (clone.type!=null&&clone.type.name!=null) {
				clone.type=clone.type.name;
			}
			return dojo.toJson(clone);
		},		
		constructor : function(options) {
					prefabware.lang.assert(options!=null,'options must be passed');
					prefabware.lang.assert(options.qattribute!=null||options.type!=null,'options.qattribute or options.type or both must be set');
					if (options.qattribute!=null) {
						prefabware.lang.assert(typeof options.qattribute=="string",'options.qattribute must be a string with a pattern for a name of a qualified attribute');
						this.qattribute=options.qattribute;//it must be a string pattern!
					}
					if (options.type!=null) {
						prefabware.lang.assert(typeof options.type=="string",'options.type must be a string with a pattern for a name of a qualified attribute');
						this.type=options.type;//it must be a string pattern!
						var typeOptions=lang.clone(options);
						delete typeOptions.matches;//the eval of a attributeMatcher doesnt work for the typeMatcher
						this.typeMatcher=new prefabware.model.match.TypeMatcher(typeOptions); 
					}
					
					this.degree = this.GENERIC_MATCH.degree;// generic match
					this.regEx=new RegExp(this.qattribute);
		},
				setOwner : function(owner) {
					this.owner=owner;
					this.typeMatcher.setOwner(owner);
				},
				matches : function(options) {
					//this.attribute is a string, may be a pattern
					//options.attribute must be a attribute

					if (options.qattribute==null) {
							//doesNotApply may throw an exception or not
						this.doesNotApply('supplied value is not a string, value='+options.value);
						return this.NO_MATCH;
					}
					if (!options.qattribute.isInstanceOf(prefabware.model.QualifiedAttribute)) {
						this.doesNotApply('options.qattribute must be an instanceof QualifiedAttribute');
						return this.NO_MATCH;
					}
					var oQattribute=options.qattribute;
					var oType=options.qattribute.attribute.type;
					var result=null;
					//if evalMatcher is defined
					if (this.evalMatcher!=null) {
						var qattribute=oQattribute;//defined for easy access in the eval
						var matchesEval=eval(this.evalMatcher);
						if (!matchesEval) {
							prefabware.lang.log('matcher with evalMatcher='+this.evalMatcher+' matches options.qattribute='+oQattribute+' result: NO_MATCH');
							return this.NO_MATCH;
						}
					}
					//if the typeMatcher is defined, see if we match the type of the attribute
					if (this.type!=null) {
						var matchesType=this.typeMatcher.matches({type:oType});
						if (matchesType.degree<=0||this.qattribute==null) {
							return matchesType;
						}
					}
					if (this.qattribute===oQattribute.name) {
						var that=this;
						result=	{
								matcher:that,
								degree: this.EXACT_MATCH.degree
								};
						prefabware.lang.log('with this.attribute='+this.qattribute+' matches options.qattribute='+oQattribute+' result.degree='+result.degree);
						return result;
					}
					else if ( this.regEx.test(oQattribute.name)) {
							var that=this;
							result=	{
									matcher:that,
									degree:that.degree
							};
							prefabware.lang.log('with this.qattribute='+this.qattribute+' matches options.qattribute='+oQattribute+' result.degree='+result.degree);
							return result;
					} else{
						prefabware.lang.log('with this.qattribute='+this.qattribute+' matches options.qattribute='+oQattribute+' result: NO_MATCH');
						return this.NO_MATCH;
					}
				},
			});
});
