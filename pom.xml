<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<url>http://www.prefabware.com</url>
	<organization>
		<name>prefabware</name>
		<url>http://www.prefabware.com</url>
	</organization>
	<scm>
		<connection>scm:git:https://bitbucket.org/sisele/prefabware-js</connection>
		<tag>HEAD</tag>
	</scm>

	<inceptionYear>2013</inceptionYear>

	<parent>
		<artifactId>maven-parent</artifactId>
		<groupId>com.prefabware</groupId>
		<version>1.0.14-SNAPSHOT</version>
	</parent>

	<artifactId>prefabware-js-parent</artifactId>
	<version>1.0.7-SNAPSHOT</version>
	<packaging>pom</packaging>

	<properties>
		<selenium.driver>selenium-firefox-driver</selenium.driver>
		<selenium.version>2.39.0</selenium.version>
		<commons.nodep.version>1.0.11</commons.nodep.version>
		<prefabware.version>1.0.14-SNAPSHOT</prefabware.version>
		<business.version>1.0.7-SNAPSHOT</business.version>
		<jersey.version>1.12</jersey.version>
		<slf4j.version>1.6.1</slf4j.version>
	</properties>
	<dependencyManagement>
		<dependencies>
			<dependency>
				<!-- this has to be declared here putting this import into businees-bom 
					did not work ! -->
				<artifactId>prefabware-bom</artifactId>
				<groupId>com.prefabware</groupId>
				<type>pom</type>
				<version>${prefabware.version}</version>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>com.prefabware</groupId>
				<artifactId>business-bom</artifactId>
				<version>${business.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<repositories>
		<repository>
			<id>repository.springframework.milestone</id>
			<name>Spring Framework Maven Milestone Repository</name>
			<url>http://maven.springframework.org/milestone</url>
		</repository>
	</repositories>
	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.eclipse.jetty</groupId>
					<artifactId>jetty-maven-plugin</artifactId>
					<configuration>
						<webAppConfig>
							<contextPath>/${project.artifactId}</contextPath>
							<!-- limit the scanner for web applications on the jar of this project -->
							<webInfIncludeJarPattern>${project.artifactId}-*.</webInfIncludeJarPattern>
							<resourceBases>
								<!-- could not find an easy way to configure the url path for the 
									webapp in tomcat allways needs /js in the url. for jetty its no problem to 
									configure this, but to have the same urls in jetty and in tomcat we will 
									use a root above /js here also -->
								<resourceBase>${project.basedir}/src/main/webapp</resourceBase>
								<resourceBase>${basedir}/target/${project.name}-${project.version}</resourceBase>
							</resourceBases>
						</webAppConfig>
						<stopPort>9966</stopPort>
						<stopKey>foo</stopKey>
						<!-- stopWait The maximum time in seconds that the plugin will wait 
							for confirmation that jetty has stopped. If false or not specified, the plugin 
							does not wait for confirmation but exits after issuing the stop command. -->
						<stopWait>2</stopWait>
						<!-- allways scan for changes -->
						<scanIntervalSeconds>1</scanIntervalSeconds>
						<!-- allways use test scope -->
						<useTestScope>true</useTestScope>
					</configuration>

					<executions>
						<execution>
							<id>start-jetty</id>
							<phase>pre-integration-test</phase>
							<goals>
								<goal>run</goal>
							</goals>
							<configuration>
								<daemon>true</daemon>
								<useTestScope>true</useTestScope>
							</configuration>
						</execution>
						<execution>
							<id>stop-jetty</id>
							<phase>post-integration-test</phase>
							<goals>
								<goal>stop</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-assembly-plugin</artifactId>
					<configuration>
						<descriptor>${basedir}/src/main/assembly/zip.xml</descriptor>
					</configuration>
					<executions>
						<execution>
							<phase>package</phase>
							<goals>
								<goal>single</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<configuration>
						<skip>true</skip>
						<trimStackTrace>false</trimStackTrace>
					</configuration>
					<executions>
						<execution>
							<id>unit-tests</id>
							<phase>test</phase>
							<goals>
								<goal>test</goal>
							</goals>
							<configuration>
								<skip>false</skip>
								<includes>
									<include>**/*Test.java</include>
								</includes>
								<excludes>
									<exclude>**/*IntegrationTest.java</exclude>
								</excludes>
							</configuration>
						</execution>
						<execution>
							<id>integration-tests</id>
							<phase>integration-test</phase>
							<goals>
								<goal>test</goal>
							</goals>
							<configuration>
								<skip>false</skip>
								<includes>
									<include>**/*IntegrationTest.java</include>
								</includes>
							</configuration>
						</execution>
					</executions>
				</plugin>

			</plugins>
		</pluginManagement>
		<plugins>
			<!-- the maven war plugin has to be configured in the childs pom because 
				of its specific overlays -->

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<configuration>
					<!-- specify UTF-8, ISO-8859-1 or any other file encoding -->
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>
			<plugin>
				<!-- exclude artefacts in packages 'exclude' from the test jar e.g. to 
					not have multiple WebApplicationInitalizers in the classpath when depending 
					on many test artefacts -->
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>2.3.1</version>
				<configuration>
					<excludes>
						<exclude>**/exclude/**</exclude>
					</excludes>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<modules>
		<module>dojo-dep-js</module>
		<module>plugins-parent</module>
		<!-- -->
		<module>prefabware-js-bom</module>
		<module>prefabware-js-dep</module>
		<module>prefabware-js-archetype</module>
		<module>prefabware-js-commons</module>
		<module>prefabware-js-inject</module>
		<module>prefabware-js-model</module>
		<module>prefabware-js-plugin</module>
		<module>prefabware-js-test</module>
	</modules>

</project>