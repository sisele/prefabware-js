//a global object for logging etc
define([ 'dojo'
         , 'dojo/_base/lang'
         , 'dojo/_base/array'
         ,'dojo/promise/all'
         , 'dojo/when'
         ,'dojo/_base/declare' ], function(dojo, lang,array,allPromises) {
	var global = function(){
		this.__reservedWords={
				_abstract:true
				,_as:true  
				,_boolean:true
				,_break:true
				,_byte:true
				,_case:true
				,_catch:true
				,_char:true
				,_class:true
				,_continue:true
				,_const:true
				,_debugger:true
				,_default:true
				,_delete:true
				,_do:true
				,_double:true
				,_else:true
				,_enum:true
				,_export:true
				,_extends:true
				,_false:true
				,_final:true
				,_finally:true
				,_float:true
				,_for:true
				,_function:true
				,_goto:true
				,_if:true
				,_implements:true
				,_import:true
				,_in:true
				,_instanceof:true
				,_int:true
				,_interface:true
				,_is:true
				,_long:true
				,_namespace:true
				,_native:true
				,_new:true
				,_null:true
				,_package:true
				,_private:true
				,_protected:true
				,_public:true
				,_return:true
				,_short:true
				,_static:true
				,_super:true
				,_switch:true
				,_synchronized:true
				,_this:true
				,_throw:true
				,_throws:true
				,_transient:true
				,_true:true
				,_try:true
				,_typeof:true
				,_use:true
				,_var:true
				,_void:true
				,_volatile:true
				,_while:true
				,_with:true
			};
		this.__error=true;
		this.__warn=true;
		this.__info=false;
		this.__log=true;
		this.__debug=false;
		this.__assert=true;//execute asserts
		this.deferredValue = function(value) {
			//returns a deferred that is already resolved to the given value
			//for functions that need to return deferred but already know the result 
			var deferred=new dojo.Deferred();
			deferred.resolve(value);
			return deferred;
		};
		this.deferredList = function(arr,func,context) {
			if (context==null) {
				context={};
			}
			//to call a function for each element of the array once
			//uses the according element of the array as argument for the function
			//each function call must return a promise
			//all the promies are chained by 'then()', so that the next is not called before the previous is resolved
			//arr = an array of elements that the func will be applied to
			//context (optional)= the cintext for the function call
			//returns a promise with the result of the last call to func
			var p=this.deferredValue(null);//initial
			array.forEach(arr,function(element){
				var ff=dojo.hitch(context, func);//prepare the function to call
				//call the function with the current element as argument
				p=p.then(function(){
					return ff(element);
				});
			});
			return p;//return the last promise of the chain, that will be resolved as last
		};
		this.isDebug = function() {
			return debug;
		};
		this.allPromises = function(objectOrArray) {
			//to use all as prefabware.lang.all without require
			return allPromises(objectOrArray);
		};
		this.isPromise = function(x) {
			return dojo.isFunction(x.then);
		};
		this.throwError = function(text) {
			this.error(text);
			throw text;
		};
		this.error = function(text) {
			if (this.__error) {
				console.error(text);
			}
		};
		this.warn = function(text) {
			if (this.__warn) {
				console.warn(text);
			}
		};
		this.info = function(text) {
			if (this.__info) {
				console.log(text);
			}
		};
		this.log = function(text) {
			if (this.__log) {
				console.log(text);
			}
		};
		this.debug = function(text) {
			if (this.__debug) {
				console.log(text);
			}
		};
		this.assert = function(condition,text) {
			if (this.__assert && !condition) {
				var msg='assertion failed :'+text+' ';
				this.error(msg);
				throw msg;
			};
		};		
		this.assertArray = function(arrayOrNot) {
			//deprecated
			return this.toArray(arrayOrNot);
		};		
		this.toArray = function(arrayOrNot) {
			//to process arguments that may be an array or a single element
			//if the argument is an array it is returned
			//if the argument is not an array, an Array with one element is returnes 
			if (dojo.isArrayLike(arrayOrNot)) {
				return arrayOrNot;
			}else{
				return [arrayOrNot];
			}
		};		
		this.toHash = function(hashOrValue,name) {
			//to process arguments that may be a hash or a value e.g. a string
			//if the argument is a hash it is returned
			//if the argument is not a hash, a hash with a property name=hashOrValue is returned 
			if (this.isHash(hashOrValue)) {
				return hashOrValue;
			}else{
				var hash= {};
				hash[name]=hashOrValue;
				return hash;
			}
		};		
		this.isHash = function(object) {
			//returns true if its an object and not a primitive an array or a function
			//returns false, if object==null
			//works different than dojo.isObject
			return dojo.isObject(object)&&object!=null&&(!(typeof object=="function"))&&(!(object instanceof Date))&&!dojo.isArrayLike(object);
		};		
		this.locale = function() {
			//returns the current locale
			//at the moment this is the dojo locale but this cannot be changed without reload
			//so we dont want to couple to tight to it.
			return dojo.locale;
		};		
		this.equal = function(a,b) {
			//compares two simple objects {..} or primitives
			if (a===b) {
				//identic object or equal  primitives
				return true;
			}
			if (typeof a!='object'&& a!== b) {
				//its not an object and not equal
				return false;
			}
			//does NOT work recursively
			//returns true if a equal b
			if (a==null&&b==null) {
				//both null, equal !
				return true;
			}
			if (a==null&&b!=null) {
				//one null, not equal !
				return false;
			}
			if (a!=null&&b==null) {
				//one null, not equal !
				return false;
			}
			if (Object.keys(a).length!=Object.keys(b).length) {
				//different number of keys, not equal
				return false;
			}
			var key=null;
			for(key in a){
		        if (a[key]!=b[key]) {
					return false;
				}
		    }
			return true;
		};		
		this.endsWith = function(string,suffix) {
			//returns true, if the string ends with the given suffix
			if (string==null||suffix==null) {
				return false;
			}
			var posi=string.lastIndexOf(suffix);
			if (posi==-1) {
				return false;
			};
			return posi==string.length-suffix.length;
		};		
		this.concat = function() {
			var result="";
			//returns in a string concatenated of all given arguments
			//arguments == null are ignored
			//always returns a string,never returns null
			var i=0;
			for ( i = 0; i < arguments.length; i++) {
				var e = arguments[i];
				if (e!=null) {
					result=result+e;
				}
			}
			return result;
		};	
		this.startsWith = function(string,prefix) {
			//returns true, if the string starts with the given suffix
			if (string==null||prefix==null) {
				return false;
			}
			return 0==string.indexOf(prefix);			
		};	
		this.firstLetterLowerCase = function(string) {
				// returns the string with the first letter lowercase
				// returns 'user' for 'User'
				return string.charAt(0).toLowerCase() + string.substring(1, string.length);
		};	
		this.firstLetterUpperCase = function(string) {
			// returns the string with the first letter uppercase
			// returns 'User' for 'user'
			return string.charAt(0).toUpperCase() + string.substring(1, string.length);
		};	
		this.isNumber = function(value) {
			//return true, if the value is a number
			//dojo.lang.numeric returns true also for strings that can be converted to a number
			//but here for values of strings false must be returned
			//lang.isNumeric does not exist anymore !
			//return !lang.isString(value)&&lang.isNumeric(value);
			return !isNaN(parseFloat(value)) && isFinite(value);
		};
		this.isReservedWord = function(string) {
			//return true, if the string is a reserved word for javascript
			return this.__reservedWords['_'+string]===true;
		};
		this.selectNotNull = function(list) {
			//filters the provided array and returns a new array, that contains only
			//the elements!=null
			return array.filter(list, function(item){
			    return item!=null;
			  });
		};		

		this.getObjectAsync = function(object, nestedProp, f) {
			var that=this;
			// async !!
			// like dojo.lang.getObject, but uses a callback for every property
			// on its way
			// object the object whos property shall be evaluated
			this.assert(object!=null,"object must not be null");
			// nestedProp the name of the nested property,
			// can be a String seperated by '.' like customer.type.name
			// can be a String e.g. "name"
			// can be an array like ["customer,"type","name"]
			this.assert(nestedProp!=null,"nestedProp must not be null");
			//f=callback function that will be called for every property of the nested property 
			this.assert(f!=null,"callback must not be null");
			// returns a promise that resolves to the value of the property
			// walks along the nestedProperty and executes the function for
			// every property on its way.
			//the function should must a promise that resolves to the value of the property
			// example :
			// nestedProperty(a,"b.c.d",f)
			// executes function 3 times :
			// f(a,"b")
			// f(a.b,"c")
			// f(a.b.c,"d")
			// if any property on the way is null, processing is stopped and
			// a promise resolving to null is returned
			var nestedAr = null;
			if (dojo.isArrayLike(nestedProp)) {
				nestedAr=nestedProp;
			}else{
				nestedAr = nestedProp.split(".");
			}
			var cProp = nestedAr[0];
			var p = f(object, cProp);
			that.assert(p!=null,"callback must not return null");
			that.assert(this.isPromise(p),"callback must return a promise");
			if (nestedAr.length <= 1) {
				// no next property, we are done, return the result of the callback
				return p;
			}
			return p.then(function(pValue){
				// get the current properties value
				// get the next property name
				var nextProp = nestedAr.slice(1);				
				if (pValue == null) {
					// no value, no object for the next property, quit
					return that.deferredValue(null);
				}
				//recurse, the object is the current value, the property is the nextProp
				return that.getObjectAsync(pValue, nextProp, f);
			});
		};		
		};
		var instance=new global();
		lang.setObject("prefabware.lang", instance);
		return instance;
});