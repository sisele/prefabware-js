//this is the profile for the prefabware layer
//to overwrite it, put yours in the same folder in the app project
var profile = (function() {
	copyOnly = function(filename, mid) {
		var list = {
		// "mycompany/dojo.profile":1,
		// "mycompany/package.json":1
		};
		return (mid in list) || /(css|png|jpg|jpeg|gif|tiff)$/.test(filename);
	};

	return {
		resourceTags : {
			test : function(filename, mid) {
				return mid.indexOf("tests") >= 0;
			},

			copyOnly : function(filename, mid) {
				console.log("mid= "+mid+" filename=" + filename );
				var list = {
					"prefabware/model/match/TypeMatcher" : 1,
					"prefabware/model/match/AttributeMatcher" : 1,
				};
				if (mid in list) {
					// Matcher use eval with access to a local var, so must not
					// be uglified
					console.log("mid=" + mid
							+ " is blacklistet, copyOnly=" + true);
					return true;
				}
				var result = (!/\.js$/.test(filename))
						&& (!/\.css$/.test(filename));
				console.log("filename=" + filename + " copyOnly=" + result);
				return result;
			},

			amd : function(filename, mid) {
				return !copyOnly(filename, mid) && /\.js$/.test(filename);
			}
		},

		trees : [ [ ".", ".", /(\/\.)|(~$)/ ] ]
	};
})();