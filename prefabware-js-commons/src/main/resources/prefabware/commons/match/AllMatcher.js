//matches all
define('prefabware/commons/match/AllMatcher', [ 'dojo',
		'prefabware/commons/match/Matcher', 'prefabware/lang',
		'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.commons.match.AllMatcher",
			[ prefabware.commons.match.Matcher ], {
				degree:1,// poor match
				constructor : function(options) {
				},
				matches : function(options) {
					// returns allways true
					var that = this;
					return {
						matcher : this,
						degree : that.degree

					};
				},
				accepts : function(options) {
					// accepts everything
					return true;
				},
			});
});
