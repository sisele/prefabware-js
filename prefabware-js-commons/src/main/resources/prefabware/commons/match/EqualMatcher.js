//matches the value of this Matcher against the value provided to .matches
//value can be a string or a object hash

define('prefabware/commons/match/EqualMatcher', 
		[ 'dojo','dojo/_base/array','prefabware/commons/match/Matcher', 'prefabware/lang','dojo/_base/declare' ], function(dojo,array) {
	dojo.declare("prefabware.commons.match.EqualMatcher", [prefabware.commons.match.Matcher], {
		value:null,
		degree:100,//exact match 100%
		constructor : function(options) {
			this.value=options.value;
		},
		matches : function(options) {
						
			//matches if this.options == options
			if (prefabware.lang.equal(this.value,options.value)) {
				var that=this;
				return	{
						matcher:this,
						degree:that.degree
						
						};
			} else{
				return this.NO_MATCH;
			}
		},
	});
});
