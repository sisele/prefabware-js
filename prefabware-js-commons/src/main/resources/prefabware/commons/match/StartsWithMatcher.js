//matches against a value that is === this.value
define('prefabware/commons/match/StartsWithMatcher', [ 'dojo','prefabware/commons/match/Matcher', 'prefabware/lang','dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.commons.match.StartsWithMatcher", [prefabware.commons.match.Matcher], {
		value:null,
		degree:50,// average match
		constructor : function(options) {
			this.value=options.value;
		},
		matches : function(options) {
			if (typeof options.value!='string') {
				//doesNotApply may throw an exception or not
				this.doesNotApply('supplied value is not a string, value='+options.value);
				return this.NO_MATCH;
			}
			if (options.value.indexOf(this.value)==0) {
				var that=this;
				return	{
						matcher:this,
						degree:that.degree
						
						};
			} else{
				return this.NO_MATCH;
			}
		},
	});
});
