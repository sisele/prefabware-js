//to load resources at runtime
//takes care to load any resource only once
//only for resources loaded with the same instance of ResourceLoader
define('prefabware/commons/ResourceLoader', [ 'dojo'
                                            ,'dojo/_base/array'
                                            ,'dojo/io/script'
                                            ,'dojox/collections/Dictionary'
                                            ,'prefabware/lang',
		'dojo/_base/declare' ], function(dojo, array) {
	dojo.declare("prefabware.commons.ResourceLoader", null, {
		cache:null, 
		constructor : function() {
			this.cache=new dojox.collections.Dictionary();
		},
		load : function(resources) {
			//async
			//if resources is an array, loads the resources one after the other
			//if resources is an object loads the single resource,
			//returns a promise
			//each resource is the path to the resource file, relativ to the dojo folder
			// use '/' not '.' as path seperator 
			// e.g. 
			if (dojo.isArrayLike(resources)) {
				return this._loadResources(resources);
			}else{
				return this._loadResources([resources]);
				}
		},
		_loadResources : function(resources) {
			//async
			//loads the resource, one after the other
			//create an array of functions, each loading a resource
			var that=this;
			var functions= array.map(resources,function(resource){
				var f=function(){
					if (!that.cache.containsKey(resource)) {
						that.cache.add(resource,resource);
						return that._loadResource(that._path(resource));
					}else{
						return prefabware.lang.deferredValue();
					}
				};
				return f;
			});
			var last=null;
			//concatinate all functions with then
			array.forEach(functions,function(f,index){
				if (index==0) {
					last=f();
				}else{
					last=last.then(f);
				}
			});
			return last;
		},
		
		_loadResource : function(url) {
			//subclasses must override this method
			throw 'not implememnted';
		},
		_path : function(script) {
			// calculates the path relative to the current document
			// for a script with the qulified name, relativ to the dojo folder
			// e.g. script 'prefabware/commons/tests/.script1.js
			//returns a path to the script, relativ to the current document
			var b=dojo.baseUrl;// "../../"  the path relative to dojo.js fromwhere to search for packages
			//should be found at 
			var r=(b+script);
			return require.toUrl(script);
		}		
	});
});