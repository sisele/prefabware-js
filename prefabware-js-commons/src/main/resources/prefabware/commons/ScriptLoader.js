//to load scripts at runtime
//takes care to load any script only once
//only for scripts loaded with the same instance of ScriptLoader
define('prefabware/commons/ScriptLoader', [ 'dojo'
                                            ,'dojo/_base/array'
                                            ,'dojo/io/script'
                                            ,'dojox/collections/Dictionary'
                                            ,'prefabware/commons/ResourceLoader'
                                            ,'prefabware/lang',
		'dojo/_base/declare' ], function(dojo, array) {
	dojo.declare("prefabware.commons.ScriptLoader", [prefabware.commons.ResourceLoader], {
		constructor : function() {
		},
		_loadResource : function(url) {
			//loads the resource with the given url
			return dojo.io.script.get({ url:url});
		},
	});
});