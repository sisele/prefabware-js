//to cache whatever
define([ "dojo", 'dojo/_base/lang', 'dojo/_base/array',
		'dojo/store/util/QueryResults',
		'dojox/collections/Dictionary',
		'prefabware/lang',
		'prefabware/util/Parameters' ], function(dojo, lang, array) {

	// module:
	// prefabware/plugins/prefabware/store/AsyncStore

	var Cache = function(target,p_methods,p_toStrings,p_evictOn) {
		// target : the object to cache
		// methods : the names of the methods to cache
		// p_toStrings : a optional function or array of functions that convert the arguments, that is used as key for the cache, into a string
		//               if no toString-function is provided, argument.toString() will be used.
		//               if no p_toString is provided, the number of arguments is unknown and 5 will be assumed as a default
		// p_evict : options when to clear the cache
		//	       : maxHits = the cache will be cleared when number of hits is >= maxHits
		//	       : TODO :maxFails = the cache will be cleared when number of fails is >= maxFails
		// returns the cache with
		// cache.hits
		// cache.fails
		// cache.map
		//usage var bestCache=prefabware.commons.Cache(resolver,['bestMatch']);
		//continue using the original object, in this example : resolver
		//the returned cache object can be used to monitor the cache hits
		
		var cache={};
		cache.hits=0;
		cache.fails=0;
		
		var evictOn={};
		if (p_evictOn!=null) {
			lang.mixin(evictOn,p_evictOn);
		}
		if (evictOn.maxSize==null) {
			evictOn.maxSize=9999;
		}
		var methods=prefabware.lang.assertArray(p_methods);
		var p2_toStrings=prefabware.lang.assertArray(p_toStrings);
		if (p_toStrings==null) {
			//if the number auf arguments for the cached method is unknown, becaus p_toStrings is not passed
			//prepare for maximum 5 parameters
			p2_toStrings=[null,null,null,null,null];
		}
		var toStrings=array.map(p2_toStrings,function(toString){
			if (toString==null) {
				//nothing supplied?
				//use the keys toString function
				return function(key){return key.toString();};
			}else if(typeof toString=='function'){
				//a function was supplied, use it
				return toString;
			}else{
				prefabware.lang.throwError("the supplied toString "+toString+"is not a function" );
			}
			
		});
		//do not use a local variable for map, so we may swap it from outside
		cache.map= new dojox.collections.Dictionary();
		cache.map.hits=0;
		array.forEach(methods,function(method){
			var ori = target[method];
			prefabware.lang.assert(typeof ori=='function','cannot cache property '+method+',its not a function');
			target[method]=function(){
				var keyStr=cache.key(arguments);
				prefabware.lang.assert(keyStr!="[object Object]",'key does not provide a usable toString()='+keyStr);
				//consider cache
				if (cache.map.contains(keyStr)) {
					//return from cache if exists
					cache.hits++;
					return cache.map.item(keyStr);
				}else{
					cache.fails++;
					//if not call the original method
					var result=ori.apply(target,arguments);
					//and put the result into the cache
					if (cache.size()+1>=evictOn.maxSize) {
						//if max. size would be reached with the element we will add, clear the cache before
						cache.evict();
					}
					cache.put(keyStr,result);
					return result;
				}
			};
		});
	cache.evict=function(){
		cache.map.clear();
	};
	cache.put=function(key,value){
		cache.map.add(key,value);
	};
	cache.size=function(){
		return cache.map.count;
	};
	cache.key=function(args){
		var keys=array.map(args,function(arg,i){
			return toStrings[i](arg);
		});
		return keys.toString();
	};
	return cache;
	};

	lang.setObject("prefabware.commons.Cache",Cache);

	return Cache;
});
