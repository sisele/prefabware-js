// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.AmdCssLoaderTest");
//Import in the code being tested.
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.commons.tests.AmdCssLoaderTest", [
{
			name : "load a Css dynamically",
			timeout : 4000,
			setUp : function() {

			},
			runTest : function() {
				// its an async test, so return a doh.Deferred to make test
				// execution wait for the reolve
				var def = new doh.Deferred();
				var links=dojo.query("head link");
				var before=links.length;
				require(['prefabware/commons/css!prefabware/commons/tests/script1.css'],function(script){
					doh.assertEqual(before+1,dojo.query("head link").length,
					'a link should have been added');
					def.callback(true);
				});
				return def;
			},
			tearDown : function() {
			}
}]);