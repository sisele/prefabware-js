// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.CacheTest");
//Import in the code being tested.
dojo.require("prefabware.commons.Cache");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
dojo.require("dojo.promise.Promise");
doh.register("prefabware.commons.tests.CacheTest", [
 
  {
	  name: "cache tst",
	  map:null,
	  setUp: function(){
		 
	  },
	  runTest: function(){
		  var service={
				  double1:function(a){return a*2;},
				  double2:function(a,b){return (a+b)*2;},
				  double3:function(a,b,c){return (a+b+c)*2;}
		  };
			
			var ts1=function(key){return 'x'+key;};
			var ts2=function(key){return 'y'+key;};
			var cache22=new prefabware.commons.Cache(service,'double2',[ts1,ts2]);
			doh.assertEqual(6, service.double2(1,2));
			doh.assertEqual(0, cache22.hits);
			doh.assertEqual(1, cache22.fails);
			
			doh.assertEqual(6,cache22.map.item('x1,y2'),'the key for the cache should be calculated using the supplied functions');
			doh.assertEqual(6, service.double2(1,2));
			doh.assertEqual(1, cache22.hits);
			doh.assertEqual(1, cache22.fails);

			var cache2=new prefabware.commons.Cache(service,'double2');
			doh.assertEqual(6, service.double2(1,2));
			doh.assertEqual(0, cache2.hits);
			doh.assertEqual(1, cache2.fails);
			doh.assertEqual(6, service.double2(1,2));
			doh.assertEqual(1, cache2.hits);
			doh.assertEqual(1, cache2.fails);
			
			var cache1=new prefabware.commons.Cache(service,'double1');
			doh.assertEqual(2, service.double1(1));
			doh.assertEqual(0, cache1.hits);
			doh.assertEqual(1, cache1.fails);
			doh.assertEqual(2, service.double1(1));
			doh.assertEqual(1, cache1.hits);
			doh.assertEqual(1, cache1.fails);
			
	  },
	  tearDown: function(){
	  }
  }, 
]);