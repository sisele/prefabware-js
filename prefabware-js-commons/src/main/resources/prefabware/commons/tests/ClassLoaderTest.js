// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.ClassLoaderTest");
//Import in the code being tested.
dojo.require("prefabware.commons.ClassLoader");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.commons.tests.ClassLoaderTest", [
{
			name : "create instance dynamically",
			timeout : 4000,
			setUp : function() {

			},
			runTest : function() {
				// its an async test, so return a doh.Deferred to make test
				// execution wait for the reolve
				var def = new doh.Deferred();

				var cl = new prefabware.commons.ClassLoader();
				doh.assertTrue(prefabware.commons.tests.TestObject == undefined,
						'class should not be loaded yet');
				var deferred = cl.createInstance('prefabware.commons.tests.TestObject',"theId");
				deferred.then(function(instance) {
					doh.assertEqual(instance.id,"theId",
							'the id should be set');
					def.callback(true);
				}, function(error) {
					// Do something on failure.
				});

				return def;
			},
			tearDown : function() {
			}
		},
  {
	  name: "load class dynamically",
	  timeout: 4000,
	  setUp: function(){
		 
	  },
	  runTest: function(){
		  // its an async test, so return a doh.Deferred to make test
			// execution wait for the reolve
		  var def = new doh.Deferred();
		  
		  var cl = new prefabware.commons.ClassLoader();
		  doh.assertTrue(prefabware.commons.Proxy==undefined,'class should not be loaded yet');
		  var deferred=cl.load('prefabware.commons.Proxy');
		  deferred.then(function(clazz){
			  doh.assertTrue(clazz==prefabware.commons.Proxy,'the class should be available now');
			  def.callback(true);
			  },
			  function(error){
			    // Do something on failure.
			});

		  return def;
	  },
	  tearDown: function(){
	  }
  },
  {
	  name: "load allready loaded class dynamically",
	  timeout: 4000,
	  setUp: function(){
		  
	  },
	  runTest: function(){
		  // its an async test, so return a doh.Deferred to make test
			// execution wait for the reolve
		  var def = new doh.Deferred();
		  
		  var cl = new prefabware.commons.ClassLoader();
		  doh.assertTrue(prefabware.commons.ClassLoader!=undefined,'class should be loaded yet');
		  var deferred=cl.load('prefabware.commons.ClassLoader');
		  deferred.then(function(clazz){
			  doh.assertTrue(clazz==prefabware.commons.ClassLoader,'the class should be returned');
			  def.callback(true);
		  },
		  function(error){
			  // Do something on failure.
		  });
		  
		  return def;
	  },
	  tearDown: function(){
	  }
  },
 
  // ...
]);