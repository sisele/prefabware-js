// Declare out the name of the test module to make dojo's module loader happy.
dojo.provide("prefabware.commons.tests.DeferredTest");
// Import in the code being tested.
dojo.require("prefabware.commons.tests.TestObject");
dojo.require('dojo.Deferred');
dojo.require("dojo");
dojo.require("doh");
doh.register("prefabware.commons.tests.DeferredTest", [ {
	name : "async function",
	extension : null,
	timeout: 1000,
	setUp : function() {
	},
	runTest : function() {
		//its an async test, so return a doh.Deferred to make test execution wait for the resolve
	var testDeferred = new doh.Deferred();
		
	this.asyncFunction("result1").then(function(result){
		doh.assertEqual("result1", result,"result1 expected");
		testDeferred.callback(true);
	});
			
	return testDeferred;
	},
	
	asyncFunction : function(result) {
		var deferred=new dojo.Deferred();
		window.setTimeout(function(){
			deferred.resolve(result);
		},100);
		
		return deferred.promise;
	},
	tearDown : function() {
	}
},
 {
	name : "async function with more steps",
	extension : null,
	timeout: 1000,
	setUp : function() {
	},
	runTest : function() {
		//its an async test, so return a doh.Deferred to make test execution wait for the resolve
		var testDeferred = new doh.Deferred();
		
		this.complexFunction().then(function(result){
			doh.assertEqual("result3", result);
			testDeferred.callback(true);
		});
		
		return testDeferred;
	},
	
	complexFunction : function() {
		var that=this;
		var promise=that.asyncFunction("result1")
		.then(function(result){
			doh.assertEqual("result1", result);
			return that.asyncFunction("result2");
		}).then(function(result){
			doh.assertEqual("result2", result);
			return "result3";
		});
		
		return promise;
	},
	asyncFunction : function(result) {
		var deferred=new dojo.Deferred();
		window.setTimeout(function(){
			deferred.resolve(result);
		},100);
		
		return deferred;
	},
	tearDown : function() {
	}
},
]);