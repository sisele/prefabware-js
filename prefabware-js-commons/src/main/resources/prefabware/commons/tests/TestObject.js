//a Descriptor for a Plugin
define('prefabware/commons/tests/TestObject', [ 'dojo', 'dojo/_base/declare' ], function(
		dojo) { 
	dojo.declare("prefabware.commons.tests.TestObject", null, {
		id:null,
		constructor : function(id) {
			this.id=id;
		},	
		add : function(summand1,summand2) {
			return summand1+summand2;
		},
	});
});