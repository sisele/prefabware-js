// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.CssLoaderTest");
//Import in the code being tested.
dojo.require("prefabware.commons.CssLoader");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
doh.register("prefabware.commons.tests.CssLoaderTest", [
{
			name : "load a Css dynamically",
			timeout : 4000,
			setUp : function() {

			},
			runTest : function() {
				// its an async test, so return a doh.Deferred to make test
				// execution wait for the reolve
				var def = new doh.Deferred();

				var cl = new prefabware.commons.CssLoader();
				var links=dojo.query("head link");
				var before=links.length;
				var deferred = cl.load('prefabware/commons/tests/script1.css');
				deferred.then(function(instance) {
					doh.assertEqual(before+1,dojo.query("head link").length,
							'an link should have been added');
				}, function(error) {
					// Do something on failure.
				}).then(function(){
					//load again
					return cl.load('prefabware/commons/tests/script1.css');
				}).then(function(){
					doh.assertEqual(before+1,dojo.query("head link").length,
					'no more links should have been added');
					def.callback(true);
				});
				return def;
			},
			tearDown : function() {
			}
}]);