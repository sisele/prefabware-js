// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.commons.tests.match.MatchTest");
//Import in the code being tested.
dojo.require("prefabware.commons.match.Matcher");
dojo.require("prefabware.commons.match.EqualMatcher");
dojo.require("prefabware.commons.match.StartsWithMatcher");
dojo.require("prefabware.commons.match.MatchResultResolver");
dojo.require("dojo._base.lang");
dojo.require("prefabware.lang");
dojo.require("prefabware.commons.Cache");
doh.register("prefabware.commons.tests.match.MatchTest", [
{
	name: "onDoesNotApply",
	map:null,
	setUp: function(){
		
	},
	runTest: function(){
		//
		var first=new prefabware.commons.match.StartsWithMatcher({id:'first',value:'value1'});
		var resolver1=new prefabware.commons.match.MatchResultResolver({cache:true});
		resolver1.register(first);		
		resolver1.startup();
		doh.assertEqual(50, resolver1.bestMatch({value:'value1xxx'}).degree);
		try {
			resolver1.bestMatch({value:99});
			doh.assertTrue(false,'exception expected, 99 is not a string');
		} catch (e) {
		}
		
		var second=new prefabware.commons.match.StartsWithMatcher({id:'second',value:'value1',onDoesNotApply:'ignore'});
		var resolver2=new prefabware.commons.match.MatchResultResolver({cache:true});
		resolver2.register(second);		
		resolver2.startup();
		doh.assertEqual(50, resolver2.bestMatch({value:'value1xxx'}).degree);
		doh.assertEqual(null,resolver2.bestMatch({value:99}),'no exception expected, matcher should ignore wrong type');
		
		
		return;
	},
	tearDown: function(){
	}
},
{
	name: "Cache",
	map:null,
	setUp: function(){
		
	},
	runTest: function(){
		//default degree of equalmatcher is 100
		var first=new prefabware.commons.match.EqualMatcher({id:'first',value:'value1'});
		
		var resolver=new prefabware.commons.match.MatchResultResolver({cache:true});
		var toString=function(object){return dojo.toJson(object);};
		var bestCache=prefabware.commons.Cache(resolver,['bestMatch'],toString);
		var allCache=prefabware.commons.Cache(resolver,['allMatches'],toString);
		
		resolver.register(first);		
		resolver.startup();
		doh.assertEqual(first, resolver.bestMatch({value:'value1'}).matcher);
		doh.assertEqual(0, bestCache.hits);
		doh.assertEqual(0, allCache.hits);
		doh.assertEqual(1, bestCache.fails);
		doh.assertEqual(0, allCache.fails);
		
		doh.assertEqual(first, resolver.bestMatch({value:'value1'}).matcher);
		doh.assertEqual(1, bestCache.hits);
		doh.assertEqual(0, allCache.hits);
		doh.assertEqual(1, bestCache.fails);
		doh.assertEqual(0, allCache.fails);

		doh.assertEqual(1, resolver.allMatches({value:'value1'}).length);
		doh.assertEqual(1, bestCache.hits);
		doh.assertEqual(0, allCache.hits);
		doh.assertEqual(1, bestCache.fails);
		doh.assertEqual(1, allCache.fails);
		
		doh.assertEqual(1, resolver.allMatches({value:'value1'}).length);
		doh.assertEqual(1, bestCache.hits);
		doh.assertEqual(1, allCache.hits);
		doh.assertEqual(1, bestCache.fails);
		doh.assertEqual(1, allCache.fails);
		
		return;
	},
	tearDown: function(){
	}
},
{
	name: "ManyMatcher, different Order",
	map:null,
	setUp: function(){
		
	},
	runTest: function(){
		//default degree of equalmatcher is 100
		var first=new prefabware.commons.match.EqualMatcher({id:'first',value:'value1'});
		doh.assertEqual(100,first.order);
		var second=new prefabware.commons.match.EqualMatcher({id:'second',value:'value1',order:50});
		doh.assertEqual(50,second.order);
		var third=new prefabware.commons.match.EqualMatcher({id:'third',value:'value1',order:101});
		doh.assertEqual(101,third.order);
		var resolver=new prefabware.commons.match.MatchResultResolver();
		resolver.register(first);
		resolver.register(second);
		resolver.register(third);
		resolver.startup();
		doh.assertEqual(second.id, resolver.bestMatch({value:'value1'}).matcher.id);
		
		return;
	},
	tearDown: function(){
	}
},
{
	  name: "ManyMatcher, different Degree",
	  map:null,
	  setUp: function(){
		  
	  },
	  runTest: function(){
		  //default degree of equalmatcher is 100
		  var first=new prefabware.commons.match.EqualMatcher({id:'first',value:'value1'});
		  doh.assertEqual(100,first.degree);
		  var second=new prefabware.commons.match.EqualMatcher({id:'second',value:'value1',degree:102});
		  doh.assertEqual(102,second.degree);
		  var third=new prefabware.commons.match.EqualMatcher({id:'third',value:'value1',degree:101});
		  doh.assertEqual(101,third.degree);
		  var resolver=new prefabware.commons.match.MatchResultResolver();
		  resolver.register(first);
		  resolver.register(second);
		  resolver.register(third);
		  resolver.startup();
		  doh.assertEqual(second.id, resolver.bestMatch({value:'value1'}).matcher.id);
		  
		  return;
	  },
	  tearDown: function(){
	  }
},
{
	name: "ManyMatcher",
	map:null,
	setUp: function(){
		
	},
	runTest: function(){
		var sw=new prefabware.commons.match.StartsWithMatcher({id:'swm',value:'value'});
		var em=new prefabware.commons.match.EqualMatcher({id:'em',value:'value1'});
		var nm=new prefabware.commons.match.EqualMatcher({id:'nm',value:'othervalue'});
		var resolver=new prefabware.commons.match.MatchResultResolver();
		resolver.register(sw);
		resolver.register(em);
		resolver.register(nm);
		resolver.startup();
		doh.assertEqual(em.id, resolver.bestMatch({value:'value1'}).matcher.id);
		doh.assertEqual(sw.id, resolver.bestMatch({value:'value'}).matcher.id);
		doh.assertEqual(null, resolver.bestMatch({value:''}));
		doh.assertEqual(2, resolver.allMatches({value:'value1'}).length);
		
		return;
	},
	tearDown: function(){
	}
},
  {
	  name: "StartsWithMatcher",
	  map:null,
	  setUp: function(){
		 
	  },
	  runTest: function(){
		  var em=new prefabware.commons.match.StartsWithMatcher({id:'swm',value:'value'});
		  var resolver=new prefabware.commons.match.MatchResultResolver();
		  resolver.register(em);
		  resolver.startup();
		  doh.assertEqual(em.id, resolver.bestMatch({value:'value1'}).matcher.id);
		  doh.assertEqual(null, resolver.bestMatch({value:''}));
		  doh.assertEqual(em.id, resolver.bestMatch({value:'value'}).matcher.id);
		  doh.assertEqual(em.id, resolver.bestMatch({value:'value2'}).matcher.id);
		  return;
	  },
	  tearDown: function(){
	  }
  },
  {
	  name: "EqualMatcher",
	  map:null,
	  setUp: function(){
		  
	  },
	  runTest: function(){
		  var em=new prefabware.commons.match.EqualMatcher({id:'em',value:{value1:'value1'}});
		  var resolver=new prefabware.commons.match.MatchResultResolver();
		  resolver.register(em);
		  resolver.startup();
		  doh.assertEqual(em.id, resolver.bestMatch({value:{value1:'value1'}}).matcher.id);
		  doh.assertEqual(null, resolver.bestMatch({value1:''}));
		  doh.assertEqual(null, resolver.bestMatch({value:{value1:'value'}}));
		  doh.assertEqual(null, resolver.bestMatch({value:{value1:'value2'}}));
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  
  // ...
]);