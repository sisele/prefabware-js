//to check arguments of a method call
//e.g. to provide unique names
define('prefabware/util/Parameters', [ 'dojo',   'dojo/_base/array','dojo/_base/declare' ], function(dojo, array) {
	dojo.declare("prefabware.util.Parameters", null, {
		parms:null,
		constructor : function(parms) {
			//parms=array of parameter definitions
			//[{name:'parameterName',required:false,default_:'the default'}]
			//the parms will be matched to the given arguments by position
			//so the name is just for debugging and logging
			this.parms=parms;
		},
		raise : function(name,msg,args) {
				throw {name:name,message:msg
					+ '\n defined parameters :'+dojo.toJson(this.parms)
					+ '\n passed  parameters :'+dojo.toJson(args)};
		},
		check : function() {
			//checks the callers arguments
			//convert arguments into an array
			//containing our callers arguments
			var args=Array.prototype.slice.call(arguments.callee.caller.arguments);
			this.checkArray(args);
		},
		checkArray : function(args) {
			//checks the given array of arguments
			if (args==undefined&&this.parms!=undefined) {
				this.raise('NoArgumentsException','no arguments given but expected some',args);
			};
			//we expect to get an array with the arguments of the caller
			if (!dojo.isArray(args)) {
				this.raise('ArgumentsNoArrayException','the arguments for check must be an array but is'+args,args);
			};
			if (args.length>this.parms.length) {
				this.raise('ToManyArgumentsException','expected '+this.parms.length+' but got '+args.length,args);
			};
			// call to check the actual arguments
			// arguments.ceck(arguments);
			array.forEach(this.parms,function(parm,i){
				var arg;
				if (i >= args.length) {
					//not passed arguments are undefined, they may be optional, and than its no error
					arg = undefined;
				} else {
					arg = args[i];
				};
				if (arg==undefined&&parm.required==true) {
					this.raise('RequiredArgumentMissingException','required parm '+parm.name+' with index '+i+' is undefined ',args);
				};
				if (parm.typeof_!=undefined&&typeof arg!==parm.typeof_) {
					this.raise('TypeofArgumentWrongException','parm '+parm.name+' with index '+i+' expected typeof '+parm.typeof_+' but got '+typeof arg,args);
				};
				if (parm.declaredClass_!=undefined&&arg.declaredClass!=parm.declaredClass_) {
					this.raise('DeclaredClassWrongException','parm '+parm.name+' with index '+i+' expected declaredClass '+parm.declaredClass_+' but got '+arg.declareClass,args);
				};
				
			},this);
			return true;
		}		
	});
});
