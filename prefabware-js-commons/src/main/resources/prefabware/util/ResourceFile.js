//a file resource e.g. a json file
//to use async loading you have to call startup
define('prefabware/util/ResourceFile', [ 'dojo', "dojo/text",'prefabware/util/Resource',
		'dojo/_base/declare','prefabware/lang' ], function(dojo) {
	dojo.declare("prefabware.util.ResourceFile", [ prefabware.util.Resource ],
			{
	content : null,
	async : false,
	started : false,//startup was called
	constructor : function(options) {
		if (options.async!=null) {
			this.async==options.async;
		}
	},
	startup : function() {
		//async
		//returns a promise resolving to this
		if (this.started) {
			return prefabware.lang.deferredValue(this);
		}
		this.started=true;
		if (this.async==true) {
			return this.__loadAsync().then(function(){
				return prefabware.lang.deferredValue(this);
			});
		}else{
			//sync loading is done lazy
			return prefabware.lang.deferredValue(this);
		}
	},
	getContentAsString : function() {
		if (this.content == null) {
			if (this.async&&!this.started) {
				//ahoud have been loaded during startup
				throw 'to load a file async you have to call startup ! file : package '
				+ this.namespace + ' file ' + this.name;
			} else {
				this.__loadSync();
			}
		}
		return this.content;
	},
	__loadSync : function() {
		// sync
		// returns the content of the file as astring
		this.content= dojo.cache(this.namespace, this.name);
		if (this.content == undefined) {
			throw 'cannot find file : package '
					+ this.namespace + ' file ' + this.name;
		}
	},
	__loadAsync : function() {
		//async
		//is called from startup
		var deferred = new dojo.Deferred();
		var id="dojo/text!"+this.toFile();
		var that=this;
		require([id], function(file){
			that.content=file;
			if (file!=null) {
				deferred.resolve(file);
			}else{
				deferred.reject('could not find file '+id);
			};
			});
		return deferred.promise;
	},
	loadJsonFromFile : function(namespace,fullFileName) {
		
	},
});
});
