//a template whith placeholders that can be replaced with values
//you may use http://regex101.com/ to check regex expressions
define('prefabware/util/Template', [ 'dojo', "dojo/_base/lang",
		'dojo/_base/declare' ], function(dojo, lang) {
	dojo.declare("prefabware.util.Template", null, {
		regEx : /\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g,//finds ${} expressions
		template : null,
		constructor : function(options) {
			this.template = options.template;
			if (options.regEx) {
				this.regEx = options.regEx;
			}
		},		
		includeKey : function(key) {},
		findKeys : function() {
			// returns an array with the keys in the template
			// this allows to prepare the values needed before replacing 
			// this may be usefull when preparation happens async		
			var keys = [];
			lang.replace(this.template, function(match, key, offset) {
				keys.push(key);
				return key; // String
			}, this.regEx);
			return keys;
		},
		replace : function(values) {
			// replaces the placeholder in the template with the values of the values object
			var result= lang.replace(this.template, function(match, key, offset) {
				var value= lang.getObject(key,false,values);
				if (value==null) {
					//if no value is given, return the key
					return key;
				}else{
					return value;
				}
			}, this.regEx);
			return result;
		},
	});
});
