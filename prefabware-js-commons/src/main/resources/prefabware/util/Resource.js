//a resource like a json file
//all qualifiedName and namespace use a '.' as seperator
define('prefabware/util/Resource', [ 'dojo',  'dojo/_base/declare' ], function(dojo) {
	dojo.declare("prefabware.util.Resource", null, {
		namespace:null,
		name:null,
		qualifiedName:null,
		//TODO no suffix is not implemented  !
		hasSuffix:true,//true=the name has a file suffix like .html
		toPath : function() {
			//returns the namespace seperated by '/' instead of '.'
			if (this.namespace==null) {
				return "";
			}
			return this.namespace.split('.').join('/');
		},
		toFile : function() {
			//returns the qualified name seperated by '/' instead of '.'			
		return this.toPath()+'/'+this.name;
		},
		constructor : function(options) {
			if (options.hasSuffix!=null) {
				this.hasSuffix=options.hasSuffix;
			}
			if (options.qualifiedName) {
				this.qualifiedName=options.qualifiedName;
				//qualifiedName  is a dot seperated name like 
				//prefabware.plugins.prefabware.ui.widgets.SidebarItem.html
				//the file is supposed to have a filesuffix also seperated by a '.'
				//TODO respect hasSuffix' here
				var parts=options.qualifiedName.split('.');
				this.namespace=parts.slice(0,parts.length-2).join('.');
				this.name=parts[parts.length-2]+'.'+parts[parts.length-1];
			}else{
				this.namespace=options.namespace;
				this.name=options.name;
				this.qualifiedName=this.namespace+"."+this.name;
			}
		},
	});
});
