// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.Map");
//Import in the code being tested.
dojo.require("prefabware.util.Map");
dojo.require("dojo._base.lang");
doh.register("prefabware.util.tests.Map", [
 
  {   
	  name: "Map",
	  map:null,
	  setUp: function(){
		  this.map = new prefabware.util.Map();
	  },
	  runTest: function(){
		  this.map.put('a.b.c','1 2 3');
		  doh.assertEqual("1 2 3", this.map.find('a.b.c'));
		  return;
	  },
	  tearDown: function(){
	  }
  },
 
  // ...
]);