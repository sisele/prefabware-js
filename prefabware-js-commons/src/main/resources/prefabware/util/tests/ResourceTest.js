// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.ResourceTest");
//Import in the code being tested.
dojo.require("prefabware.util.Resource");
dojo.require("prefabware.util.ResourceFile");
dojo.require("dojo._base.lang");
doh.register("prefabware.util.tests.ResourceTest", [
 
  
  {   
	  name: "resource name",
	  setUp: function(){
	  },
	  runTest: function(){
		  var resource = new prefabware.util.Resource({qualifiedName:"a.b.c.file.html"});
		  doh.assertEqual("a.b.c", resource.namespace);
		  doh.assertEqual("file.html", resource.name);
		  doh.assertEqual("a/b/c", resource.toPath());
		  doh.assertEqual("a/b/c/file.html", resource.toFile());
		  
		  var qualifiedName="prefabware.plugins.prefabware.business.commons.tests.currency-type.json";
		  var resource2 = new prefabware.util.Resource({qualifiedName:qualifiedName});
		  doh.assertEqual("prefabware.plugins.prefabware.business.commons.tests", resource2.namespace);
		  doh.assertEqual("currency-type.json", resource2.name);
		  doh.assertEqual(qualifiedName, resource2.qualifiedName);
		  
		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  {   
	  name: "laod sync",
	  setUp: function(){
	  },
	  runTest: function(){
		  var deferred=new doh.Deferred();
		  var resource = new prefabware.util.ResourceFile({qualifiedName:"prefabware.util.tests.ResourceTest.js"});
		  doh.assertNotEqual(null,resource.getContentAsString());
		  return;
	  },
	  tearDown: function(){
	  }
  },
  {   
	  name: "laod async",
	  setUp: function(){
	  },
	  runTest: function(){
		  var deferred=new doh.Deferred();
		  var resource = new prefabware.util.ResourceFile({qualifiedName:"prefabware.util.tests.ResourceTest.js"});
		  resource.async=true;
		  resource.startup().then(function(){
			  doh.assertNotEqual(null,resource.getContentAsString());
			  deferred.callback(true);
		  });		  		  
		  return;
	  },
	  tearDown: function(){
	  }
  },
  // ...
]);