// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.Parameters");
//Import in the code being tested.
dojo.require("prefabware.util.Parameters");
dojo.require("dojo._base.lang");
doh.register("prefabware.util.tests.Parameters", [
 
  {
	  name: "Parameters",
	  setUp: function(){
	  },
	  expectedException: function(name,caught){
		  if (caught==null) {
			  throw(name+' expected but no excepetion was raised');
		  }
		  if (caught==null||caught.name!=name) {
				throw(name+' expected but raised '+caught.name+' message='+caught.message);
			}
	  },
	  runTest: function(){
		  var f=function(type){
			  var parameters = new prefabware.util.Parameters([{name:'type',required:true,typeof_:String}]);
			  parameters.check(arguments);
		  };
		  var f2=function(type){
			  var parameters = new prefabware.util.Parameters([{name:'type',required:true,declaredClass_:'prefabware.util.Parameters'}]);
			  parameters.check(arguments);
		  };
		var caught=null;
		try {			  
			  valid=f();
		} catch (e) {
		caught=e;	
		}
		this.expectedException('RequiredArgumentMissingException', caught);
		
		try {			  
			valid=f(4);
		} catch (e) {
		caught=e;	}
		this.expectedException('TypeofArgumentWrongException', caught);

		try {			  
			valid=f('a','b');
		} catch (e) {
			caught=e;	}
		this.expectedException('ToManyArgumentsException', caught);
		try {			  
			valid=f2('a');
		} catch (e) {
			caught=e;	}
		this.expectedException('DeclaredClassWrongException', caught);
		
		
		  //doh.assertEqual("1 2 3", this.Parameters.find('a.b.c'));
		  return;
	  },
	  tearDown: function(){
	  }
  },
 
  // ...
]);