// this is a non visual test. you will see nothing of the ui in the test runner
dojo.provide("prefabware.util.tests.TestTest");
//Import in the code being tested.
dojo.require("dojo._base.lang");
dojo.require("prefabware.test");
doh.register("prefabware.util.tests.TestTest", [
{
	name : "test test1",
	doSetup : function() {
		//must be declared before setUp
		this.a='a';
		this.b={value:1};
		//just simulating async setup
		var def=new dojo.Deferred();
		def.resolve();
		return def;
	},
	setUp : function() {
		prefabware.test.createSetup(this.doSetup);
	},
	runTest : function() {
		prefabware.test.runSetup();
		var setup=prefabware.test.setup;
		doh.assertEqual('a',setup.a);
		setup.b.value++;
		doh.assertEqual(2,setup.b.value);
	},
	tearDown : function() {
	}
},
{
	name : "test test2",
		runTest : function() {
		prefabware.test.runSetup();
		var setup=prefabware.test.setup;
		doh.assertEqual('a',setup.a);
		doh.assertEqual(1,setup.b.value,'unchanged original value expected');
	},
	tearDown : function() {
	}
},
{
	name : "test test3 asyncSetup",
	runTest : function() {
		def=new doh.Deferred();
		prefabware.test.runSetup().then(function(){
			var setup=prefabware.test.setup;
			doh.assertEqual('a',setup.a);
			doh.assertEqual(1,setup.b.value,'unchanged original value expected');
			def.callback(true);
		});
		return def;
	},
	tearDown : function() {
	}
},
]);