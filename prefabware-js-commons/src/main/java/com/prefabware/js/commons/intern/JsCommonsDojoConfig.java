package com.prefabware.js.commons.intern;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.prefabware.web.ResourceMappings.ResourceMapping;
import com.prefabware.web.WebJar;

/**
 * to configure the dojo resources as a webjar
 * @author stefan
 *
 */
@Configuration
public class JsCommonsDojoConfig {
	
	
	@Bean
	WebJar thirdpartyDojoWebJar() {
		return new WebJar("dojo-dojo", "1.9.5");
	}
	@Bean
	ResourceMapping rmWebJarDojo(@Qualifier("thirdpartyDojoWebJar") WebJar wj) {
		String path = "dojo/";
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}
	@Bean
	ResourceMapping rmWebJarDojox(@Qualifier("thirdpartyDojoWebJar") WebJar wj) {
		String path = "dojox/";
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}
	@Bean
	ResourceMapping rmWebJarDojoUtil(@Qualifier("thirdpartyDojoWebJar") WebJar wj) {
		String path = "util/";
		return new ResourceMapping(wj.mappedUrlPath(path), wj.classpathResource(path));
	}	
	
}
