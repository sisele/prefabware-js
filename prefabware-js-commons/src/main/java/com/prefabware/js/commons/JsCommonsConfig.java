package com.prefabware.js.commons;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.prefabware.js.commons.intern.JsCommonsDojoConfig;
import com.prefabware.js.commons.intern.JsCommonsMavenConfig;
import com.prefabware.js.commons.intern.JsCommonsWebJarConfig;
import com.prefabware.web.ResourceConfig;

/**
 * to configure everything necessary to use dojo and js-commons
 * import this config
 * @author stefan
 *
 */
@Configuration
@Import({
	JsCommonsMavenConfig.class,
	JsCommonsDojoConfig.class,
	JsCommonsWebJarConfig.class,
	ResourceConfig.class})
public class JsCommonsConfig {
	
}
