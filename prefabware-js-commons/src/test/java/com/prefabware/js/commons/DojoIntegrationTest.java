package com.prefabware.js.commons;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.prefabware.js.test.DohTestUrlBuilder;
import com.prefabware.js.test.DojoTestSupport;
import com.prefabware.web.ResourceMappings.ResourceMapping;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpringBootTestApplication.class)
// port:0 = use a random, free port
@WebIntegrationTest("server.port:0")
public class DojoIntegrationTest {
	private DojoTestSupport testSupport;

	@Value("${local.server.port}") int port;

	@Autowired @Qualifier("rmWebJarDojoUtil") ResourceMapping rmWebJarDojoUtil;
	@Autowired @Qualifier("rmWebJarPrefabware") ResourceMapping rmWebJarPrefabware;

	private DohTestUrlBuilder urlBuilder;

	@Before
	public void setUp() throws Exception {
		String baseUrl = "http://localhost:" + Integer.toString(port);
		testSupport = new DojoTestSupport(baseUrl);
		urlBuilder = createUrlBuilder();
		int wait = 300;

		add("prefabware.util.tests.TemplateTest");

		add("prefabware.util.tests.DeferredListTest");
		add("prefabware.util.tests.UrlBuilderTest");
	
		add("prefabware.commons.tests.AmdScriptLoaderTest", wait);
		add("prefabware.commons.tests.CssLoaderTest");
		add("prefabware.commons.tests.ScriptLoaderTest");
		add("prefabware.util.tests.i18n.ResourceI18nTest");
		add("prefabware.util.tests.ResourceTest");
		add("prefabware.commons.tests.CacheTest");
		add("prefabware.util.tests.LangTest");
		add("prefabware.util.tests.RestTest");
		add("prefabware.util.tests.TestTest");
		add("prefabware.commons.tests.match.MatchTest");
		add("prefabware.commons.tests.match.MatchTest");
		add("prefabware.commons.tests.DeferredTest");
		add("prefabware.commons.tests.ObjectProxyTest");
		add("prefabware.commons.tests.Proxy");
		add("prefabware.util.tests.Map");
		add("prefabware.util.tests.Parameters");
		add("prefabware.util.tests.UnexpectedAsyncTest");

	}

	public void add(String testJsClass) {
		testSupport.add(urlBuilder.withTestClass(testJsClass).build());		
	}
	public void add(String testJsClass,int wait) {
		testSupport.add(urlBuilder.withTestClass(testJsClass).build(),wait);		
	}
	public DohTestUrlBuilder createUrlBuilder() {
		DohTestUrlBuilder b = new DohTestUrlBuilder();
		b
				.withDohUtilBaseUrl(rmWebJarDojoUtil.urlPath())
				.withModule("prefabware", rmWebJarPrefabware.urlPath());
		return b;
	}

	@Test
	public void testAll() throws Exception {
		testSupport.testAll();
		return;
	}
}